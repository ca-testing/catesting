package com.testScripts;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.driverFactory.WebDriverFactory;
import com.page.BookARoomComponent;
import com.page.LoginComponent;
import com.utilities.BookARoomDataProvider;
import com.utilities.Constant;
import com.utilities.DynamicWait;

public class BookARoomTestScript extends BaseTest {

	@Test(enabled = true, priority = 1, dataProvider = "RoomBooking", dataProviderClass = BookARoomDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void createandVerifyBookRoom(String emailId, String pwd, String bookingType, String testAccount,
			String property, String rooms, String bookingSource, String cardType, String guestEmail,
			String guestComment, String goodCard, String emailForMainSite, String pwdForMainSite, String numberOfAdults,
			String numberOfChild, String arrivingBy, String arrivingTime, String numberOfGuest, String cardNumber,
			String expiryDate, String cardPin, String cardPostalNumber) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		BookARoomComponent bookARoom;
		boolean pre_Checkin_status = false;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			// loginComponent.clickOnDashBoard();
			bookARoom = new BookARoomComponent(WebDriverFactory.getDriver());
			// bookARoom.openBlankTab("https://www.google.com");
			bookARoom.clickOnCreateTestBooking();
			String title = bookARoom.getBookingTitle();
			System.out.println("Booking Title ----------- " + title);
			if (title.equals("Bookings")) {
				bookARoom.chooseBookingType(bookingType);
				bookARoom.chooseTestAccount(testAccount);
				bookARoom.chooseProperty(property);
				bookARoom.chooseRooms(rooms);
				bookARoom.chooseBookingSource(bookingSource);
				bookARoom.chooseCardType(cardType);
				//bookARoom.setcheckinDateForBooking();
				bookARoom.setcheckinDateForBooking();
				bookARoom.setPrice("1000");
				bookARoom.enterGuestEmail(guestEmail);
				bookARoom.enterGuestComment(guestComment);
				bookARoom.selectGoodCard(goodCard);
				bookARoom.clickOnSaveBtn();
				String bookId = bookARoom.getBookingId();
				System.out.println("Booking Id after booking --------- " + bookId);
				bookARoom.doLogout();

				loginComponent.clickOnLoginAtHomePage();
				loginComponent.enterEmail(emailForMainSite);
				loginComponent.enterPassword(pwdForMainSite);
				loginComponent.clickOnLogin();
				if (loginComponent.validateLogin()) {
					bookARoom.clickOnBookingFromMenuLink();
					String bookingId = bookARoom.isBookingIdMatched();
					String id = bookARoom.getIdValue();
					if (bookId.equals(bookingId)) {
						bookARoom.clickOnMenu(id);
						bookARoom.clickOnShareLink(id);
						String link = bookARoom.getLinkFromPopup();
						System.out.println("Link value from the popup -------- " + link);
						bookARoom.openBlankTab(link);
						bookARoom.clickOngetStarted();

						int stepCount = bookARoom.total_Step();
						for (int i = 0; i < stepCount; i++) {
							// ArrayList<String> stepName = bookARoom.checkSteps();
							String stepName = bookARoom.checkSteps();
							if (stepName.equals(Constant.BASIC_INFO)) {
								bookARoom.changeToLiveMode("Live Mode");
								bookARoom.enterGuestEmailId(guestEmail);
								bookARoom.enterNumberOfAdult(numberOfAdults);
								bookARoom.enterNumberOfChildren(numberOfChild);
								bookARoom.clickOnSaveContinueBtn();
								DynamicWait.waitUntil(10);
							} else if (stepName.equals(Constant.ARRIVAL)) {
								String arrivalHead = bookARoom.getArrivingHeading();
								if (arrivalHead.equals("ARRIVAL INFORMATION")) {
									if (arrivingBy.equals("Flight")) {
										bookARoom.selectArrivingBy(arrivingBy);
										bookARoom.setFlightNumber("IND9876S");
										bookARoom.selectExpectedArrivalTime(arrivingTime);
										bookARoom.clickOnSaveContinueBtn();
										DynamicWait.waitUntil(10);
									} else if (arrivingBy.equals("Other")) {
										bookARoom.selectArrivingBy(arrivingBy);
										bookARoom.setOtherDetails("Arriving By Other");
										// bookARoom.selectExpectedArrivalTime(arrivingTime);
										bookARoom.clickOnSaveContinueBtn();
										DynamicWait.waitUntil(10);
									} else {
										bookARoom.selectArrivingBy(arrivingBy);
										bookARoom.selectExpectedArrivalTime(arrivingTime);
										bookARoom.clickOnSaveContinueBtn();
										DynamicWait.waitUntil(10);
									}
								}
							} else if (stepName.equals(Constant.VERIFICATION)) {
								String creditHead = bookARoom.getCreditCardHeading();
								if (creditHead.equals("UPLOAD DOCUMENT(S)")) {
									// bookARoom.changeToLiveMode("View Mode");
									bookARoom.uploadCreditCard();
									bookARoom.clickOnSaveContinueBtn();
									DynamicWait.waitUntil(10);
								}
							} else if (stepName.equals(Constant.SELF_PORTRAIT)) {
								String selfieHead = bookARoom.getSelfieHeading();
								if (selfieHead.equals("Take a selfie to authenticate your identity")) {
									bookARoom.changeToLiveMode("View Mode");
									DynamicWait.waitUntil(3);
									bookARoom.clickOnSaveContinueBtn();
									DynamicWait.waitUntil(10);
								}
							} else if (stepName.equals(Constant.ADD_ON_SERVICES)) {
								String addOnServiceHeading = bookARoom.getAddOnServiceHeading();
								if (addOnServiceHeading.equals("Mandatory Add-on Services")) {
									bookARoom.changeToLiveMode("Live Mode");
									bookARoom.enterNumberOfGuestForLunch(numberOfGuest);
									bookARoom.checkedBreakFast();
									bookARoom.enterNumberOfGuestForBreakFast(numberOfGuest);
									bookARoom.checkedCarParking();
									bookARoom.enterNumberOfCarParking(numberOfGuest);
									bookARoom.checkedBikeTour();
									bookARoom.enterNumberOfBikeTour(numberOfGuest);
									bookARoom.checkEarlyCheckIn();
									bookARoom.checkedContinental();
									bookARoom.enterNumberOfGuestForContinental(numberOfGuest);
									bookARoom.clickOnSaveContinueBtn();
									DynamicWait.waitUntil(10);
								}
							} else if (stepName.equals(Constant.CREDIT_CARD)) {
								int count = bookARoom.isCredit_CardTextBoxPresent();
								if (count > 0) {
									// bookARoom.changeToLiveMode("Live Mode");
									bookARoom.setCredit_Card(cardNumber, expiryDate, cardPin, cardPostalNumber);
									WebDriverFactory.getDriver().switchTo().defaultContent();
									bookARoom.clickOnPay_BalanceContinue();
									DynamicWait.waitUntil(10);
								} else {
									bookARoom.clickOnPay_BalanceContinue();
									DynamicWait.waitUntil(10);
								}
							} else if (stepName.equals(Constant.QUESTIONNAIRE)) {
								bookARoom.fill_Questionnare("Nice!", "+91 7890654312", "pgtest@yopmail.com", "1000",
										"4/5 Tom Street");
								bookARoom.clickOnSaveContinueBtn();
								DynamicWait.waitUntil(10);
							}
						}

						int i = 0;
						String summary_Head = bookARoom.getSummaryHeading();
						String addOnServiceHeading = bookARoom.getAddOnServiceHeading();
						if (addOnServiceHeading.equals("Purchased Add-on Services")) {
							bookARoom.changeToLiveMode("Live Mode");
							bookARoom.clickOnSaveContinueBtn();
							DynamicWait.waitUntil(10);
						} else if (summary_Head.equals("Your Summary")) {
							String summaryHeade = bookARoom.getSummaryHeading();
							if (summaryHeade.equals("Your Summary")) {
								bookARoom.drawYourSign();
								bookARoom.clickOnTermsCondition();
								bookARoom.clickOnSaveContinueBtn();
								DynamicWait.waitUntil(15);
								pre_Checkin_status = bookARoom.isPrecheckinComplete(id);
								System.out.println("Status of the pre-checkin ------- " + pre_Checkin_status);
								i = i + 1;
							}
						}

						if (i == 0) {
							String summaryHeade = bookARoom.getSummaryHeading();
							if (summaryHeade.equals("Your Summary")) {
								bookARoom.drawYourSign();
								bookARoom.clickOnTermsCondition();
								bookARoom.clickOnSaveContinueBtn();
								DynamicWait.waitUntil(15);
								pre_Checkin_status = bookARoom.isPrecheckinComplete(id);
								System.out.println("Status of the pre-checkin ------- " + pre_Checkin_status);
							}
						}

						Assert.assertEquals(bookingId, bookId);
						Assert.assertEquals(pre_Checkin_status, true);
					} else {
						System.out.println("Registered Booking Id not found for pre-Checkin Form -------- " + bookId);
					}
				}
			}
		}
	}

	@Test(enabled = true, priority = 2, dataProvider = "sendPre_CheckinEmail", dataProviderClass = BookARoomDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void send_Pre_Check_inEmail(String emailId, String pwd, String bookingType, String testAccount,
			String property, String rooms, String bookingSource, String cardType, String guestEmail,
			String guestComment, String goodCard, String emailForMainSite, String pwdForMainSite, String numberOfAdults,
			String numberOfChild, String arrivingBy, String arrivingTime, String numberOfGuest, String cardNumber,
			String expiryDate, String cardPin, String cardPostalNumber) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		BookARoomComponent bookARoom;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			bookARoom = new BookARoomComponent(WebDriverFactory.getDriver());
			bookARoom.clickOnCreateTestBooking();
			String title = bookARoom.getBookingTitle();
			System.out.println("Booking Title ------------ " + title);
			if (title.equals("Bookings")) {
				bookARoom.chooseBookingType(bookingType);
				bookARoom.chooseTestAccount(testAccount);
				bookARoom.chooseProperty(property);
				bookARoom.chooseRooms(rooms);
				bookARoom.chooseBookingSource(bookingSource);
				bookARoom.chooseCardType(cardType);
				bookARoom.setcheckinDateForBooking();
				bookARoom.enterGuestEmail(guestEmail);
				bookARoom.enterGuestComment(guestComment);
				bookARoom.selectGoodCard(goodCard);
				bookARoom.clickOnSaveBtn();
				String bookId = bookARoom.getBookingId();
				System.out.println("Booking Id --------- " + bookId);
				bookARoom.doLogout();

				loginComponent.clickOnLoginAtHomePage();
				loginComponent.enterEmail(emailForMainSite);
				loginComponent.enterPassword(pwdForMainSite);
				loginComponent.clickOnLogin();
				if (loginComponent.validateLogin()) {
					bookARoom.clickOnBookingFromMenuLink();
					String bookingId = bookARoom.isBookingIdMatched();
					String id = bookARoom.getIdValue();
					if (bookId.equals(bookingId)) {
						bookARoom.clickOnMenu(id);
						bookARoom.clickOnSend_Pre_Check_in_Email(id);
						String msg = bookARoom.getEmailSuccessMsg();
						System.out.println("Email Success Message ------------ " + msg);
						Assert.assertEquals(msg, "Email was sent successfully.");
					}
				}
			}
		}
	}

	@Test(enabled = true, priority = 3, dataProvider = "sendPre_CheckinEmail", dataProviderClass = BookARoomDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void re_Sync(String emailId, String pwd, String bookingType, String testAccount, String property,
			String rooms, String bookingSource, String cardType, String guestEmail, String guestComment,
			String goodCard, String emailForMainSite, String pwdForMainSite, String numberOfAdults,
			String numberOfChild, String arrivingBy, String arrivingTime, String numberOfGuest, String cardNumber,
			String expiryDate, String cardPin, String cardPostalNumber) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		BookARoomComponent bookARoom;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			bookARoom = new BookARoomComponent(WebDriverFactory.getDriver());
			bookARoom.clickOnCreateTestBooking();
			String title = bookARoom.getBookingTitle();
			System.out.println("Booking Title ------------ " + title);
			if (title.equals("Bookings")) {
				bookARoom.chooseBookingType(bookingType);
				bookARoom.chooseTestAccount(testAccount);
				bookARoom.chooseProperty(property);
				bookARoom.chooseRooms(rooms);
				bookARoom.chooseBookingSource(bookingSource);
				bookARoom.chooseCardType(cardType);
				bookARoom.setcheckinDateForBooking();
				bookARoom.enterGuestEmail(guestEmail);
				bookARoom.enterGuestComment(guestComment);
				bookARoom.selectGoodCard(goodCard);
				bookARoom.clickOnSaveBtn();
				String bookId = bookARoom.getBookingId();
				System.out.println("Booking Id --------- " + bookId);
				bookARoom.doLogout();

				loginComponent.clickOnLoginAtHomePage();
				loginComponent.enterEmail(emailForMainSite);
				loginComponent.enterPassword(pwdForMainSite);
				loginComponent.clickOnLogin();
				if (loginComponent.validateLogin()) {
					bookARoom.clickOnBookingFromMenuLink();
					String bookingId = bookARoom.isBookingIdMatched();
					String id = bookARoom.getIdValue();
					if (bookId.equals(bookingId)) {
						bookARoom.clickOnMenu(id);
						bookARoom.clickOnRe_Sync(id);
						String msg = bookARoom.getEmailSuccessMsg();
						System.out.println("Booking synced successfully. ------------ " + msg);
						Assert.assertEquals(msg, "Booking synced successfully.");
					}
				}
			}
		}
	}

	@Test(enabled = false, priority = 4, dataProvider = "sendPre_CheckinEmail", dataProviderClass = BookARoomDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void view_Details(String emailId, String pwd, String bookingType, String testAccount, String property,
			String rooms, String bookingSource, String cardType, String guestEmail, String guestComment,
			String goodCard, String emailForMainSite, String pwdForMainSite, String numberOfAdults,
			String numberOfChild, String arrivingBy, String arrivingTime, String numberOfGuest, String cardNumber,
			String expiryDate, String cardPin, String cardPostalNumber) {

		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		BookARoomComponent bookARoom;
		boolean pre_Checkin_status = false;
		String guestFirstName = null;
		String guestLastName = null;
		String guestFullName = null;
		String guestName = null;
		String guestEmailAtBookList = null;
		String numberOfAdult = null;
		String numberOfChilds = null;
		String paymentType = null;
		String numberOfCreditCard = null;
		String numberOfDocument = null;
		String numberOfUpsell = null;
		// String creditCardSubmitted = null;
		String checkinDate = null;
		String checkoutDate = null;
		String nightStay = null;
		int beforeAdd_charge = 0;
		int afterAdd_charge = 0;
		int beforeAdd_card = 0;
		int afterAdd_card = 0;
		String upsell_head = null;
		String document_head = null;
		int total_Doc = 0;
		String textValue = null;
		String phnNumber = null;
		String emailValue = null;
		String numberValue = null;
		String textAreaValue = null;
		String descriptionVerify = null;
		String amountVerify = null;
		// int verifySign = 0;
		// boolean addOnservice = false;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			// loginComponent.clickOnDashBoard();
			bookARoom = new BookARoomComponent(WebDriverFactory.getDriver());
			// bookARoom.openBlankTab("https://www.google.com");
			bookARoom.clickOnCreateTestBooking();
			String title = bookARoom.getBookingTitle();
			System.out.println("Booking Title ----------- " + title);
			if (title.equals("Bookings")) {
				bookARoom.chooseBookingType(bookingType);
				bookARoom.chooseTestAccount(testAccount);
				bookARoom.chooseProperty(property);
				bookARoom.chooseRooms(rooms);
				bookARoom.chooseBookingSource(bookingSource);
				bookARoom.chooseCardType(cardType);
				bookARoom.setcheckinDateForBooking();
				bookARoom.setPrice("1000");
				bookARoom.enterGuestEmail(guestEmail);
				bookARoom.enterGuestComment(guestComment);
				bookARoom.selectGoodCard(goodCard);
				guestFirstName = bookARoom.getFirstNameOfGuest();
				guestLastName = bookARoom.getLastNameOfGuest();
				guestFullName = guestFirstName.concat(" " + guestLastName);
				System.out.println("Guest First Name ------- " + guestFirstName);
				System.out.println("Guest Last Name -------- " + guestLastName);
				System.out.println("Guest Full Name -------- " + guestFullName);
				bookARoom.clickOnSaveBtn();
				String bookId = bookARoom.getBookingId();
				System.out.println("Booking Id after booking --------- " + bookId);
				bookARoom.doLogout();

				loginComponent.clickOnLoginAtHomePage();
				loginComponent.enterEmail(emailForMainSite);
				loginComponent.enterPassword(pwdForMainSite);
				loginComponent.clickOnLogin();
				if (loginComponent.validateLogin()) {
					bookARoom.clickOnBookingFromMenuLink();
					String bookingId = bookARoom.isBookingIdMatched();
					String id = bookARoom.getIdValue();
					if (bookId.equals(bookingId)) {
						guestName = bookARoom.getGuestNameAtBookingListPage(id);
						System.out.println("Guest Name ------ " + guestName);
						bookARoom.clickOnMenu(id);
						bookARoom.clickOnPre_Check_in_Link(id);
						bookARoom.clickOngetStarted();

						int stepCount = bookARoom.total_Step();
						for (int i = 0; i < stepCount; i++) {
							// ArrayList<String> stepName = bookARoom.checkSteps();
							String stepName = bookARoom.checkSteps();
							if (stepName.equals(Constant.BASIC_INFO)) {
								bookARoom.changeToLiveMode("Live Mode");
								bookARoom.enterGuestEmailId(guestEmail);
								bookARoom.enterNumberOfAdult(numberOfAdults);
								bookARoom.enterNumberOfChildren(numberOfChild);
								bookARoom.clickOnSaveContinueBtn();
								DynamicWait.waitUntil(10);
							} else if (stepName.equals(Constant.ARRIVAL)) {
								String arrivalHead = bookARoom.getArrivingHeading();
								if (arrivalHead.equals("ARRIVAL INFORMATION")) {
									if (arrivingBy.equals("Flight")) {
										bookARoom.selectArrivingBy(arrivingBy);
										bookARoom.setFlightNumber("IND9876S");
										bookARoom.selectExpectedArrivalTime(arrivingTime);
										bookARoom.clickOnSaveContinueBtn();
										DynamicWait.waitUntil(10);
									} else if (arrivingBy.equals("Other")) {
										bookARoom.selectArrivingBy(arrivingBy);
										bookARoom.setOtherDetails("Arriving By Other");
										bookARoom.selectExpectedArrivalTime(arrivingTime);
										bookARoom.clickOnSaveContinueBtn();
										DynamicWait.waitUntil(10);
									} else {
										bookARoom.selectArrivingBy(arrivingBy);
										bookARoom.selectExpectedArrivalTime(arrivingTime);
										bookARoom.clickOnSaveContinueBtn();
										DynamicWait.waitUntil(10);
									}
								}
							} else if (stepName.equals(Constant.VERIFICATION)) {
								String creditHead = bookARoom.getCreditCardHeading();
								if (creditHead.equals("UPLOAD DOCUMENT(S)")) {
									// bookARoom.changeToLiveMode("View Mode");
									bookARoom.uploadCreditCard();
									bookARoom.clickOnSaveContinueBtn();
									DynamicWait.waitUntil(10);
								}
							} else if (stepName.equals(Constant.SELF_PORTRAIT)) {
								String selfieHead = bookARoom.getSelfieHeading();
								if (selfieHead.equals("Take a selfie to authenticate your identity")) {
									bookARoom.changeToLiveMode("View Mode");
									DynamicWait.waitUntil(3);
									bookARoom.clickOnSaveContinueBtn();
									DynamicWait.waitUntil(10);
								}
							} else if (stepName.equals(Constant.ADD_ON_SERVICES)) {
								String addOnServiceHeading = bookARoom.getAddOnServiceHeading();
								if (addOnServiceHeading.equals("Mandatory Add-on Services")) {
									bookARoom.changeToLiveMode("Live Mode");
									bookARoom.enterNumberOfGuestForLunch(numberOfGuest);
									bookARoom.checkedBreakFast();
									bookARoom.enterNumberOfGuestForBreakFast(numberOfGuest);
									bookARoom.checkedCarParking();
									bookARoom.enterNumberOfCarParking(numberOfGuest);
									bookARoom.checkedBikeTour();
									bookARoom.enterNumberOfBikeTour(numberOfGuest);
									bookARoom.checkEarlyCheckIn();
									bookARoom.checkedContinental();
									bookARoom.enterNumberOfGuestForContinental(numberOfGuest);
									bookARoom.clickOnSaveContinueBtn();
									DynamicWait.waitUntil(10);
								}
							} else if (stepName.equals(Constant.CREDIT_CARD)) {
								int count = bookARoom.isCredit_CardTextBoxPresent();
								if (count > 0) {
									// bookARoom.changeToLiveMode("Live Mode");
									bookARoom.setCredit_Card(cardNumber, expiryDate, cardPin, cardPostalNumber);
									WebDriverFactory.getDriver().switchTo().defaultContent();
									bookARoom.clickOnPay_BalanceContinue();
									DynamicWait.waitUntil(10);
								} else {
									bookARoom.clickOnPay_BalanceContinue();
									DynamicWait.waitUntil(10);
								}
							} else if (stepName.equals(Constant.QUESTIONNAIRE)) {
								bookARoom.fill_Questionnare("Nice!", "+91 7890654312", "pgtest@yopmail.com", "1000",
										"4/5 Tom Street");
								bookARoom.clickOnSaveContinueBtn();
								DynamicWait.waitUntil(10);
							}
						}

						int i = 0;
						String summary_Head = bookARoom.getSummaryHeading();
						String addOnServiceHeading = bookARoom.getAddOnServiceHeading();
						if (addOnServiceHeading.equals("Purchased Add-on Services")) {
							bookARoom.changeToLiveMode("Live Mode");
							bookARoom.clickOnSaveContinueBtn();
							DynamicWait.waitUntil(10);
						} else if (summary_Head.equals("Your Summary")) {
							String summaryHeade = bookARoom.getSummaryHeading();
							if (summaryHeade.equals("Your Summary")) {
								bookARoom.drawYourSign();
								bookARoom.clickOnTermsCondition();
								bookARoom.clickOnSaveContinueBtn();
								DynamicWait.waitUntil(15);
								pre_Checkin_status = bookARoom.isPrecheckinComplete(id);
								System.out.println("Status of the pre-checkin ------- " + pre_Checkin_status);
								checkinDate = bookARoom.getcheckinDate_in_Card(id);
								checkoutDate = bookARoom.getcheckoutDate_in_Card(id);
								nightStay = bookARoom.get_numberOfDays_Stays(id);
								numberOfCreditCard = bookARoom.getNumberofCreditCardUploaded(id);
								numberOfDocument = bookARoom.getNumberOfDocumentUploaded(id);
								numberOfUpsell = bookARoom.getnumberofUpsell(id);
								System.out.println("Number of credit card --------- " + numberOfCreditCard);
								System.out.println("Number of document upload -------- " + numberOfDocument);
								System.out.println("Number of Upsell ------------ " + numberOfUpsell);
								System.out.println("Checkin Date on card ---------- " + checkinDate);
								System.out.println("Checkout Date on card ---------- " + checkoutDate);
								System.out.println("Total night Stay ------------- " + nightStay);
								bookARoom.openCard(id);
								DynamicWait.waitUntil(10);
								guestEmailAtBookList = bookARoom.guestEmailAtBookingListPage(id);
								numberOfAdult = bookARoom.numberOfAdultGuestAtBookingList(id);
								numberOfChilds = bookARoom.numberOfChildGuestAtBookingList(id);
								paymentType = bookARoom.paymentTypeAtBookingList(id);
								i = i + 1;
							}
						}

						if (i == 0) {
							String summaryHeade = bookARoom.getSummaryHeading();
							if (summaryHeade.equals("Your Summary")) {
								bookARoom.drawYourSign();
								bookARoom.clickOnTermsCondition();
								bookARoom.clickOnSaveContinueBtn();
								DynamicWait.waitUntil(15);
								pre_Checkin_status = bookARoom.isPrecheckinComplete(id);
								System.out.println("Status of the pre-checkin ------- " + pre_Checkin_status);
								checkinDate = bookARoom.getcheckinDate_in_Card(id);
								checkoutDate = bookARoom.getcheckoutDate_in_Card(id);
								nightStay = bookARoom.get_numberOfDays_Stays(id);
								numberOfCreditCard = bookARoom.getNumberofCreditCardUploaded(id);
								numberOfDocument = bookARoom.getNumberOfDocumentUploaded(id);
								numberOfUpsell = bookARoom.getnumberofUpsell(id);
								System.out.println("Number of credit card --------- " + numberOfCreditCard);
								System.out.println("Number of document upload -------- " + numberOfDocument);
								System.out.println("Number of Upsell ------------ " + numberOfUpsell);
								System.out.println("Checkin Date on card ---------- " + checkinDate);
								System.out.println("Checkout Date on card ---------- " + checkoutDate);
								System.out.println("Total night Stay ------------- " + nightStay);
								bookARoom.openCard(id);
								DynamicWait.waitUntil(10);
								guestEmailAtBookList = bookARoom.guestEmailAtBookingListPage(id);
								numberOfAdult = bookARoom.numberOfAdultGuestAtBookingList(id);
								numberOfChilds = bookARoom.numberOfChildGuestAtBookingList(id);
								paymentType = bookARoom.paymentTypeAtBookingList(id);
							}
						}

						String bookingId_bookingTab = null;
						String bookingDate_bookingTab = null;
						String source_bookingTab = null;
						String checkinDate_bookingTab = null;
						String checkoutDate_bookingTab = null;
						String guestComment_bookingTab = null;
						String notes = null;
						String firstName_bookinTab = null;
						String lastName_bookingTab = null;

						if (pre_Checkin_status) {
							bookARoom.clickOnMenu(id);
							bookARoom.clickOnViewDetails(id);
							bookingId_bookingTab = bookARoom.getBookingId_from_bookingTab();
							bookingDate_bookingTab = bookARoom.getBookingDate_from_bookingTab();
							source_bookingTab = bookARoom.getBookingSource_from_BookingTab();
							checkinDate_bookingTab = bookARoom.getcheckinDate_from_BookingTab();
							checkoutDate_bookingTab = bookARoom.getcheckoutDate_from_BookingTab();
							bookARoom.changeEstimatedCheckinTime(arrivingTime);
							bookARoom.changeNumberOfGuest(numberOfAdults);
							bookARoom.changeNumberOfChild(numberOfChild);
							guestComment_bookingTab = bookARoom.getGuestComment();
							bookARoom.changeInternalNotes("This is Test Notes");
							notes = bookARoom.getNotes();
							firstName_bookinTab = bookARoom.getFirstName_from_BookingTab();
							lastName_bookingTab = bookARoom.getLastName_from_BookingTab();
							bookARoom.changeEmail(guestEmail);
							bookARoom.changePhoneNumber("8765904321");
							bookARoom.clickSaveChanges();

							System.out.println("Booking Id on Booking Tab --------- " + bookingId_bookingTab);
							System.out.println("Booking Date on Booking Tab --------- " + bookingDate_bookingTab);
							System.out.println("Booking Source on Booking Tab ------- " + source_bookingTab);
							System.out.println("Checkin Date on Booking Tab --------- " + checkinDate_bookingTab);
							System.out.println("Checkout Date on Bookinhg Tab -------- " + checkoutDate_bookingTab);
							System.out.println("Guest Comment on Booking Tab --------- " + guestComment_bookingTab);
							System.out.println("Notes on Booking Tab --------------- " + notes);
							System.out.println("First Name on Booking Tab ------------ " + firstName_bookinTab);
							System.out.println("Last Name on Booking Tab -------------- " + lastName_bookingTab);

							/*
							 * Payments
							 */

							bookARoom.clickOnPaymentTab();
							beforeAdd_charge = bookARoom.paymentStatusCard();
							bookARoom.additionalCharge("100", "Test Charge");
							afterAdd_charge = bookARoom.paymentStatusCard();
							beforeAdd_card = bookARoom.totalNumberOfCreditCard();
							bookARoom.clickOnAddPaymentMethod();
							int count = bookARoom.isCredit_CardTextBoxPresent();
							if (count > 0) {
								// bookARoom.changeToLiveMode("Live Mode");
								bookARoom.setCredit_Card(cardNumber, expiryDate, cardPin, cardPostalNumber);
								WebDriverFactory.getDriver().switchTo().defaultContent();
							}
							bookARoom.updateAfterAddPaymentMethod();
							afterAdd_card = bookARoom.totalNumberOfCreditCard();

							/*
							 * Refund
							 */

							bookARoom.clickOnRefundButton();
							bookARoom.doRefund("100", "Test");
							descriptionVerify = bookARoom.refund_DescriptionVerify();
							amountVerify = bookARoom.amountRefundVerify();
							System.out.println("Description Verify Text from Test ------- " + descriptionVerify);
							System.out.println("Amount verification Text from Test ----------- " + amountVerify);

							/*
							 * Online Check-in
							 */

							bookARoom.clickOnCheckin_Tab();
							bookARoom.changeEstimatedArrivalTime(arrivingTime);
							bookARoom.changeArrivalBy(arrivingBy, "IND87535R");
							bookARoom.clickOnSaveChangeAfterEdit();

							/*
							 * Upsell & Document
							 */

							bookARoom.clickOnUpsell();
							upsell_head = bookARoom.verifyTitleUpsell();
							bookARoom.clickOnDocument();
							document_head = bookARoom.getHeadingDocument();
							total_Doc = bookARoom.total_document();

							/*
							 * Questionnaire Tab
							 * 
							 */

							textValue = bookARoom.getTextBoxValue_questionTab();
							phnNumber = bookARoom.getTextPhone_questionTab();
							emailValue = bookARoom.getEmail_questionTab();
							numberValue = bookARoom.getNumber_questionTab();
							textAreaValue = bookARoom.textarea_questionTab();

						}

						Assert.assertEquals(bookingId, bookId);
						Assert.assertEquals(pre_Checkin_status, true);
						Assert.assertEquals(guestFullName, guestName);
						Assert.assertEquals(numberOfCreditCard, "1");
						Assert.assertEquals(numberOfDocument, "2");
						Assert.assertEquals(numberOfUpsell, "6");
						Assert.assertEquals(guestEmail, guestEmailAtBookList);
						Assert.assertTrue(numberOfAdult.contains(numberOfAdults));
						Assert.assertTrue(numberOfChilds.contains(numberOfChild));
						if (paymentType.equals(cardType)) {
							Assert.assertEquals(paymentType, cardType);
						} else {
							Assert.assertEquals(paymentType, "Credit Card");
						}
						Assert.assertEquals(bookingId_bookingTab, bookingId);
						Assert.assertEquals(bookingDate_bookingTab, bookARoom.getTodaysDate());
						Assert.assertTrue(bookingSource.contains(source_bookingTab));
						// Assert.assertTrue(checkinDate.contains(checkinDate_bookingTab));
						// Assert.assertTrue(checkoutDate.contains(checkoutDate_bookingTab));
						Assert.assertEquals(guestComment, guestComment_bookingTab);
						Assert.assertEquals(firstName_bookinTab, guestFirstName);
						Assert.assertEquals(lastName_bookingTab, guestLastName);
						Assert.assertTrue(afterAdd_charge > beforeAdd_charge);
						Assert.assertTrue(afterAdd_card > beforeAdd_card);
						Assert.assertTrue(upsell_head.contains("Add-on Services Purchased"));
						Assert.assertTrue(document_head.contains("Verify Documents"));
						Assert.assertTrue(total_Doc > 1);
						Assert.assertEquals(textValue, "Nice!");
						Assert.assertEquals(phnNumber, "+91 7890654312");
						Assert.assertEquals(emailValue, guestEmail);
						Assert.assertEquals(numberValue, "1000");
						Assert.assertEquals(textAreaValue, "4/5 Tom Street");
						// Assert.assertTrue(descriptionVerify.contains("Test (Refund)"));
						// Assert.assertTrue(amountVerify.contains("-₹100.00"));

					} else {
						System.out.println("Registered Booking Id not found for pre-Checkin Form -------- " + bookId);
					}
				}
			}
		}
	}

	@Test(enabled = true, priority = 5, dataProvider = "sendPre_CheckinEmail", dataProviderClass = BookARoomDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void guest_Portal(String emailId, String pwd, String bookingType, String testAccount, String property,
			String rooms, String bookingSource, String cardType, String guestEmail, String guestComment,
			String goodCard, String emailForMainSite, String pwdForMainSite, String numberOfAdults,
			String numberOfChild, String arrivingBy, String arrivingTime, String numberOfGuest, String cardNumber,
			String expiryDate, String cardPin, String cardPostalNumber) {

		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		BookARoomComponent bookARoom;
		boolean pre_Checkin_status = false;
		String guestFirstName = null;
		String guestLastName = null;
		String guestFullName = null;
		String guestName = null;
		String guestEmailAtBookList = null;
		String numberOfAdult = null;
		String numberOfChilds = null;
		String paymentType = null;
		String numberOfCreditCard = null;
		String numberOfDocument = null;
		String numberOfUpsell = null;
		String creditCardSubmitted = null;
		int verifySign = 0;
		boolean addOnservice = false;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			// loginComponent.clickOnDashBoard();
			bookARoom = new BookARoomComponent(WebDriverFactory.getDriver());
			// bookARoom.openBlankTab("https://www.google.com");
			bookARoom.clickOnCreateTestBooking();
			String title = bookARoom.getBookingTitle();
			System.out.println("Booking Title ----------- " + title);
			if (title.equals("Bookings")) {
				bookARoom.chooseBookingType(bookingType);
				bookARoom.chooseTestAccount(testAccount);
				bookARoom.chooseProperty(property);
				bookARoom.chooseRooms(rooms);
				bookARoom.chooseBookingSource(bookingSource);
				bookARoom.chooseCardType(cardType);
				bookARoom.setcheckinDateForBooking();
				bookARoom.setPrice("1000");
				bookARoom.enterGuestEmail(guestEmail);
				bookARoom.enterGuestComment(guestComment);
				bookARoom.selectGoodCard(goodCard);
				guestFirstName = bookARoom.getFirstNameOfGuest();
				guestLastName = bookARoom.getLastNameOfGuest();
				guestFullName = guestFirstName.concat(" " + guestLastName);
				System.out.println("Guest First Name ------- " + guestFirstName);
				System.out.println("Guest Last Name -------- " + guestLastName);
				System.out.println("Guest Full Name -------- " + guestFullName);
				bookARoom.clickOnSaveBtn();
				String bookId = bookARoom.getBookingId();
				System.out.println("Booking Id after booking --------- " + bookId);
				bookARoom.doLogout();

				loginComponent.clickOnLoginAtHomePage();
				loginComponent.enterEmail(emailForMainSite);
				loginComponent.enterPassword(pwdForMainSite);
				loginComponent.clickOnLogin();
				if (loginComponent.validateLogin()) {
					bookARoom.clickOnBookingFromMenuLink();
					String bookingId = bookARoom.isBookingIdMatched();
					String id = bookARoom.getIdValue();
					if (bookId.equals(bookingId)) {
						guestName = bookARoom.getGuestNameAtBookingListPage(id);
						System.out.println("Guest Name ------ " + guestName);
						bookARoom.clickOnMenu(id);
						bookARoom.clickOnPre_Check_in_Link(id);
						bookARoom.clickOngetStarted();

						int stepCount = bookARoom.total_Step();
						for (int i = 0; i < stepCount; i++) {
							// ArrayList<String> stepName = bookARoom.checkSteps();
							String stepName = bookARoom.checkSteps();
							if (stepName.equals(Constant.BASIC_INFO)) {
								bookARoom.changeToLiveMode("Live Mode");
								bookARoom.enterGuestEmailId(guestEmail);
								bookARoom.enterNumberOfAdult(numberOfAdults);
								bookARoom.enterNumberOfChildren(numberOfChild);
								bookARoom.clickOnSaveContinueBtn();
								DynamicWait.waitUntil(10);
							} else if (stepName.equals(Constant.ARRIVAL)) {
								String arrivalHead = bookARoom.getArrivingHeading();
								if (arrivalHead.equals("ARRIVAL INFORMATION")) {
									if (arrivingBy.equals("Flight")) {
										bookARoom.selectArrivingBy(arrivingBy);
										bookARoom.setFlightNumber("IND9876S");
										bookARoom.selectExpectedArrivalTime(arrivingTime);
										bookARoom.clickOnSaveContinueBtn();
										DynamicWait.waitUntil(10);
									} else if (arrivingBy.equals("Other")) {
										bookARoom.selectArrivingBy(arrivingBy);
										bookARoom.setOtherDetails("Arriving By Other");
										bookARoom.selectExpectedArrivalTime(arrivingTime);
										bookARoom.clickOnSaveContinueBtn();
										DynamicWait.waitUntil(10);
									} else {
										bookARoom.selectArrivingBy(arrivingBy);
										bookARoom.selectExpectedArrivalTime(arrivingTime);
										bookARoom.clickOnSaveContinueBtn();
										DynamicWait.waitUntil(10);
									}
								}
							} else if (stepName.equals(Constant.VERIFICATION)) {
								String creditHead = bookARoom.getCreditCardHeading();
								if (creditHead.equals("UPLOAD DOCUMENT(S)")) {
									// bookARoom.changeToLiveMode("View Mode");
									bookARoom.uploadCreditCard();
									bookARoom.clickOnSaveContinueBtn();
									DynamicWait.waitUntil(10);
								}
							} else if (stepName.equals(Constant.SELF_PORTRAIT)) {
								String selfieHead = bookARoom.getSelfieHeading();
								if (selfieHead.equals("Take a selfie to authenticate your identity")) {
									bookARoom.changeToLiveMode("View Mode");
									DynamicWait.waitUntil(3);
									bookARoom.clickOnSaveContinueBtn();
									DynamicWait.waitUntil(10);
								}
							} else if (stepName.equals(Constant.ADD_ON_SERVICES)) {
								String addOnServiceHeading = bookARoom.getAddOnServiceHeading();
								if (addOnServiceHeading.equals("Mandatory Add-on Services")) {
									bookARoom.changeToLiveMode("Live Mode");
									bookARoom.enterNumberOfGuestForLunch(numberOfGuest);
									bookARoom.checkedBreakFast();
									bookARoom.enterNumberOfGuestForBreakFast(numberOfGuest);
									bookARoom.checkedCarParking();
									bookARoom.enterNumberOfCarParking(numberOfGuest);
									bookARoom.checkedBikeTour();
									bookARoom.enterNumberOfBikeTour(numberOfGuest);
									bookARoom.checkEarlyCheckIn();
									bookARoom.checkedContinental();
									bookARoom.enterNumberOfGuestForContinental(numberOfGuest);
									bookARoom.clickOnSaveContinueBtn();
									DynamicWait.waitUntil(10);
								}
							} else if (stepName.equals(Constant.CREDIT_CARD)) {
								int count = bookARoom.isCredit_CardTextBoxPresent();
								if (count > 0) {
									// bookARoom.changeToLiveMode("Live Mode");
									bookARoom.setCredit_Card(cardNumber, expiryDate, cardPin, cardPostalNumber);
									WebDriverFactory.getDriver().switchTo().defaultContent();
									bookARoom.clickOnPay_BalanceContinue();
									DynamicWait.waitUntil(10);
								} else {
									bookARoom.clickOnPay_BalanceContinue();
									DynamicWait.waitUntil(10);
								}
							} else if (stepName.equals(Constant.QUESTIONNAIRE)) {
								bookARoom.fill_Questionnare("Nice!", "+91 7890654312", "pgtest@yopmail.com", "1000",
										"4/5 Tom Street");
								bookARoom.clickOnSaveContinueBtn();
								DynamicWait.waitUntil(10);
							}
						}

						int i = 0;
						String summary_Head = bookARoom.getSummaryHeading();
						String addOnServiceHeading = bookARoom.getAddOnServiceHeading();
						if (addOnServiceHeading.equals("Purchased Add-on Services")) {
							bookARoom.changeToLiveMode("Live Mode");
							bookARoom.clickOnSaveContinueBtn();
							DynamicWait.waitUntil(10);
						} else if (summary_Head.equals("Your Summary")) {
							String summaryHeade = bookARoom.getSummaryHeading();
							if (summaryHeade.equals("Your Summary")) {
								bookARoom.drawYourSign();
								bookARoom.clickOnTermsCondition();
								bookARoom.clickOnSaveContinueBtn();
								DynamicWait.waitUntil(15);
								pre_Checkin_status = bookARoom.isPrecheckinComplete(id);
								System.out.println("Status of the pre-checkin ------- " + pre_Checkin_status);
								numberOfCreditCard = bookARoom.getNumberofCreditCardUploaded(id);
								numberOfDocument = bookARoom.getNumberOfDocumentUploaded(id);
								numberOfUpsell = bookARoom.getnumberofUpsell(id);
								System.out.println("Number of credit card --------- " + numberOfCreditCard);
								System.out.println("Number of document upload -------- " + numberOfDocument);
								System.out.println("Number of Upsell ------------ " + numberOfUpsell);
								bookARoom.openCard(id);
								DynamicWait.waitUntil(10);
								guestEmailAtBookList = bookARoom.guestEmailAtBookingListPage(id);
								numberOfAdult = bookARoom.numberOfAdultGuestAtBookingList(id);
								numberOfChilds = bookARoom.numberOfChildGuestAtBookingList(id);
								paymentType = bookARoom.paymentTypeAtBookingList(id);
								i = i + 1;
							}
						}

						if (i == 0) {
							String summaryHeade = bookARoom.getSummaryHeading();
							if (summaryHeade.equals("Your Summary")) {
								bookARoom.drawYourSign();
								bookARoom.clickOnTermsCondition();
								bookARoom.clickOnSaveContinueBtn();
								DynamicWait.waitUntil(15);
								pre_Checkin_status = bookARoom.isPrecheckinComplete(id);
								System.out.println("Status of the pre-checkin ------- " + pre_Checkin_status);
								numberOfCreditCard = bookARoom.getNumberofCreditCardUploaded(id);
								numberOfDocument = bookARoom.getNumberOfDocumentUploaded(id);
								numberOfUpsell = bookARoom.getnumberofUpsell(id);
								System.out.println("Number of credit card --------- " + numberOfCreditCard);
								System.out.println("Number of document upload -------- " + numberOfDocument);
								System.out.println("Number of Upsell ------------ " + numberOfUpsell);
								bookARoom.openCard(id);
								DynamicWait.waitUntil(10);
								guestEmailAtBookList = bookARoom.guestEmailAtBookingListPage(id);
								numberOfAdult = bookARoom.numberOfAdultGuestAtBookingList(id);
								numberOfChilds = bookARoom.numberOfChildGuestAtBookingList(id);
								paymentType = bookARoom.paymentTypeAtBookingList(id);
							}
						}

						if (pre_Checkin_status) {
							bookARoom.clickOnMenu(id);
							bookARoom.clickOnGuestPortal(id);
							bookARoom.clickOnBookingInformation();
							bookARoom.clickOnEditButton();
							bookARoom.editEmail(guestEmail);
							bookARoom.changeArrivingBy(arrivingBy, "IND7654R");
							bookARoom.changeEstimatedTime(arrivingTime);
							bookARoom.clickOnUpdateAfterEdit();
							DynamicWait.waitUntil(10);
							bookARoom.clickOnEditPaymentDetails();
							int count = bookARoom.isCredit_CardTextBoxPresent();
							if (count > 0) {
								// bookARoom.changeToLiveMode("Live Mode");
								bookARoom.setCredit_Card(cardNumber, expiryDate, cardPin, cardPostalNumber);
								WebDriverFactory.getDriver().switchTo().defaultContent();
								bookARoom.clickOnSaveBtn_AfterEditCreditCard();
								DynamicWait.waitUntil(10);
							}
							creditCardSubmitted = bookARoom.verifyDocumentUploaded();
							System.out.println("Is Credit Card Submitted ----- " + creditCardSubmitted);
							verifySign = bookARoom.verifySignGiven();
							System.out.println("Is Signature given ----------- " + verifySign);
							addOnservice = bookARoom.verifyaddOnService();
							System.out.println("All Add on Services Present ------- " + addOnservice);
						}

						Assert.assertEquals(bookingId, bookId);
						Assert.assertEquals(pre_Checkin_status, true);
						Assert.assertEquals(guestFullName, guestName);
						Assert.assertEquals(numberOfCreditCard, "1");
						Assert.assertEquals(numberOfDocument, "2");
						Assert.assertEquals(numberOfUpsell, "6");
						Assert.assertEquals(guestEmail, guestEmailAtBookList);
						Assert.assertTrue(numberOfAdult.contains(numberOfAdults));
						Assert.assertTrue(numberOfChilds.contains(numberOfChild));
						if (paymentType.equals(cardType)) {
							Assert.assertEquals(paymentType, cardType);
						} else {
							Assert.assertEquals(paymentType, "Credit Card");
						}
						Assert.assertEquals("Credit Card Submitted", creditCardSubmitted);
						Assert.assertEquals(1, verifySign);
						Assert.assertEquals(true, addOnservice);
					} else {
						System.out.println("Registered Booking Id not found for pre-Checkin Form -------- " + bookId);
					}
				}
			}
		}
	}

	@Test(enabled = true, priority = 6, dataProvider = "sendPre_CheckinEmail", dataProviderClass = BookARoomDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void Void_From_Card(String emailId, String pwd, String bookingType, String testAccount, String property,
			String rooms, String bookingSource, String cardType, String guestEmail, String guestComment,
			String goodCard, String emailForMainSite, String pwdForMainSite, String numberOfAdults,
			String numberOfChild, String arrivingBy, String arrivingTime, String numberOfGuest, String cardNumber,
			String expiryDate, String cardPin, String cardPostalNumber) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		BookARoomComponent bookARoom;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			bookARoom = new BookARoomComponent(WebDriverFactory.getDriver());
			bookARoom.clickOnCreateTestBooking();
			String title = bookARoom.getBookingTitle();
			System.out.println("Booking Title ------------ " + title);
			if (title.equals("Bookings")) {
				bookARoom.chooseBookingType(bookingType);
				bookARoom.chooseTestAccount(testAccount);
				bookARoom.chooseProperty(property);
				bookARoom.chooseRooms(rooms);
				bookARoom.chooseBookingSource(bookingSource);
				bookARoom.chooseCardType(cardType);
				bookARoom.setcheckinDateForBooking();
				bookARoom.setPrice("1000");
				bookARoom.enterGuestEmail(guestEmail);
				bookARoom.enterGuestComment(guestComment);
				bookARoom.selectGoodCard(goodCard);
				bookARoom.clickOnSaveBtn();
				String bookId = bookARoom.getBookingId();
				System.out.println("Booking Id --------- " + bookId);
				bookARoom.doLogout();

				loginComponent.clickOnLoginAtHomePage();
				loginComponent.enterEmail(emailForMainSite);
				loginComponent.enterPassword(pwdForMainSite);
				loginComponent.clickOnLogin();
				if (loginComponent.validateLogin()) {
					bookARoom.clickOnBookingFromMenuLink();
					String bookingId = bookARoom.isBookingIdMatched();
					String id = bookARoom.getIdValue();
					if (bookId.equals(bookingId)) {
						bookARoom.openCard(id);
						bookARoom.clickOnVoid(id);
						// bookARoom.doRefund("100", "Test Refund");
						String authorize = bookARoom.authorize_Status(id);
						String paid = bookARoom.paid_Status(id);
						System.out.println("Authorize Status at Test Case --------- " + authorize);
						System.out.println("Paid Status at Test Case -------------- " + paid);
						Assert.assertEquals("Voided", authorize);
						Assert.assertEquals("Voided", paid);
						DynamicWait.waitUntil(5);
					}
				}
			}
		}
	}

	@Test(enabled = true, priority = 7, dataProvider = "sendPre_CheckinEmail", dataProviderClass = BookARoomDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void chargeNow_From_Card(String emailId, String pwd, String bookingType, String testAccount, String property,
			String rooms, String bookingSource, String cardType, String guestEmail, String guestComment,
			String goodCard, String emailForMainSite, String pwdForMainSite, String numberOfAdults,
			String numberOfChild, String arrivingBy, String arrivingTime, String numberOfGuest, String cardNumber,
			String expiryDate, String cardPin, String cardPostalNumber) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		BookARoomComponent bookARoom;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			bookARoom = new BookARoomComponent(WebDriverFactory.getDriver());
			bookARoom.clickOnCreateTestBooking();
			String title = bookARoom.getBookingTitle();
			System.out.println("Booking Title ------------ " + title);
			if (title.equals("Bookings")) {
				bookARoom.chooseBookingType(bookingType);
				bookARoom.chooseTestAccount(testAccount);
				bookARoom.chooseProperty(property);
				bookARoom.chooseRooms(rooms);
				bookARoom.chooseBookingSource(bookingSource);
				bookARoom.chooseCardType(cardType);
				bookARoom.setcheckinDateForBooking();
				bookARoom.setPrice("1000");
				bookARoom.enterGuestEmail(guestEmail);
				bookARoom.enterGuestComment(guestComment);
				bookARoom.selectGoodCard(goodCard);
				bookARoom.clickOnSaveBtn();
				String bookId = bookARoom.getBookingId();
				System.out.println("Booking Id --------- " + bookId);
				bookARoom.doLogout();

				loginComponent.clickOnLoginAtHomePage();
				loginComponent.enterEmail(emailForMainSite);
				loginComponent.enterPassword(pwdForMainSite);
				loginComponent.clickOnLogin();
				if (loginComponent.validateLogin()) {
					bookARoom.clickOnBookingFromMenuLink();
					String bookingId = bookARoom.isBookingIdMatched();
					String id = bookARoom.getIdValue();
					if (bookId.equals(bookingId)) {
						bookARoom.openCard(id);
						bookARoom.chargeNowFromCard(id);
						// bookARoom.doRefund("100", "Test Refund");
						String authorize = bookARoom.authorize_Status(id);
						String paid = bookARoom.paid_Status(id);
						System.out.println("Authorize Status at Test Case --------- " + authorize);
						System.out.println("Paid Status at Test Case -------------- " + paid);
						Assert.assertEquals("Authorized", authorize);
						Assert.assertEquals("Paid", paid);
						DynamicWait.waitUntil(5);
					}
				}
			}
		}
	}

	@Test(enabled = true, priority = 8, dataProvider = "sendPre_CheckinEmail", dataProviderClass = BookARoomDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void changeAmount_And_MarkAsPaid_Authorize_Release_From_Card(String emailId, String pwd, String bookingType,
			String testAccount, String property, String rooms, String bookingSource, String cardType, String guestEmail,
			String guestComment, String goodCard, String emailForMainSite, String pwdForMainSite, String numberOfAdults,
			String numberOfChild, String arrivingBy, String arrivingTime, String numberOfGuest, String cardNumber,
			String expiryDate, String cardPin, String cardPostalNumber) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		BookARoomComponent bookARoom;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			bookARoom = new BookARoomComponent(WebDriverFactory.getDriver());
			bookARoom.clickOnCreateTestBooking();
			String title = bookARoom.getBookingTitle();
			System.out.println("Booking Title ------------ " + title);
			if (title.equals("Bookings")) {
				bookARoom.chooseBookingType(bookingType);
				bookARoom.chooseTestAccount(testAccount);
				bookARoom.chooseProperty(property);
				bookARoom.chooseRooms(rooms);
				bookARoom.chooseBookingSource(bookingSource);
				bookARoom.chooseCardType(cardType);
				bookARoom.setcheckinDateForBooking();
				bookARoom.setPrice("1000");
				bookARoom.enterGuestEmail(guestEmail);
				bookARoom.enterGuestComment(guestComment);
				bookARoom.selectGoodCard(goodCard);
				bookARoom.clickOnSaveBtn();
				String bookId = bookARoom.getBookingId();
				System.out.println("Booking Id --------- " + bookId);
				bookARoom.doLogout();

				loginComponent.clickOnLoginAtHomePage();
				loginComponent.enterEmail(emailForMainSite);
				loginComponent.enterPassword(pwdForMainSite);
				loginComponent.clickOnLogin();
				if (loginComponent.validateLogin()) {
					bookARoom.clickOnBookingFromMenuLink();
					String bookingId = bookARoom.isBookingIdMatched();
					String id = bookARoom.getIdValue();
					if (bookId.equals(bookingId)) {
						bookARoom.openCard(id);
						bookARoom.changeAmountMarkasPaid(id);
						bookARoom.AuthorizeAndRelease(id);
						String authorize = bookARoom.authorize_Status(id);
						String paid = bookARoom.paid_Status(id);
						System.out.println("Authorize Status at Test Case --------- " + authorize);
						System.out.println("Paid Status at Test Case -------------- " + paid);
						Assert.assertEquals("Manually Released", authorize);
						Assert.assertEquals("Paid", paid);
						DynamicWait.waitUntil(5);
					}
				}
			}
		}
	}

	@Test(enabled = true, priority = 9, dataProvider = "sendPre_CheckinEmail", dataProviderClass = BookARoomDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void changeAmount_And_MarkAsPaid_Authorize_Capture_From_Card(String emailId, String pwd, String bookingType,
			String testAccount, String property, String rooms, String bookingSource, String cardType, String guestEmail,
			String guestComment, String goodCard, String emailForMainSite, String pwdForMainSite, String numberOfAdults,
			String numberOfChild, String arrivingBy, String arrivingTime, String numberOfGuest, String cardNumber,
			String expiryDate, String cardPin, String cardPostalNumber) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		BookARoomComponent bookARoom;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			bookARoom = new BookARoomComponent(WebDriverFactory.getDriver());
			bookARoom.clickOnCreateTestBooking();
			String title = bookARoom.getBookingTitle();
			System.out.println("Booking Title ------------ " + title);
			if (title.equals("Bookings")) {
				bookARoom.chooseBookingType(bookingType);
				bookARoom.chooseTestAccount(testAccount);
				bookARoom.chooseProperty(property);
				bookARoom.chooseRooms(rooms);
				bookARoom.chooseBookingSource(bookingSource);
				bookARoom.chooseCardType(cardType);
				bookARoom.setcheckinDateForBooking();
				bookARoom.setPrice("1000");
				bookARoom.enterGuestEmail(guestEmail);
				bookARoom.enterGuestComment(guestComment);
				bookARoom.selectGoodCard(goodCard);
				bookARoom.clickOnSaveBtn();
				String bookId = bookARoom.getBookingId();
				System.out.println("Booking Id --------- " + bookId);
				bookARoom.doLogout();

				loginComponent.clickOnLoginAtHomePage();
				loginComponent.enterEmail(emailForMainSite);
				loginComponent.enterPassword(pwdForMainSite);
				loginComponent.clickOnLogin();
				if (loginComponent.validateLogin()) {
					bookARoom.clickOnBookingFromMenuLink();
					String bookingId = bookARoom.isBookingIdMatched();
					String id = bookARoom.getIdValue();
					if (bookId.equals(bookingId)) {
						bookARoom.openCard(id);
						bookARoom.changeAmountMarkasPaid(id);
						bookARoom.AuthorizeAndCapture(id);
						String authorize = bookARoom.authorize_Status(id);
						String paid = bookARoom.paid_Status(id);
						System.out.println("Authorize Status at Test Case --------- " + authorize);
						System.out.println("Paid Status at Test Case -------------- " + paid);
						Assert.assertEquals("Captured", authorize);
						Assert.assertEquals("Paid", paid);
						DynamicWait.waitUntil(5);
					}
				}
			}
		}
	}

	@Test(enabled = true, priority = 10, dataProvider = "differentPayment", dataProviderClass = BookARoomDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void changeAmount_And_MarkAsPaid_Authorize_Without3D_From_Card(String emailId, String pwd,
			String bookingType, String testAccount, String property, String rooms, String bookingSource,
			String cardType, String guestEmail, String guestComment, String goodCard, String emailForMainSite,
			String pwdForMainSite, String numberOfAdults, String numberOfChild, String arrivingBy, String arrivingTime,
			String numberOfGuest, String cardNumber, String expiryDate, String cardPin, String cardPostalNumber) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		BookARoomComponent bookARoom;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			bookARoom = new BookARoomComponent(WebDriverFactory.getDriver());
			bookARoom.clickOnCreateTestBooking();
			String title = bookARoom.getBookingTitle();
			System.out.println("Booking Title ------------ " + title);
			if (title.equals("Bookings")) {
				bookARoom.chooseBookingType(bookingType);
				bookARoom.chooseTestAccount(testAccount);
				bookARoom.chooseProperty(property);
				bookARoom.chooseRooms(rooms);
				bookARoom.chooseBookingSource(bookingSource);
				bookARoom.chooseCardType(cardType);
				bookARoom.setcheckinDateForBooking();
				bookARoom.setPrice("1000");
				bookARoom.enterGuestEmail(guestEmail);
				bookARoom.enterGuestComment(guestComment);
				bookARoom.selectGoodCard(goodCard);
				bookARoom.clickOnSaveBtn();
				String bookId = bookARoom.getBookingId();
				System.out.println("Booking Id --------- " + bookId);
				bookARoom.doLogout();

				loginComponent.clickOnLoginAtHomePage();
				loginComponent.enterEmail(emailForMainSite);
				loginComponent.enterPassword(pwdForMainSite);
				loginComponent.clickOnLogin();
				if (loginComponent.validateLogin()) {
					bookARoom.clickOnBookingFromMenuLink();
					String bookingId = bookARoom.isBookingIdMatched();
					String id = bookARoom.getIdValue();
					if (bookId.equals(bookingId)) {
						bookARoom.addCreditCardFromBookingList(id);
						bookARoom.isCredit_CardTextBoxPresent();
						bookARoom.setCredit_Card(cardNumber, expiryDate, cardPin, cardPostalNumber);
						WebDriverFactory.getDriver().switchTo().defaultContent();
						bookARoom.updateCreditCard();
						bookARoom.openCard(id);
						// bookARoom.changeAmountMarkasPaid(id);
						bookARoom.AuthorizeNowWithout3D(id);
						String authorize = bookARoom.authorize_Status(id);
						String paid = bookARoom.paid_Status(id);
						System.out.println("Authorize Status at Test Case --------- " + authorize);
						System.out.println("Paid Status at Test Case -------------- " + paid);
						Assert.assertEquals("Paid", paid);
						Assert.assertEquals("Authorized", authorize);
						DynamicWait.waitUntil(5);
					}
				}
			}
		}
	}

	@Test(enabled = true, priority = 11, dataProvider = "differentPayment", dataProviderClass = BookARoomDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void add_AdditionalCharge_From_Card(String emailId, String pwd, String bookingType, String testAccount,
			String property, String rooms, String bookingSource, String cardType, String guestEmail,
			String guestComment, String goodCard, String emailForMainSite, String pwdForMainSite, String numberOfAdults,
			String numberOfChild, String arrivingBy, String arrivingTime, String numberOfGuest, String cardNumber,
			String expiryDate, String cardPin, String cardPostalNumber) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		int beforeaddcardCount = 0;
		int afteraddcardCount = 0;
		BookARoomComponent bookARoom;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			bookARoom = new BookARoomComponent(WebDriverFactory.getDriver());
			bookARoom.clickOnCreateTestBooking();
			String title = bookARoom.getBookingTitle();
			System.out.println("Booking Title ------------ " + title);
			if (title.equals("Bookings")) {
				bookARoom.chooseBookingType(bookingType);
				bookARoom.chooseTestAccount(testAccount);
				bookARoom.chooseProperty(property);
				bookARoom.chooseRooms(rooms);
				bookARoom.chooseBookingSource(bookingSource);
				bookARoom.chooseCardType(cardType);
				bookARoom.setcheckinDateForBooking();
				bookARoom.setPrice("1000");
				bookARoom.enterGuestEmail(guestEmail);
				bookARoom.enterGuestComment(guestComment);
				bookARoom.selectGoodCard(goodCard);
				bookARoom.clickOnSaveBtn();
				String bookId = bookARoom.getBookingId();
				System.out.println("Booking Id --------- " + bookId);
				bookARoom.doLogout();

				loginComponent.clickOnLoginAtHomePage();
				loginComponent.enterEmail(emailForMainSite);
				loginComponent.enterPassword(pwdForMainSite);
				loginComponent.clickOnLogin();
				if (loginComponent.validateLogin()) {
					bookARoom.clickOnBookingFromMenuLink();
					String bookingId = bookARoom.isBookingIdMatched();
					String id = bookARoom.getIdValue();
					if (bookId.equals(bookingId)) {
						bookARoom.addCreditCardFromBookingList(id);
						bookARoom.isCredit_CardTextBoxPresent();
						bookARoom.setCredit_Card(cardNumber, expiryDate, cardPin, cardPostalNumber);
						WebDriverFactory.getDriver().switchTo().defaultContent();
						bookARoom.updateCreditCard();
						bookARoom.openCard(id);
						beforeaddcardCount = bookARoom.totalPaymentCardCount();
						bookARoom.clickOnAdditionalPaymentButton(id);
						bookARoom.additionalChargeAtCard("1000", "Test additional description");
						afteraddcardCount = bookARoom.totalPaymentCardCount();
						Assert.assertTrue(afteraddcardCount > beforeaddcardCount);
						DynamicWait.waitUntil(5);
					}
				}
			}
		}
	}

	@Test(enabled = true, priority = 12, dataProvider = "bookingData", dataProviderClass = BookARoomDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void add_and_Edit_Booking_From_BookingList(String emailId, String pwd, String bookingType, String testAccount,
			String property, String rooms, String bookingSource, String cardType, String guestEmail,
			String guestComment, String goodCard, String emailForMainSite, String pwdForMainSite, String numberOfAdults,
			String numberOfChild, String arrivingBy, String arrivingTime, String numberOfGuest, String cardNumber,
			String expiryDate, String cardPin, String cardPostalNumber) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);

		BookARoomComponent bookARoom;

		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailForMainSite);
		loginComponent.enterPassword(pwdForMainSite);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			bookARoom = new BookARoomComponent(WebDriverFactory.getDriver());
			bookARoom.clickOnBookingFromMenuLink();
			bookARoom.openBookingForm();
			bookARoom.setAssignedRental(property);
			bookARoom.setBookingSource(bookingSource);
			bookARoom.setDates();
			bookARoom.setPriceInForm("1000");
			bookARoom.setNoteInForm("This is test note.");
			bookARoom.setFirstName();
			bookARoom.setLastName();
			bookARoom.setPhoneNumber();
			bookARoom.setEmail();
			bookARoom.setAdults();
			bookARoom.setChild();
			bookARoom.clickOnSaveForms();
			String id = bookARoom.getIdValue();
			System.out.println("Id from first booking row -------- " + id);
			bookARoom.clickOnMenu(id);
			bookARoom.clickOnEdit_Booking(id);
			bookARoom.setAssignedRental(property);
			bookARoom.setBookingSource(bookingSource);
			bookARoom.setDates();
			bookARoom.setPriceInForm("1000");
			bookARoom.setNoteInForm("This is test note.");
			bookARoom.setFirstName();
			bookARoom.setLastName();
			bookARoom.setPhoneNumber();
			bookARoom.setEmail();
			bookARoom.setAdults();
			bookARoom.setChild();
			bookARoom.clickOnSaveForms();
		}
	}
	
	@Test(enabled = true, priority = 13, dataProvider = "bookingData", dataProviderClass = BookARoomDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void paused_confirmed_Booking_From_BookingList(String emailId, String pwd, String bookingType, String testAccount,
			String property, String rooms, String bookingSource, String cardType, String guestEmail,
			String guestComment, String goodCard, String emailForMainSite, String pwdForMainSite, String numberOfAdults,
			String numberOfChild, String arrivingBy, String arrivingTime, String numberOfGuest, String cardNumber,
			String expiryDate, String cardPin, String cardPostalNumber) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);

		BookARoomComponent bookARoom;
		String pausedStatus = null;
		String confirmStatus = null;

		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailForMainSite);
		loginComponent.enterPassword(pwdForMainSite);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			bookARoom = new BookARoomComponent(WebDriverFactory.getDriver());
			bookARoom.clickOnBookingFromMenuLink();
			bookARoom.openBookingForm();
			bookARoom.setAssignedRental(property);
			bookARoom.setBookingSource(bookingSource);
			bookARoom.setDates();
			bookARoom.setPriceInForm("1000");
			bookARoom.setNoteInForm("This is test note.");
			bookARoom.setFirstName();
			bookARoom.setLastName();
			bookARoom.setPhoneNumber();
			bookARoom.setEmail();
			bookARoom.setAdults();
			bookARoom.setChild();
			bookARoom.clickOnSaveForms();
			String id = bookARoom.getIdValue();
			System.out.println("Id from first booking row -------- " + id);
			bookARoom.clickOnMenu(id);
			bookARoom.clickOnEdit_Booking(id);
			bookARoom.changeBookingStatus("Paused");
			bookARoom.clickOnSaveForms();
			pausedStatus = bookARoom.getBookingStatus(id);
			bookARoom.clickOnMenu(id);
			bookARoom.clickOnEdit_Booking(id);
			bookARoom.changeBookingStatus("Confirmed");
			bookARoom.clickOnSaveForms();
			confirmStatus = bookARoom.getBookingStatus(id);
			Assert.assertEquals("Paused", pausedStatus);
			Assert.assertEquals("Confirmed", confirmStatus);
		}
	}
	
	@Test(enabled = true, priority = 14, dataProvider = "bookingData", dataProviderClass = BookARoomDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void cancel_Booking_From_BookingList(String emailId, String pwd, String bookingType, String testAccount,
			String property, String rooms, String bookingSource, String cardType, String guestEmail,
			String guestComment, String goodCard, String emailForMainSite, String pwdForMainSite, String numberOfAdults,
			String numberOfChild, String arrivingBy, String arrivingTime, String numberOfGuest, String cardNumber,
			String expiryDate, String cardPin, String cardPostalNumber) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);

		BookARoomComponent bookARoom;
		String cancelStatus = null;

		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailForMainSite);
		loginComponent.enterPassword(pwdForMainSite);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			bookARoom = new BookARoomComponent(WebDriverFactory.getDriver());
			bookARoom.clickOnBookingFromMenuLink();
			bookARoom.openBookingForm();
			bookARoom.setAssignedRental(property);
			bookARoom.setBookingSource(bookingSource);
			bookARoom.setDates();
			bookARoom.setPriceInForm("1000");
			bookARoom.setNoteInForm("This is test note.");
			bookARoom.setFirstName();
			bookARoom.setLastName();
			bookARoom.setPhoneNumber();
			bookARoom.setEmail();
			bookARoom.setAdults();
			bookARoom.setChild();
			bookARoom.clickOnSaveForms();
			String id = bookARoom.getIdValue();
			System.out.println("Id from first booking row -------- " + id);
			bookARoom.clickOnMenu(id);
			bookARoom.clickOnEdit_Booking(id);
			bookARoom.changeBookingStatus("Cancelled");
			bookARoom.clickOnSaveForms();
			cancelStatus = bookARoom.getBookingStatus(id);
			
			Assert.assertEquals("Cancelled", cancelStatus);
		}
	}
	
	@Test(enabled = true, priority = 15, dataProvider = "chargewithout3D", dataProviderClass = BookARoomDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void charge_now_without3D(String emailId, String pwd, String bookingType, String testAccount,
			String property, String rooms, String bookingSource, String cardType, String guestEmail,
			String guestComment, String goodCard, String emailForMainSite, String pwdForMainSite, String numberOfAdults,
			String numberOfChild, String arrivingBy, String arrivingTime, String numberOfGuest, String cardNumber,
			String expiryDate, String cardPin, String cardPostalNumber) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);

		BookARoomComponent bookARoom;

		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailForMainSite);
		loginComponent.enterPassword(pwdForMainSite);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			bookARoom = new BookARoomComponent(WebDriverFactory.getDriver());
			bookARoom.clickOnBookingFromMenuLink();
			bookARoom.openBookingForm();
			bookARoom.setAssignedRental(property);
			bookARoom.setBookingSource(bookingSource);
			bookARoom.setDates();
			bookARoom.setPriceInForm("1000");
			bookARoom.setNoteInForm("This is test note.");
			bookARoom.setFirstName();
			bookARoom.setLastName();
			bookARoom.setPhoneNumber();
			bookARoom.setEmail();
			bookARoom.setAdults();
			bookARoom.setChild();
			bookARoom.clickOnSaveForms();
			String id = bookARoom.getIdValue();
			System.out.println("Id from first booking row -------- " + id);
			bookARoom.openCard(id);
			bookARoom.charge_and_AuthorizeNowWithout3D(id);
			String authorize = bookARoom.authorize_Status(id);
			String paid = bookARoom.paid_Status(id);
			System.out.println("Authorize Status at Test Case --------- " + authorize);
			System.out.println("Paid Status at Test Case -------------- " + paid);
			Assert.assertEquals("Paid", paid);
			Assert.assertEquals("Authorized", authorize);
			DynamicWait.waitUntil(5);
		}
	}

}
