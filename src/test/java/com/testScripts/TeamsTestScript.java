package com.testScripts;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.driverFactory.WebDriverFactory;
import com.page.LoginComponent;
import com.page.ProfileUpdateComponent;
import com.page.TeamsComponent;
import com.utilities.CredentialDataProvider;
import com.utilities.ExtentReport;
import com.utilities.UpdateProfileDataProvider;

public class TeamsTestScript extends BaseTest {

	//public static HashMap<String, ArrayList<String>> teamsEmailList = new HashMap<String, ArrayList<String>>();

	@Test(enabled =true, priority = 1, dataProvider = "TeamsData", dataProviderClass = UpdateProfileDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void createTeams(String emailId, String pwd, String firstName, String lastname, String teamemails,
			String phoneNum, String role, String accountSetup, String bookings, String experience, String preferences,
			String property) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		TeamsComponent teamsComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		boolean status = loginComponent.validateLogin();

		if (status) {
			loginComponent.clickOnDashBoard();
			System.out.println("Key -------------- " + TeamsComponent.teamsEmailList.get(emailId));
			System.out.println("Value ------------ " + TeamsComponent.teamsEmailList.values());
			teamsComponent = new TeamsComponent(WebDriverFactory.getDriver());
			teamsComponent.clickOnTeams();
			teamsComponent.clickOnAddTeams();
			// teamsComponent.getCardId();
			// teamsComponent.getEmailsTextFromCard();
			teamsComponent.sendTeamProfileIcon();
			if (!firstName.equals("null")) {
				teamsComponent.enterTeamsFirstName(firstName);
			}
			if (!lastname.equals("null")) {
				teamsComponent.enterTeamLastName(lastname);
			}
			if (!teamemails.equals("null")) {
				teamsComponent.enterTeamEmail(teamemails);
			}
			if (!phoneNum.equals("null")) {
				teamsComponent.enterTeamPhoneNumber(phoneNum);
			}
			if (!role.equals("null")) {
				teamsComponent.selectTeamRole(role);
			}
			if (!accountSetup.equals("null")) {
				teamsComponent.getAccountSetup();
			}
			if (!bookings.equals("null")) {
				teamsComponent.getBookings();
			}
			if (!experience.equals("null")) {
				teamsComponent.getGuestExperience();
			}
			if (!preferences.equals("null")) {
				teamsComponent.getPreferences();
			}
			if (!property.equals("null")) {
				teamsComponent.getProperties();
			}
			teamsComponent.clickOnSendInvitation();
			String errorMsg = teamsComponent.validateForm();
			if (errorMsg.equals("null")) {
				ProfileUpdateComponent updateComponent = new ProfileUpdateComponent(WebDriverFactory.getDriver());
				String successMsg = updateComponent.getToasterMsg();
				System.out.println("Success Message --------- " + successMsg);
				//Assert.assertEquals(successMsg, Constant.TEAM_CREATE_SUCCESS);
				ExtentReport.extentTest.info("User Email Id ----- " + emailId);
				ExtentReport.extentTest.info("Uploaded profile image of team member.");
				ExtentReport.extentTest.info("Team first name ----- " + firstName);
				ExtentReport.extentTest.info("Team last name ------- " + lastname);
				ExtentReport.extentTest.info("Team email id -------- " + teamemails);
				ExtentReport.extentTest.info("Team phone number ------- " + phoneNum);
				ExtentReport.extentTest.info("Team Role --------- " + role);
				ExtentReport.extentTest.info("Team Given access to Account Setup ----- " + accountSetup);
				ExtentReport.extentTest.info("Team Given access to Booking -------- " + bookings);
				ExtentReport.extentTest.info("Team Given access to Guest Experience ------- " + experience);
				ExtentReport.extentTest.info("Team Given access to Preferences ------------ " + preferences);
				ExtentReport.extentTest.info("Team Given access to Property --------- " + property);
			} else {
				System.out.println("Error Message ----------- " + errorMsg);
				ExtentReport.extentTest.warning("Error Message ------------ " + errorMsg);
				ExtentReport.extentTest.info("Actual First Name sent -------- " + firstName);
				ExtentReport.extentTest.info("Actual Last name sent --------- " + lastname);
				ExtentReport.extentTest.info("Actual Email Id sent ---------- " + teamemails);
				ExtentReport.extentTest.info("Actual Phone number sent -------- " + phoneNum);
				ExtentReport.extentTest.info("Actual Role sent ------- " + role);
				ExtentReport.extentTest.info("Actual Given access to Account Setup --------- " + accountSetup);
				ExtentReport.extentTest.info("Actual Given access to Booking ----------- " + bookings);
				ExtentReport.extentTest.info("Actual Given access to Guest Experience ---------- " + experience);
				ExtentReport.extentTest.info("Actual Given access to Preferences ----------- " + preferences);
				ExtentReport.extentTest.info("Actual Given access to Property ----------- " + property);
			}
		}
		System.out.println("Teams Emails IDs ------- " + TeamsComponent.teamsEmailList);
	}

	@Test(enabled =true, priority = 2, dataProvider = "validCredentialData", dataProviderClass = CredentialDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void resendInvitationToTeam(String emailId, String pwd) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		TeamsComponent teamsComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		boolean status = loginComponent.validateLogin();

		if (status) {
			loginComponent.clickOnDashBoard();
			teamsComponent = new TeamsComponent(WebDriverFactory.getDriver());
			teamsComponent.clickOnTeams();
			// teamsComponent.getEmailsTextFromCard();
			teamsComponent.teamResendInvitation(emailId);
		}
	}

	@Test(enabled =true, priority = 3, dataProvider = "validCredentialData", dataProviderClass = CredentialDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void activate_deactivate_Team(String emailId, String pwd) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		TeamsComponent teamsComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		boolean status = loginComponent.validateLogin();
		if (status) {
			loginComponent.clickOnDashBoard();
			teamsComponent = new TeamsComponent(WebDriverFactory.getDriver());
			teamsComponent.clickOnTeams();
			teamsComponent.teamActivateDeactivate(emailId);
		}
	}

	@Test(enabled =true, priority = 4, dataProvider = "validCredentialData", dataProviderClass = CredentialDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void edit_Teams_Member(String emailId, String pwd) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		TeamsComponent teamsComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		boolean status = loginComponent.validateLogin();
		if (status) {
			loginComponent.clickOnDashBoard();
			teamsComponent = new TeamsComponent(WebDriverFactory.getDriver());
			teamsComponent.clickOnTeams();
			teamsComponent.editTeams(emailId);
		}
	}

	@Test(enabled =true, priority = 5, dataProvider = "validCredentialData", dataProviderClass = CredentialDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void delete_Teams_Member(String emailId, String pwd) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		TeamsComponent teamsComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		boolean status = loginComponent.validateLogin();
		if (status) {
			loginComponent.clickOnDashBoard();
			teamsComponent = new TeamsComponent(WebDriverFactory.getDriver());
			teamsComponent.clickOnTeams();
			teamsComponent.deleteTeams(emailId);
		}
	}

}
