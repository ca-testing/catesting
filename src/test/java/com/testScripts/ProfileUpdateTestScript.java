package com.testScripts;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.driverFactory.WebDriverFactory;
import com.page.LoginComponent;
import com.page.ProfileUpdateComponent;
import com.utilities.ExtentReport;
import com.utilities.UpdateProfileDataProvider;

public class ProfileUpdateTestScript extends BaseTest {

	@Test(enabled = true, dataProvider = "ProfileUpdateDataProvider", dataProviderClass = UpdateProfileDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void updateUserProfile(String emailId, String pwd, String userName, String phoneNumber, String address,
			String companyName, String companyEmail, String companyAddress) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		ProfileUpdateComponent profileUpdateComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		boolean status = loginComponent.validateLogin();
		if (status) {
			//loginComponent.clickOnDashBoard();
			profileUpdateComponent = new ProfileUpdateComponent(WebDriverFactory.getDriver());
			profileUpdateComponent.clickOnMenu();
			profileUpdateComponent.clickOnProfileUpdate();
			profileUpdateComponent.uploadUserImage();
			String userIcon = profileUpdateComponent.getToasterMsg();
			System.out.println("User Icon success MSG ----- > " + userIcon);
			profileUpdateComponent.updateName(userName);
			String userEmailId = profileUpdateComponent.getEmailId();
			System.out.println("User Email at Test script -------- " + userEmailId);
			profileUpdateComponent.updateConatactNumber(phoneNumber);
			profileUpdateComponent.updateAddress(address);
			profileUpdateComponent.uploadCompanyImage();
			String companyIcon = profileUpdateComponent.getToasterMsg();
			System.out.println("Company Icon success MSG ----- > " + companyIcon);
			profileUpdateComponent.updateCompanyName(companyName);
			profileUpdateComponent.updateCompanyEmail(companyEmail);
			profileUpdateComponent.updateCompanyAddress(companyAddress);
			profileUpdateComponent.scrollToSaveChange();
			profileUpdateComponent.clickOnSaveChanges();
			String updateStatus = profileUpdateComponent.validateUpdateForm();
			if (updateStatus.equals("null")) {
				//Assert.assertEquals(userEmailId, emailId);
				String isProfileUpdate = profileUpdateComponent.getToasterMsg();
				System.out.println("Profile Update Toaster Message ------ " + isProfileUpdate);
				System.out.println("Profile updated successfully......!!!");
				ExtentReport.extentTest.pass("Profile updated successfully......!!!");
				ExtentReport.extentTest.info("Email Id ----- " + emailId);
				ExtentReport.extentTest.info("Profile Image ----- " + userIcon);
				ExtentReport.extentTest.info("Updated user name ----- " + userName);
				ExtentReport.extentTest.info("Updated user phone number ------ " + phoneNumber);
				ExtentReport.extentTest.info("Updated address ------ " + address);
				ExtentReport.extentTest.info("Company Logo ------ " + companyIcon);
				ExtentReport.extentTest.info("Updated Company Name ------- " + companyName);
				ExtentReport.extentTest.info("Updated Company Email Id ------ " + companyEmail);
				ExtentReport.extentTest.info("Updated Company Address ------- " + companyAddress);
			} else {
				System.out.println("Update Status ---------- " + updateStatus);
				ExtentReport.extentTest.warning(updateStatus);
				ExtentReport.extentTest.info("Actual user name sent ---- " + userName);
				ExtentReport.extentTest.info("Actual user phone number sent ----- " + phoneNumber);
				ExtentReport.extentTest.info("Actual user address sent ----- " + address);
				ExtentReport.extentTest.info("Actaul Company Name sent ------- " + companyName);
				ExtentReport.extentTest.info("Actual Company Email Id sent ------- " + companyEmail);
				ExtentReport.extentTest.info("Actual Company address sent ------- " + companyAddress);
			}
		} else {
			System.out.println("User unable to login....!");
			System.out.println("Wrong username or password....!!!");
			ExtentReport.extentTest.warning("User unable to login ........!");
			ExtentReport.extentTest.warning("Wrong username or password........!!!");
		}
	}

}
