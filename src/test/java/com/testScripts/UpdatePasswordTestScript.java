package com.testScripts;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.driverFactory.WebDriverFactory;
import com.page.ChangePasswordComponent;
import com.page.LoginComponent;
import com.utilities.Constant;
import com.utilities.ExtentReport;
import com.utilities.UpdateProfileDataProvider;

public class UpdatePasswordTestScript extends BaseTest {

	@Test(enabled = true, dataProvider = "passwordUpdateDataProvider", dataProviderClass = UpdateProfileDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void verifyUpdatePassword(String emailId, String pwd, String oldPwd, String newPwd, String confirmPwd) {
		LoginComponent login = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		ChangePasswordComponent changePasswordComponent;
		login.clickOnLoginAtHomePage();
		login.enterEmail(emailId);
		login.enterPassword(pwd);
		login.clickOnLogin();
		boolean status = login.validateLogin();

		if (status) {
			login.clickOnDashBoard();
			changePasswordComponent = new ChangePasswordComponent(WebDriverFactory.getDriver());
			changePasswordComponent.clickOnChangePwdMenu();
			changePasswordComponent.changePassword(oldPwd, newPwd, confirmPwd);
			String errorMsg = changePasswordComponent.validateErrorMsg();
			if (errorMsg.equals("null")) {
				String successMsg = changePasswordComponent.isPasswordUpdate();
				Assert.assertEquals(successMsg, Constant.PASSWORD_UPDATE_SUCCESS);
				System.out.println("Success Message ----------- " + successMsg);
				ExtentReport.extentTest.info("Users Email Id is -------------- " + emailId);
			} else {
				System.out.println("Got the error and Error Message is ----------- " + errorMsg);
				ExtentReport.extentTest.warning("Error Msg is ----------" + errorMsg);
				ExtentReport.extentTest.info("Users Email Id is -------------- " + emailId);
				ExtentReport.extentTest.info("Old Password provided ----------- " + oldPwd);
				ExtentReport.extentTest.info("New Password provided ------------- " + newPwd);
				ExtentReport.extentTest.info("Confirm Password Provided ------------ " + confirmPwd);
			}
		} else {
			System.out.println("User unable to login......!");
			System.out.println("Wrong user id or password.....");
			ExtentReport.extentTest.info("User unable to login......!");
			ExtentReport.extentTest.info("Wrong user id or password......!");
			ExtentReport.extentTest.info("User Email Id -------- " + emailId);
		}
	}

}
