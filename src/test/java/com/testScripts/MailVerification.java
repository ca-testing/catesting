package com.testScripts;

import org.testng.annotations.Test;

import com.driverFactory.WebDriverFactory;
import com.page.VerifyEmailMailsacComponent;
import com.utilities.Constant;

public class MailVerification extends BaseTest {

	@Test
	public void verifyEmail() {
		String emailId = "testautomation@mailsac.com";
		VerifyEmailMailsacComponent mailsacComponent = new VerifyEmailMailsacComponent(WebDriverFactory.getDriver());
		WebDriverFactory.getDriver().get("https://mailsac.com/inbox/" + emailId);
		boolean emailRecieved = mailsacComponent.isEmailRecieved(emailId, Constant.MAILSAC_FORGOT_PWD_MAIL);
		if (emailRecieved) {
			mailsacComponent.clickOnVerifyNowLink(Constant.MAILSAC_FORGOT_PWD_MAIL);
			//mailsacComponent.loginToMailsac("shabaz.softronet@gmail.com", "ChargeAutomation@123");
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//mailsacComponent.clickOnVerifyNowLink(Constant.MAILSAC_FORGOT_PWD_MAIL);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mailsacComponent.openEmailInBrowser(Constant.MAILSAC_FORGOT_PWD_MAIL);
			mailsacComponent.clickOnResetNow();
		}
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
