package com.testScripts;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.driverFactory.WebDriverFactory;
import com.page.LoginComponent;
import com.page.PaymentRequestComponent;
import com.utilities.Constant;
import com.utilities.DynamicWait;
import com.utilities.ExtentReport;
import com.utilities.PaymentRequestDataProvider;

public class PaymentRequestTestScript extends BaseTest {

	@Test(enabled = true, priority = 1, invocationCount = 1, dataProvider = "createPaymentRequest", dataProviderClass = PaymentRequestDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void createRequest(String emailId, String pwd, String guestEmail, String amount, String currency,
			String description) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		PaymentRequestComponent paymentRequestComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			// loginComponent.clickOnDashBoard();
			paymentRequestComponent = new PaymentRequestComponent(WebDriverFactory.getDriver());
			paymentRequestComponent.clickOnPaymentRequest();
			paymentRequestComponent.createRequest();
			paymentRequestComponent.enterEmail_inForm(guestEmail);
			paymentRequestComponent.enterAmount(amount);
			if (currency.length() > 0) {
				paymentRequestComponent.changeCurrency(currency);
			}
			paymentRequestComponent.enterDescription(description);
			paymentRequestComponent.clickOnPaymentLink();
			String formStatus = paymentRequestComponent.validatePaymentForm();
			if (formStatus.equals("null")) {
				String paymentLink = paymentRequestComponent.getFromLink();
				String paymentRequestId = paymentRequestComponent.getRequestId_FromLink(paymentLink);
				paymentRequestComponent.clickOnPaymentRequest();
				paymentRequestComponent.clickOnManageRequest();
				String chargePaymentRequest = paymentRequestComponent.verifyIcon(3);
				String amounts = paymentRequestComponent.getAmount(3);
				String pending = paymentRequestComponent.verifyPendingAfterCreateRequest();
				String requestId = paymentRequestComponent.getRequestId(3);
				String emails = paymentRequestComponent.getEmailId(3);
				String createDate = paymentRequestComponent.getCreatedDate(3);
				String currentDate = paymentRequestComponent.getcurrentDate();
				System.out.println("Current Date -------- " + currentDate);
				Assert.assertEquals(chargePaymentRequest, "Dollar");
				Assert.assertEquals(amounts, amount);
				Assert.assertEquals(pending, Constant.STATUS_PENDING);
				Assert.assertEquals(requestId, paymentRequestId);
				Assert.assertEquals(emails, guestEmail);
				// Assert.assertEquals(createDate, currentDate);
				paymentRequestComponent.payment(paymentLink);
				paymentRequestComponent.payAll();
				String msg = paymentRequestComponent.getSuccessMsg();
				String msgs = paymentRequestComponent.isPaymentSuccess();
				if (msgs.equals(Constant.PAYMENT_SUCCESS)) {
					ExtentReport.extentTest.info("User ----------- " + emailId);
					ExtentReport.extentTest.info("Create Payment Request Starts --------------- ");
					ExtentReport.extentTest.info("Guest Email Id ----------- " + guestEmail);
					ExtentReport.extentTest.info("Guest needs to Paid ----------- " + amount);
					if (currency.length() > 0) {
						ExtentReport.extentTest.info("Guest needs to pay in " + currency + " Currency");
					}
					ExtentReport.extentTest.info("Payment Description --------- " + description);
					ExtentReport.extentTest.info("Request id for the payment request ------ " + requestId);
					ExtentReport.extentTest.info("Payment Request Created Date --------- " + createDate);
				} else {
					ExtentReport.extentTest.info("User ------------- " + emailId);
					ExtentReport.extentTest.warning("Status of the Payment -------------- " + msg);
				}

			} else {
				ExtentReport.extentTest.info("User ----------- " + emailId);
				ExtentReport.extentTest.warning("Status of the Process ---------- " + formStatus);
			}
		}
	}

	@Test(enabled = true, priority = 2, invocationCount = 2, dataProvider = "createPaymentRequest", dataProviderClass = PaymentRequestDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void createPaymentLinkAndSentEmail(String emailId, String pwd, String guestEmail, String amount,
			String currency, String description) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		PaymentRequestComponent paymentRequestComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			// loginComponent.clickOnDashBoard();
			paymentRequestComponent = new PaymentRequestComponent(WebDriverFactory.getDriver());
			paymentRequestComponent.clickOnPaymentRequest();
			paymentRequestComponent.createRequest();
			paymentRequestComponent.enterEmail_inForm(guestEmail);
			paymentRequestComponent.enterAmount(amount);
			if (currency.length() > 0) {
				paymentRequestComponent.changeCurrency(currency);
			}
			paymentRequestComponent.enterDescription(description);
			paymentRequestComponent.clickOnCreatePaymentLink();
			String formStatus = paymentRequestComponent.validatePaymentForm();
			if (formStatus.equals("null")) {
				String msg = paymentRequestComponent.create_PaymentLink();
				System.out.println("Create Payment Link Message ---------- " + msg);
				if (msg.equals(Constant.PAYMENT_LINK_CREATE_SUCCESS)) {
					paymentRequestComponent.sendEmailPaymentLink();
					String paymentLink = paymentRequestComponent.getFromLink();
					String paymentRequestId = paymentRequestComponent.getRequestId_FromLink(paymentLink);
					paymentRequestComponent.clickOnPaymentRequest();
					paymentRequestComponent.clickOnManageRequest();
					String status = paymentRequestComponent.verifyPendingAfterCreateRequest();
					String chargePaymentRequest = paymentRequestComponent.verifyIcon(3);
					String amounts = paymentRequestComponent.getAmount(3);
					String requestId = paymentRequestComponent.getRequestId(3);
					String emails = paymentRequestComponent.getEmailId(3);
					String createDate = paymentRequestComponent.getCreatedDate(3);
					String currentDate = paymentRequestComponent.getcurrentDate();
					Assert.assertEquals(chargePaymentRequest, "Dollar");
					Assert.assertEquals(amounts, amount);
					Assert.assertEquals(requestId, paymentRequestId);
					Assert.assertEquals(emails, guestEmail);
					// Assert.assertEquals(createDate, currentDate);
					System.out.println("Current Date ------------ " + currentDate);
					Assert.assertEquals(status, Constant.STATUS_PENDING);
					ExtentReport.extentTest.info("User ---------- " + emailId);
					ExtentReport.extentTest.info("Create Payment Request Link Process Start --------- ");
					ExtentReport.extentTest.info("Request Id ---------- " + requestId);
					ExtentReport.extentTest.info("Guest Email Id ------------ " + guestEmail);
					ExtentReport.extentTest.info("Guest needs to Paid -------- " + amount);
					if (currency.length() > 0) {
						ExtentReport.extentTest.info("Guest needs to pay in " + currency + " Currency");
					}
					ExtentReport.extentTest.info("Payment Description --------- " + description);
					ExtentReport.extentTest.info("Status of the Payment --------- " + status);
					ExtentReport.extentTest.info("Payment Request Created Date --------- " + createDate);
				}
			} else {
				ExtentReport.extentTest.info("User ------------ " + emailId);
				ExtentReport.extentTest.warning("Status of the Process ---------- " + formStatus);
			}
		}
	}

	@Test(enabled = true, invocationCount = 2, priority = 6, dataProvider = "paymentCredential", dataProviderClass = PaymentRequestDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void getPayment(String emailId, String pwd) {
		String charge = null;
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		PaymentRequestComponent paymentRequestComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			// loginComponent.clickOnDashBoard();
			paymentRequestComponent = new PaymentRequestComponent(WebDriverFactory.getDriver());
			paymentRequestComponent.clickOnPaymentRequest();
			paymentRequestComponent.clickOnManageRequest();
			int row = paymentRequestComponent.paymentBegins(Constant.STATUS_PENDING);
			if (row > 0) {
				paymentRequestComponent.payPayment();
				charge = paymentRequestComponent.getToastMsg();
				if (charge.equals(Constant.PAYMENT_CHARGED_SUCCESS)) {
					String status = paymentRequestComponent.getPaymentStatus(row);
					String requestId = paymentRequestComponent.getRequestId(row);
					System.out.println("Request Id for the Payment Request ----------- " + requestId);
					System.out.println("Payment Status After Payment ---------- " + status);
					if (status.equals(Constant.AUTHORIZE)) {
						Assert.assertEquals(status, Constant.AUTHORIZE);
					} else {
						Assert.assertEquals(status, Constant.STATUS_PAID);
					}

					ExtentReport.extentTest.info("User ------------ " + emailId);
					ExtentReport.extentTest.info("Payment Process Starts ----------- ");
					ExtentReport.extentTest.info("Request id for the payment request ------ " + requestId);
					ExtentReport.extentTest.info("Status After Payment ------ " + status);
				} else {
					ExtentReport.extentTest.info("User ------------ " + emailId);
					ExtentReport.extentTest.warning("Payment Status --------- " + charge);
				}

			} else {
				ExtentReport.extentTest.info("User --------- " + emailId);
				ExtentReport.extentTest.info("Pending Status not found in the List");
			}
		}
	}

	@Test(enabled = true, priority = 7, invocationCount = 1, dataProvider = "paymentCredential", dataProviderClass = PaymentRequestDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void markAsPaid(String emailId, String Pwd) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		PaymentRequestComponent paymentRequestComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(Pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			// loginComponent.clickOnDashBoard();
			paymentRequestComponent = new PaymentRequestComponent(WebDriverFactory.getDriver());
			paymentRequestComponent.clickOnPaymentRequest();
			paymentRequestComponent.clickOnManageRequest();
			int row = paymentRequestComponent.markasPaid(Constant.STATUS_PENDING);
			if (row > 0) {
				String status = paymentRequestComponent.getPaymentStatus(row);
				String requestId = paymentRequestComponent.getRequestId(row);
				System.out.println("Request Id for the Payment Request --------- " + requestId);
				System.out.println("Payment Status After Marked as Payment --------- " + status);
				Assert.assertEquals(status, Constant.MARK_AS_PAID);

				ExtentReport.extentTest.info("User ------------" + emailId);
				ExtentReport.extentTest.info("Mark as Paid Process Starts ---------- ");
				ExtentReport.extentTest.info("RequestId for the Payment Request --------- " + requestId);
				ExtentReport.extentTest.info("Status After Payment Mark As paid -------- " + status);
			} else {
				ExtentReport.extentTest.info("User ---------- " + emailId);
				ExtentReport.extentTest.info("Pending Status not found in the list");
			}
		}
	}

	@Test(enabled = true, priority = 6, invocationCount = 1, dataProvider = "paymentCredential", dataProviderClass = PaymentRequestDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void sendPaymentLinkAgain(String emailId, String pwd) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		PaymentRequestComponent paymentRequestComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			// loginComponent.clickOnDashBoard();
			paymentRequestComponent = new PaymentRequestComponent(WebDriverFactory.getDriver());
			paymentRequestComponent.clickOnPaymentRequest();
			paymentRequestComponent.clickOnManageRequest();
			int row = paymentRequestComponent.sendRequestAgain(Constant.STATUS_PENDING);
			if (row > 0) {
				// String successMsg = paymentRequestComponent.getPaymentlinksuccess_Msg();
				String status = paymentRequestComponent.getPaymentStatus(row);
				String requestId = paymentRequestComponent.getRequestId(row);
				System.out.println("Request Id of the payment --------- " + requestId);
				System.out.println("Payment Status After Send Request Again ------------- " + status);
				Assert.assertEquals(status, Constant.STATUS_PENDING);
				// Assert.assertEquals(successMsg, Constant.PAYMENT_LINK_SUCCESS);
				ExtentReport.extentTest.info("User ---------- " + emailId);
				ExtentReport.extentTest.info("Send Payment Link agian to guest -------- ");
				ExtentReport.extentTest.info("Request Id of the Payment Request ---------- " + requestId);
				ExtentReport.extentTest.info("Status After resend the link to guest ---------- " + status);

			} else {
				ExtentReport.extentTest.info("User ---------- " + emailId);
				ExtentReport.extentTest.info("Pending Status not found in list");
			}
		}
	}

	@Test(enabled = true, priority = 5, dataProvider = "createPaymentRequest", dataProviderClass = PaymentRequestDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void editPaymentDetails(String emailId, String pwd, String guestEmail, String amount, String currency,
			String description) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		PaymentRequestComponent paymentRequestComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			// loginComponent.clickOnDashBoard();
			paymentRequestComponent = new PaymentRequestComponent(WebDriverFactory.getDriver());
			paymentRequestComponent.clickOnPaymentRequest();
			paymentRequestComponent.clickOnManageRequest();
			int row = paymentRequestComponent.editPaymentRequests(Constant.STATUS_PENDING);
			if (row > 0) {
				paymentRequestComponent.enterAmount(amount);
				paymentRequestComponent.enterDescription(description);
				paymentRequestComponent.clickOnPaymentLink();
				paymentRequestComponent.getFromLink();
				String status = paymentRequestComponent.getPaymentStatus(row);
				String requestId = paymentRequestComponent.getRequestId(row);
				System.out.println("Payment Status After Edit the Payment request ---- " + status);
				System.out.println("Request Id for the Payment Request ------- " + requestId);
				Assert.assertEquals(status, Constant.STATUS_PENDING);

				ExtentReport.extentTest.info("User ------------ " + emailId);
				ExtentReport.extentTest.info("Edit Payment Request Starts ----------- ");
				ExtentReport.extentTest.info("Request Id for the payment Request -------- " + requestId);
				ExtentReport.extentTest.info("Guest Email Id ----------- " + guestEmail);
				ExtentReport.extentTest.info("Amount Edit ----------- " + amount);
				ExtentReport.extentTest.info("Payment Description Edited -------- " + description);
				ExtentReport.extentTest.info("Payment Status after edit the Payment Request ------- " + status);

			} else {
				ExtentReport.extentTest.info("User ---------- " + emailId);
				ExtentReport.extentTest.info("Pending Status not found in the list");
			}
		}
	}

	@Test(enabled = true, priority = 5, invocationCount = 1, dataProvider = "paymentCredential", dataProviderClass = PaymentRequestDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void voidPayment(String emailId, String pwd) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		PaymentRequestComponent paymentRequestComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			// loginComponent.clickOnDashBoard();
			paymentRequestComponent = new PaymentRequestComponent(WebDriverFactory.getDriver());
			paymentRequestComponent.clickOnPaymentRequest();
			paymentRequestComponent.clickOnManageRequest();
			int row = paymentRequestComponent.voidPayment2(Constant.STATUS_PENDING);
			if (row > 0) {
				String status = paymentRequestComponent.getPaymentStatus(row);
				String requestId = paymentRequestComponent.getRequestId(row);
				System.out.println("Request Id for the Payment Request --------- " + requestId);
				System.out.println("Payment Status After void Payment ------------- " + status);
				Assert.assertEquals(status, Constant.STATUS_VOIDED);

				ExtentReport.extentTest.info("User -------------- " + emailId);
				ExtentReport.extentTest.info("Void Payment Starts -------- ");
				ExtentReport.extentTest.info("Request Id for the Payment Request ---------- " + requestId);
				ExtentReport.extentTest.info("Status After void payment -------- " + status);

			} else {
				ExtentReport.extentTest.info("User -------------- " + emailId);
				ExtentReport.extentTest.info("Pending Status not found in the list");
			}
		}
	}

	@Test(enabled = true, priority = 8, dataProvider = "refundPayment", dataProviderClass = PaymentRequestDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void refundPayment(String emailId, String pwd, String guestEmail, String amount, String description) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		PaymentRequestComponent paymentRequestComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			// loginComponent.clickOnDashBoard();
			paymentRequestComponent = new PaymentRequestComponent(WebDriverFactory.getDriver());
			paymentRequestComponent.clickOnPaymentRequest();
			paymentRequestComponent.clickOnManageRequest();
			int row = paymentRequestComponent.refundPayment(Constant.STATUS_PAID, amount, description);
			if (row > 0) {
				String status = paymentRequestComponent.getPaymentStatus(row);
				String requestId = paymentRequestComponent.getRequestId(row);
				System.out.println("Request Id for the Payment Request ------------ " + requestId);
				System.out.println("Payment Status after refund -------------- " + status);
				// Assert.assertEquals(status, Constant.STATUS_REFUND);
				// Assert.assertEquals(status.contains(Constant.STATUS_REFUND));
				Assert.assertTrue(status.contains(Constant.STATUS_REFUND));

				ExtentReport.extentTest.info("User ------------- " + emailId);
				ExtentReport.extentTest.info("Refund payment starts -------- ");
				ExtentReport.extentTest.info("Request Id for the Payment Request ---------- " + requestId);
				ExtentReport.extentTest.info("Status after refund the amount to guest --------- " + status);

			} else {
				ExtentReport.extentTest.info("User ----------- " + emailId);
				ExtentReport.extentTest.info("Paid status not found in the list");
			}
		}
	}

	@Test(enabled = true, priority = 3, dataProvider = "createPaymentRequest", dataProviderClass = PaymentRequestDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void chargeNowWithoutCreateLink(String emailId, String pwd, String guestEmail, String amount,
			String currency, String description) {
		String charge = null;
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		PaymentRequestComponent paymentRequestComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			// loginComponent.clickOnDashBoard();
			paymentRequestComponent = new PaymentRequestComponent(WebDriverFactory.getDriver());
			paymentRequestComponent.clickOnPaymentRequest();
			paymentRequestComponent.createRequest();
			paymentRequestComponent.enterEmail_inForm(guestEmail);
			paymentRequestComponent.enterAmount(amount);
			if (currency.length() > 0) {
				paymentRequestComponent.changeCurrency(currency);
			}
			paymentRequestComponent.enterDescription(description);
			paymentRequestComponent.clickOnChargeNow_WithoutCreateLink();
			String formStatus = paymentRequestComponent.validatePaymentForm();
			if (formStatus.equals("null")) {
				paymentRequestComponent.getChargeNowWithoutCreateLink();
				charge = paymentRequestComponent.getToastMsg();
				if (charge.equals(Constant.PAYMENT_CHARGED_SUCCESS)) {
					paymentRequestComponent.clickOnPaymentRequest();
					paymentRequestComponent.clickOnManageRequest();
					String chargePaymentIcon = paymentRequestComponent.verifyIcon(3);
					String amounts = paymentRequestComponent.getAmount(3);
					String emails = paymentRequestComponent.getEmailId(3);
					String currentDate = paymentRequestComponent.getcurrentDate();
					String createdDate = paymentRequestComponent.getCreatedDate(3);
					String status = paymentRequestComponent.verifyPendingAfterCreateRequest();
					String requestId = paymentRequestComponent.getRequestId(3);
					Assert.assertEquals(chargePaymentIcon, "Dollar");
					Assert.assertEquals(amount, amounts);
					Assert.assertEquals(status, Constant.STATUS_PAID);
					Assert.assertEquals(emails, guestEmail);
					// Assert.assertEquals(currentDate, createdDate);
					System.out.println("Current Date ----------- " + currentDate);

					ExtentReport.extentTest.info("User ------------ " + emailId);
					ExtentReport.extentTest.info("Charge now without creating link starts ------------ ");
					ExtentReport.extentTest.info("Request id for the payment Request -------- " + requestId);
					ExtentReport.extentTest.info("Guest's Email Id ---------- " + guestEmail);
					ExtentReport.extentTest.info("Guest need to paid --------- " + amount);
					if (currency.length() > 0) {
						ExtentReport.extentTest.info("Guest need to paid in " + currency + " Currency");
					}
					ExtentReport.extentTest.info("Payment Request Description ----------- " + description);
					ExtentReport.extentTest.info("After Payment Status --------- " + status);
					ExtentReport.extentTest.info("Payment Request Created Date --------- " + createdDate);
				} else {
					ExtentReport.extentTest.info("User -------------- " + emailId);
					ExtentReport.extentTest.warning("Payment Status ----------- " + charge);
				}

			} else {
				ExtentReport.extentTest.info("User ------------- " + emailId);
				ExtentReport.extentTest.warning("Status of the Process ----------- " + formStatus);
			}
		}
	}

	@Test(enabled = true, priority = -2, invocationCount = 2, dataProvider = "createPaymentRequest", dataProviderClass = PaymentRequestDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void sendAuthorizationLink(String emailId, String pwd, String guestEmailId, String amount, String currency,
			String description) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		PaymentRequestComponent paymentRequestComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			// loginComponent.clickOnDashBoard();
			paymentRequestComponent = new PaymentRequestComponent(WebDriverFactory.getDriver());
			paymentRequestComponent.clickOnPaymentRequest();
			paymentRequestComponent.createRequest();
			paymentRequestComponent.clickOnAuthorizationTab();
			paymentRequestComponent.enterEmail_inForm(guestEmailId);
			paymentRequestComponent.enterAmount(amount);
			if (currency.length() > 0) {
				paymentRequestComponent.changeCurrency(currency);
			}
			paymentRequestComponent.enterDescription(description);
			paymentRequestComponent.clickOnSendAuthorizeLink();
			String formStatus = paymentRequestComponent.validatePaymentForm();
			if (formStatus.equals("null")) {
				String link = paymentRequestComponent.getFromLink();
				String requestIds = paymentRequestComponent.getRequestId_FromLink(link);
				paymentRequestComponent.clickOnPaymentRequest();
				paymentRequestComponent.clickOnManageRequest();
				String status = paymentRequestComponent.verifyPendingAfterCreateRequest();
				String requestId = paymentRequestComponent.getRequestId(3);
				String paymentIcon = paymentRequestComponent.verifyIcon(3);
				String amounts = paymentRequestComponent.getAmount(3);
				String emails = paymentRequestComponent.getEmailId(3);
				String createdDate = paymentRequestComponent.getCreatedDate(3);
				String currentDate = paymentRequestComponent.getcurrentDate();
				System.out.println("Request Id ---------- " + requestId);
				Assert.assertEquals(status, Constant.STATUS_PENDING);
				Assert.assertEquals(paymentIcon, "Lock");
				Assert.assertEquals(amounts, amount);
				Assert.assertEquals(emails, guestEmailId);
				Assert.assertEquals(requestId, requestIds);
				// Assert.assertEquals(createdDate, currentDate);
				System.out.println("Current Date --------- " + currentDate);

				ExtentReport.extentTest.info("Users Email Id --------------- " + emailId);
				ExtentReport.extentTest.info("Send Authorization Link Start ------------ ");
				ExtentReport.extentTest.info("Guest Email Id ------------ " + guestEmailId);
				ExtentReport.extentTest.info("Guest needs to Pay ---------------- " + amount);
				ExtentReport.extentTest.info("Payment Request Description --------- " + description);
				ExtentReport.extentTest.info("Request Id for the Payment Request -------- " + requestId);
				ExtentReport.extentTest.info("Payment Request Created Date --------- " + createdDate);
				if (currency.length() > 0) {
					ExtentReport.extentTest.info("Guest needs to pay in " + currency + " Currency");
				}
				ExtentReport.extentTest.info("After Sending Authorization Link the Status is ------------ " + status);
			} else {
				ExtentReport.extentTest.info("User Email Id ----------- " + emailId);
				ExtentReport.extentTest.warning("Status of the Process ---------- " + formStatus);
			}
		}
	}

	@Test(enabled = true, priority = -1, invocationCount = 1, dataProvider = "createPaymentRequest", dataProviderClass = PaymentRequestDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void createAuthorizationLinkAndSendEmail(String emailId, String pwd, String guestEmailId, String amount,
			String currency, String description) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		PaymentRequestComponent paymentRequestComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			// loginComponent.clickOnDashBoard();
			paymentRequestComponent = new PaymentRequestComponent(WebDriverFactory.getDriver());
			paymentRequestComponent.clickOnPaymentRequest();
			paymentRequestComponent.createRequest();
			paymentRequestComponent.clickOnAuthorizationTab();
			paymentRequestComponent.enterEmail_inForm(guestEmailId);
			paymentRequestComponent.enterAmount(amount);
			if (currency.length() > 0) {
				paymentRequestComponent.changeCurrency(currency);
			}
			paymentRequestComponent.enterDescription(description);
			paymentRequestComponent.clickOnMorePaymentOption();
			paymentRequestComponent.clickOnCreateAuthorizationLink();
			String formStatus = paymentRequestComponent.validatePaymentForm();
			if (formStatus.equals("null")) {
				paymentRequestComponent.sendEmailPaymentLink();
				String link = paymentRequestComponent.getFromLink();
				String requestIds = paymentRequestComponent.getRequestId_FromLink(link);
				paymentRequestComponent.clickOnPaymentRequest();
				paymentRequestComponent.clickOnManageRequest();
				String status = paymentRequestComponent.verifyPendingAfterCreateRequest();
				String requestId = paymentRequestComponent.getRequestId(3);
				String paymentIcon = paymentRequestComponent.verifyIcon(3);
				String amounts = paymentRequestComponent.getAmount(3);
				String emails = paymentRequestComponent.getEmailId(3);
				String createdDate = paymentRequestComponent.getCreatedDate(3);
				String currentDate = paymentRequestComponent.getcurrentDate();
				System.out.println("Current Date --------- " + currentDate);

				Assert.assertEquals(paymentIcon, "Lock");
				Assert.assertEquals(status, Constant.STATUS_PENDING);
				Assert.assertEquals(amounts, amount);
				Assert.assertEquals(requestId, requestIds);
				Assert.assertEquals(emails, guestEmailId);
				// Assert.assertEquals(createdDate, currentDate);
				ExtentReport.extentTest.info("User Email Id -------------- " + emailId);
				ExtentReport.extentTest.info("Create Authorization Link and send starts ---------- ");
				ExtentReport.extentTest.info("Guest Email Id ------------ " + guestEmailId);
				ExtentReport.extentTest.info("Guest needs to Pay ----------- " + amount);
				ExtentReport.extentTest.info("Payment Request description ------------ " + description);
				ExtentReport.extentTest.info("Request Id for the payment request ---------- " + requestId);
				ExtentReport.extentTest.info("Payment Request Created Date --------- " + createdDate);
				if (currency.length() > 0) {
					ExtentReport.extentTest.info("Guest needs to pay in " + currency + " Currency");
				}
				ExtentReport.extentTest
						.info("After sending the create link the status of the payment ----------- " + status);
			} else {
				ExtentReport.extentTest.info("User Email Id ------------ " + emailId);
				ExtentReport.extentTest.warning("Status of the Process ------------ " + formStatus);
			}
		}
	}

	@Test(enabled = true, priority = 4, invocationCount = 1, dataProvider = "createPaymentRequest", dataProviderClass = PaymentRequestDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void getChargeNowWithoutAuthorize(String emailId, String pwd, String guestEmailId, String amount,
			String currency, String description) {
		String charge = null;
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		PaymentRequestComponent paymentRequestComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			// loginComponent.clickOnDashBoard();
			paymentRequestComponent = new PaymentRequestComponent(WebDriverFactory.getDriver());
			paymentRequestComponent.clickOnPaymentRequest();
			paymentRequestComponent.createRequest();
			paymentRequestComponent.clickOnAuthorizationTab();
			paymentRequestComponent.enterEmail_inForm(guestEmailId);
			paymentRequestComponent.enterAmount(amount);
			if (currency.length() > 0) {
				paymentRequestComponent.changeCurrency(currency);
			}
			paymentRequestComponent.enterDescription(description);
			paymentRequestComponent.clickOnMorePaymentOption();
			paymentRequestComponent.clickOnAuthorizeNow();
			String formStatus = paymentRequestComponent.validatePaymentForm();
			if (formStatus.equals("null")) {
				paymentRequestComponent.getChargeWithoutAuthorizeLink();
				charge = paymentRequestComponent.getToastMsg();
				if (charge.equals(Constant.PAYMENT_CHARGED_SUCCESS)) {
					paymentRequestComponent.clickOnPaymentRequest();
					paymentRequestComponent.clickOnManageRequest();
					String status = paymentRequestComponent.verifyPendingAfterCreateRequest();
					String requestId = paymentRequestComponent.getRequestId(3);
					String paymentIcon = paymentRequestComponent.verifyIcon(3);
					String amounts = paymentRequestComponent.getAmount(3);
					String emails = paymentRequestComponent.getEmailId(3);
					String createdDate = paymentRequestComponent.getCreatedDate(3);
					String currentDate = paymentRequestComponent.getcurrentDate();
					System.out.println("Status of the Payment ------ " + status);
					System.out.println("Request Id of the payment request ------- " + requestId);
					Assert.assertEquals(status, Constant.AUTHORIZE);
					Assert.assertEquals(paymentIcon, "Lock");
					Assert.assertEquals(amounts, amount);
					Assert.assertEquals(emails, guestEmailId);
					// Assert.assertEquals(createdDate, currentDate);
					System.out.println("Current Date --------- " + currentDate);

					ExtentReport.extentTest.info("User Email Id is -------- " + emailId);
					ExtentReport.extentTest.info("Guest Email Id is ---------- " + guestEmailId);
					ExtentReport.extentTest.info("Guest need to paid -------------- " + amount);
					ExtentReport.extentTest.info("Description for the payment Request ---------- " + description);
					ExtentReport.extentTest.info("Request Id for the payment Request ---------- " + requestId);
					if (currency.length() > 0) {
						ExtentReport.extentTest.info("Guest needs to pay in " + currency + " Currency");
					}
					ExtentReport.extentTest.info("Status of the Payment Request --------- " + status);
					ExtentReport.extentTest.info("Payment Request Created Date --------- " + createdDate);
				} else {
					ExtentReport.extentTest.info("User Email Id is --------- " + emailId);
					ExtentReport.extentTest.warning("Payment Status ------------ " + charge);
				}

			} else {
				ExtentReport.extentTest.info("User Email Id is --------- " + emailId);
				ExtentReport.extentTest.warning("Status of the Process ------ " + formStatus);
			}
		}
	}

	@Test(enabled = true, priority = 13, dataProvider = "refundPayment", dataProviderClass = PaymentRequestDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void captureNow(String emailId, String pwd, String guestEmailId, String amount, String description) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		PaymentRequestComponent paymentRequestComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			// loginComponent.clickOnDashBoard();
			paymentRequestComponent = new PaymentRequestComponent(WebDriverFactory.getDriver());
			paymentRequestComponent.clickOnPaymentRequest();
			paymentRequestComponent.clickOnManageRequest();
			int row = paymentRequestComponent.clickOnCapture(Constant.AUTHORIZE, amount, description);
			if (row > 0) {
				String status = paymentRequestComponent.getPaymentStatus(row);
				String requestId = paymentRequestComponent.getRequestId(row);
				System.out.println("Status of the Payment ------------ " + status);
				System.out.println("Request Id of the Payment --------------- " + requestId);

				if (status.equals(Constant.CAPTURED)) {
					Assert.assertTrue(status.contains(Constant.CAPTURED));
				} else {
					Assert.assertTrue(status.contains(Constant.PARTIAL_RELEASE));
				}

				ExtentReport.extentTest.info("User Email Id --------- " + emailId);
				ExtentReport.extentTest.info("Capture starts -------------- ");
				ExtentReport.extentTest.info("Captured amount --------- " + amount);
				ExtentReport.extentTest.info("Captured Description ------ " + description);
				ExtentReport.extentTest.info("Request Id of Payment ---------- " + requestId);
				ExtentReport.extentTest.info("Status After Captured --------- " + status);
			} else {
				ExtentReport.extentTest.info("User -------------- " + emailId);
				ExtentReport.extentTest.info("Authorize status not found in the list");
			}
		}
	}

	@Test(enabled = true, priority = 14, dataProvider = "paymentCredential", dataProviderClass = PaymentRequestDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void releasePayment(String emailId, String pwd) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		PaymentRequestComponent paymentRequestComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			// loginComponent.clickOnDashBoard();
			paymentRequestComponent = new PaymentRequestComponent(WebDriverFactory.getDriver());
			paymentRequestComponent.clickOnPaymentRequest();
			paymentRequestComponent.clickOnManageRequest();
			int row = paymentRequestComponent.realeasePayment(Constant.AUTHORIZE);
			if (row > 0) {
				String status = paymentRequestComponent.getPaymentStatus(row);
				String requestId = paymentRequestComponent.getRequestId(row);
				System.out.println("Status of the Payment ---------- " + status);
				System.out.println("Request Id of the Payment --------- " + requestId);

				Assert.assertEquals(status, Constant.RELEASED);

				ExtentReport.extentTest.info("User Email Id ------------- " + emailId);
				ExtentReport.extentTest.info("Release Process Starts ------------ ");
				ExtentReport.extentTest.info("After release status -------- " + status);
				ExtentReport.extentTest.info("Request Id for the Payment ---------- " + requestId);
			} else {
				ExtentReport.extentTest.info("User ------------- " + emailId);
				ExtentReport.extentTest.info("Authorize status not found in the list");
			}
		}
	}

	@Test(enabled = true, priority = 15, dataProvider = "createPaymentWithMoreSetting", dataProviderClass = PaymentRequestDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void createRequestWithMoreSetting(String emailId, String pwd, String guestEmail, String amount,
			String currency, String description, String scheduleDate, String expireDate, String chargeProtection,
			String termCondition, String paymentType) {
		String ExpireVal = null;
		String dueDate = null;
		String scheduleVal = null;
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		PaymentRequestComponent paymentRequestComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			// loginComponent.clickOnDashBoard();
			paymentRequestComponent = new PaymentRequestComponent(WebDriverFactory.getDriver());
			paymentRequestComponent.clickOnPaymentRequest();
			paymentRequestComponent.createRequest();
			paymentRequestComponent.enterEmail_inForm(guestEmail);
			paymentRequestComponent.enterAmount(amount);
			if (currency.length() > 0) {
				paymentRequestComponent.changeCurrency(currency);
			}
			paymentRequestComponent.enterDescription(description);
			paymentRequestComponent.clickOnMoreSetting();
			if (chargeProtection.equals("true")) {
				paymentRequestComponent.setChargeProtection();
			}
			paymentRequestComponent.setTermsCondition(termCondition);
			if (paymentType.equals(Constant.CREATE_PAYMENT_LINK) || paymentType.equals(Constant.SEND_PAYMENT_LINK)
					|| paymentType.equals(Constant.SCHEDULE_CHARGE)) {
				paymentRequestComponent.setScheduleDate(scheduleDate);
				paymentRequestComponent.setScheduleDateTime(scheduleDate);
				// paymentRequestComponent.clickSetDateTime();
				paymentRequestComponent.setExpiryDate(expireDate);

				ExpireVal = paymentRequestComponent.getExpiryDate();
				scheduleVal = paymentRequestComponent.getScheduleDateFromForm();
				System.out.println("Expiry Date ----------- " + ExpireVal);
			}
			// paymentRequestComponent.clickSetDateTime();
			// DynamicWait.waitUntil(20);
			if (paymentType.equals(Constant.SEND_PAYMENT_LINK)) {
				paymentRequestComponent.clickOnPaymentLink();
			} else if (paymentType.equals(Constant.CREATE_PAYMENT_LINK)) {
				paymentRequestComponent.clickOnCreatePaymentLink();
			} else if (paymentType.equals(Constant.CHARGE_NOW)) {
				paymentRequestComponent.clickOnChargeNow_WithoutCreateLink();
			} else if (paymentType.equals(Constant.SCHEDULE_CHARGE)) {
				paymentRequestComponent.clickOnScheduleCharge();
			}

			String formStatus = paymentRequestComponent.validatePaymentForm();
			String paymentRequestId = null;
			if (formStatus.equals("null")) {
				if (paymentType.equals(Constant.SEND_PAYMENT_LINK)) {
					String paymentLink = paymentRequestComponent.getFromLink();
					paymentRequestId = paymentRequestComponent.getRequestId_FromLink(paymentLink);
				} else if (paymentType.equals(Constant.CREATE_PAYMENT_LINK)) {
					String paymentLink = paymentRequestComponent.getScheduleCreatePaymentLink();
					paymentRequestId = paymentRequestComponent.getRequestId_FromLink(paymentLink);
				} else if (paymentType.equals(Constant.CHARGE_NOW)) {
					paymentRequestComponent.getChargeNowWithoutCreateLink();
					String toastMsg = paymentRequestComponent.getToastMsg();
					System.out.println("Toast Message" + toastMsg);
				} else if (paymentType.equals(Constant.SCHEDULE_CHARGE)) {
					paymentRequestComponent.getChargeSchedule();
					String paymentLink = paymentRequestComponent.getRequestIdFromLinkScheduleCharge();
					paymentRequestId = paymentRequestComponent.getRequestId_FromLink(paymentLink);
				}
				paymentRequestComponent.clickOnPaymentRequest();
				paymentRequestComponent.clickOnManageRequest();
				String icon = paymentRequestComponent.verifyIcon(3);
				System.out.println("Payment Icon ---- " + icon);
				String status = paymentRequestComponent.verifyPendingAfterCreateRequest();
				String requestId = paymentRequestComponent.getRequestId(3);
				String paymentIcon = null;
				if (chargeProtection.equals("true")) {
					paymentIcon = paymentRequestComponent.verifySheild(3);
				}
				String amounts = paymentRequestComponent.getAmount(3);
				String emails = paymentRequestComponent.getEmailId(3);
				if (paymentType.equals(Constant.CREATE_PAYMENT_LINK) || paymentType.equals(Constant.SEND_PAYMENT_LINK)
						|| paymentType.equals(Constant.SCHEDULE_CHARGE)) {
					dueDate = paymentRequestComponent.getDueDate(3);
				}

				String createdDate = paymentRequestComponent.getCreatedDate(3);
				String currentDate = paymentRequestComponent.getcurrentDate();
				System.out.println("Current Date ------------- " + currentDate);

				paymentRequestComponent.clickOnOpenMoreOptionInRow(3, paymentType);
				String scheduleDate_InMore = paymentRequestComponent.getScheduleDate();
				String expiredDate_InMore = paymentRequestComponent.getExpiredDate();
				String chargeProtected = paymentRequestComponent.getChargeProtected();
				String paymentMethod = paymentRequestComponent.getPaymentMethod();
				String description_InMore = paymentRequestComponent.getDescription();
				String terms_InMore = paymentRequestComponent.getTermCondition();

				System.out.println("Schedule Date in More Option --------- " + scheduleDate_InMore);
				System.out.println("Expired Date in More Option ------------- " + expiredDate_InMore);
				System.out.println("Charge Protection in More Option --------- " + chargeProtected);
				System.out.println("Payment Method in More Option --------------- " + paymentMethod);
				System.out.println("Payment Description in More Option ----------- " + description_InMore);
				System.out.println("Payment Terms and Condition ------------------- " + terms_InMore);

				System.out.println("Payment Request Id ----------- " + paymentRequestId);
				System.out.println("Status of the Payment ------ " + status);
				System.out.println("Request Id of the payment request ------- " + requestId);
				System.out.println("Payment Icon ------- " + paymentIcon);
				System.out.println("Amount --------- " + amounts);
				System.out.println("Guest Email ---------- " + emails);
				System.out.println("Due date -------------" + dueDate);
				System.out.println("Created Date -------- " + createdDate);
				System.out.println("Current Date ---------- " + currentDate);

				Assert.assertEquals(icon, "Dollar");
				Assert.assertEquals(paymentIcon, "shield");
				Assert.assertEquals(amounts, amount);
				if (paymentType.equals(Constant.CREATE_PAYMENT_LINK) || paymentType.equals(Constant.SEND_PAYMENT_LINK)
						|| paymentType.equals(Constant.SCHEDULE_CHARGE)) {
					Assert.assertEquals(status, Constant.STATUS_SCHEDULED);
					Assert.assertEquals(paymentRequestId, requestId);
					// Assert.assertEquals(createdDate, currentDate);
					// Assert.assertTrue(scheduleDate_InMore.contains(scheduleVal));
					// Assert.assertTrue(expiredDate_InMore.contains(ExpireVal));
					System.out.println("Schedule Date ---------- " + scheduleVal);
					if (chargeProtection.equals("true")) {
						if (chargeProtected.equals("Yes")) {
							Assert.assertEquals("true", chargeProtection);
						}
					}
				} else if (paymentType.equals(Constant.CHARGE_NOW)) {
					Assert.assertEquals(status, Constant.AWAITING_APPROVAL);
				}
				Assert.assertEquals(emails, guestEmail);
				Assert.assertEquals(description, description_InMore);
				Assert.assertEquals(termCondition, terms_InMore);
				ExtentReport.extentTest.info("Payment Request Tyep ----------- " + paymentType);
				ExtentReport.extentTest.info("User Email ------------ " + emailId);
				ExtentReport.extentTest.info("Guest Email Id ----------- " + guestEmail);
				ExtentReport.extentTest.info("Amount needs to pay " + amount);
				if (currency.length() > 0) {
					ExtentReport.extentTest.info("Guest needs to pay in " + currency + " Currency");
				}
				ExtentReport.extentTest.info("Status of the payment ------- " + status);
				ExtentReport.extentTest.info("Request Id of the payment ---------- " + requestId);
				ExtentReport.extentTest.info("Created date of the payment ----------- " + createdDate);
				ExtentReport.extentTest.info("Due date of the payment ---------- " + dueDate);
			} else {
				ExtentReport.extentTest.info("User ----------- " + emailId);
				ExtentReport.extentTest.warning("Status of the Process ---------- " + formStatus);
			}
		}
	}

	@Test(enabled = true, priority = 16, dataProvider = "createAuthorizePaymentWithMoreSetting", dataProviderClass = PaymentRequestDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void createAuthorizePayment_MoreSetting(String emailId, String pwd, String guestEmail, String amount,
			String currency, String description, String scheduleDate, String expireDate, String chargeProtection,
			String termCondition, String paymentType) {
		String scheduleVal = null;
		String ExpireVal = null;
		String paymentRequestId = null;
		String dueDate = null;
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		PaymentRequestComponent paymentRequestComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			// loginComponent.clickOnDashBoard();
			paymentRequestComponent = new PaymentRequestComponent(WebDriverFactory.getDriver());
			paymentRequestComponent.clickOnPaymentRequest();
			paymentRequestComponent.createRequest();
			paymentRequestComponent.clickOnAuthorizationTab();
			paymentRequestComponent.enterEmail_inForm(guestEmail);
			paymentRequestComponent.enterAmount(amount);
			if (currency.length() > 0) {
				paymentRequestComponent.changeCurrency(currency);
			}
			paymentRequestComponent.enterDescription(description);
			paymentRequestComponent.clickOnMoreSetting();
			if (chargeProtection.equals("true")) {
				paymentRequestComponent.setChargeProtection();
			}
			paymentRequestComponent.setTermsCondition(termCondition);

			if (paymentType.equals(Constant.CREATE_AUTHORIZE_LINK) || paymentType.equals(Constant.SEND_AUTHORIZE_LINK)
					|| paymentType.equals(Constant.SCHEDULE_AUTHORIZE)) {
				paymentRequestComponent.setScheduleDate(scheduleDate);
				paymentRequestComponent.setScheduleDateTime(scheduleDate);
				//paymentRequestComponent.clickSetDateTime();
				paymentRequestComponent.setExpiryDate(expireDate);
				ExpireVal = paymentRequestComponent.getExpiryDate();
				scheduleVal = paymentRequestComponent.getScheduleDateFromForm();
				System.out.println("Schedule Date -------------- " + scheduleVal);
				System.out.println("Expiry Date ---------- " + ExpireVal);
			}

			if (paymentType.equals(Constant.SEND_AUTHORIZE_LINK)) {
				DynamicWait.waitUntil(2);
				paymentRequestComponent.clickOnPaymentLink();
			} else if (paymentType.equals(Constant.CREATE_AUTHORIZE_LINK)) {
				DynamicWait.waitUntil(2);
				paymentRequestComponent.clickOnMorePaymentOption();
				paymentRequestComponent.clickOnCreateAuthorizationLink();
			} else if (paymentType.equals(Constant.AUTHORIZE_NOW)) {
				DynamicWait.waitUntil(2);
				paymentRequestComponent.clickOnMorePaymentOption();
				paymentRequestComponent.clickOnAuthorizeNow();
			} else if (paymentType.equals(Constant.SCHEDULE_AUTHORIZE)) {
				DynamicWait.waitUntil(2);
				paymentRequestComponent.clickOnScheduleCharge();
			}

			String formStatus = paymentRequestComponent.validatePaymentForm();
			if (formStatus.equals("null")) {
				if (paymentType.equals(Constant.SEND_AUTHORIZE_LINK)) {
					String paymentLink = paymentRequestComponent.getFromLink();
					paymentRequestId = paymentRequestComponent.getRequestId_FromLink(paymentLink);
					System.out.println("Request Id from the link ----------- " + paymentRequestId);
				} else if (paymentType.equals(Constant.CREATE_AUTHORIZE_LINK)) {
					String paymentLink = paymentRequestComponent.getScheduleCreatePaymentLink();
					paymentRequestId = paymentRequestComponent.getRequestId_FromLink(paymentLink);
					System.out.println("Request Id from the link ---------- " + paymentRequestId);
				} else if (paymentType.equals(Constant.AUTHORIZE_NOW)) {
					paymentRequestComponent.getChargeWithoutAuthorizeLink();
					String toastMsg = paymentRequestComponent.getToastMsg();
					System.out.println("Toast Message -------------- " + toastMsg);
				} else if (paymentType.equals(Constant.SCHEDULE_AUTHORIZE)) {
					paymentRequestComponent.getChargeSchedule();
					String paymentLink = paymentRequestComponent.getRequestIdFromLinkScheduleCharge();
					paymentRequestId = paymentRequestComponent.getRequestId_FromLink(paymentLink);
					System.out.println("Request Id from the Link -------------- " + paymentRequestId);
					// DynamicWait.waitUntil(10);
				}

				paymentRequestComponent.clickOnPaymentRequest();
				paymentRequestComponent.clickOnManageRequest();
				String icon = paymentRequestComponent.verifyIcon(3);
				System.out.println("Payment Icon ---------- " + icon);
				String status = paymentRequestComponent.verifyPendingAfterCreateRequest();
				String requestId = paymentRequestComponent.getRequestId(3);
				String paymentIcon = null;
				if (chargeProtection.equals("true")) {
					paymentIcon = paymentRequestComponent.verifySheild(3);
				}
				String amounts = paymentRequestComponent.getAmount(3);
				String emails = paymentRequestComponent.getEmailId(3);

				if (paymentType.equals(Constant.CREATE_AUTHORIZE_LINK)
						|| paymentType.equals(Constant.SEND_AUTHORIZE_LINK)
						|| paymentType.equals(Constant.SCHEDULE_AUTHORIZE)) {
					dueDate = paymentRequestComponent.getDueDate(3);
					System.out.println("Due date ------ " + dueDate);
				}
				String createdDate = paymentRequestComponent.getCreatedDate(3);
				String currentDate = paymentRequestComponent.getcurrentDate();

				paymentRequestComponent.clickOnOpenMoreOptionInRow(3, Constant.AUTHORIZE);
				String scheduleDate_InMore = paymentRequestComponent.getScheduleDate();
				String expiredDate_InMore = paymentRequestComponent.getExpiredDate();
				String chargeProtected = paymentRequestComponent.getChargeProtected();
				String paymentMethod = paymentRequestComponent.getPaymentMethod();
				String description_inMore = paymentRequestComponent.getDescription();
				String terms_InMore = paymentRequestComponent.getTermCondition();

				System.out.println("Schedule Date in More Option ------------- " + scheduleDate_InMore);
				System.out.println("Expired date in More Option --------------- " + expiredDate_InMore);
				System.out.println("Charge Protection in More Option --------- " + chargeProtected);
				System.out.println("Payment Method in More Option --------- " + paymentMethod);
				System.out.println("Description in More Option ------------- " + description_inMore);
				System.out.println("Terms and Condition in More Option -------- " + terms_InMore);
				System.out.println("Current Date ------------- " + currentDate);

				Assert.assertEquals(icon, "Lock");
				Assert.assertEquals(paymentIcon, "shield");
				Assert.assertEquals(amounts, amount);

				if (paymentType.equals(Constant.SEND_AUTHORIZE_LINK)
						|| paymentType.equals(Constant.CREATE_AUTHORIZE_LINK)
						|| paymentType.equals(Constant.SCHEDULE_AUTHORIZE)) {
					Assert.assertEquals(status, Constant.STATUS_SCHEDULED);
					Assert.assertEquals(paymentRequestId, requestId);
					//Assert.assertEquals(createdDate, currentDate);
					System.out.println("ScheduleDate_InMore ------- " + scheduleDate_InMore);
					System.out.println("ScheduleDateVal --------- " + scheduleVal);
					//Assert.assertTrue(scheduleDate_InMore.contains(scheduleVal));
					System.out.println("ExpiredDate_InMore ------ " + expiredDate_InMore);
					System.out.println("Expire Value ------------- " + ExpireVal);
					//Assert.assertTrue(expiredDate_InMore.contains(ExpireVal));
					if (chargeProtection.equals("true")) {
						if (chargeProtected.equals("Yes")) {
							Assert.assertEquals("true", chargeProtection);
						}
					}
					Assert.assertEquals(description, description_inMore);
					Assert.assertEquals(termCondition, terms_InMore);
				} else if (paymentType.equals(Constant.AUTHORIZE_NOW)) {
					Assert.assertEquals(status, Constant.AWAITING_APPROVAL);
				}
				Assert.assertEquals(emails, guestEmail);

				ExtentReport.extentTest.info("Payment Request Type ------------ " + paymentType);
				ExtentReport.extentTest.info("User Email -------------- " + emailId);
				ExtentReport.extentTest.info("Guest Email Id ----------- " + guestEmail);
				ExtentReport.extentTest.info("Amount needs to Pay --------- " + amount);
				if (currency.length() > 0) {
					ExtentReport.extentTest.info("Guest needs to pay in " + currency + " Currency");
				}
				ExtentReport.extentTest.info("Status of the payment ---------- " + status);
				ExtentReport.extentTest.info("Request Id of the payment ------- " + requestId);
				ExtentReport.extentTest.info("Created Date of the Payment ------- " + createdDate);
				ExtentReport.extentTest.info("Due date of the payment ---------- " + dueDate);

			} else {
				ExtentReport.extentTest.info("User ----------- " + emailId);
				ExtentReport.extentTest.warning("Status of the Process ---------- " + formStatus);
			}
		}
	}

	@Test(enabled = true, priority = 17, dataProvider = "createPaymentWithMoreSetting", dataProviderClass = PaymentRequestDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void paymentRequestFromManageRequestPage_Charge(String emailId, String pwd, String guestEmail, String amount,
			String currency, String description, String scheduleDate, String expireDate, String chargeProtection,
			String termCondition, String paymentType) {
		String scheduleVal = null;
		String expireVal = null;
		String dueDate = null;

		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		PaymentRequestComponent paymentRequestComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			// loginComponent.clickOnDashBoard();
			paymentRequestComponent = new PaymentRequestComponent(WebDriverFactory.getDriver());
			paymentRequestComponent.clickOnPaymentRequest();
			paymentRequestComponent.clickOnManageRequest();
			paymentRequestComponent.clickOnAddNewRequestBtn();
			paymentRequestComponent.enterEmail_inForm(guestEmail);
			paymentRequestComponent.enterAmount(amount);
			if (currency.length() > 0) {
				paymentRequestComponent.changeCurrency(currency);
			}
			paymentRequestComponent.enterDescription(description);
			paymentRequestComponent.clickOnMoreSetting();
			if (chargeProtection.equals("true")) {
				paymentRequestComponent.setChargeProtection();
			}
			paymentRequestComponent.setTermsCondition(termCondition);
			if (paymentType.equals(Constant.SEND_PAYMENT_LINK) || paymentType.equals(Constant.CREATE_PAYMENT_LINK)
					|| paymentType.equals(Constant.SCHEDULE_CHARGE)) {
				paymentRequestComponent.setScheduleDate(scheduleDate);
				paymentRequestComponent.setScheduleDateTime(scheduleDate);
				paymentRequestComponent.setExpiryDate(expireDate);
				expireVal = paymentRequestComponent.getExpiryDate();
				scheduleVal = paymentRequestComponent.getScheduleDateFromForm();
				System.out.println("Schedule Date From Form ---------------- " + scheduleVal);
				System.out.println("Expire Date From Form ------------------ " + expireVal);
			}
			if (paymentType.equals(Constant.SEND_PAYMENT_LINK)) {
				paymentRequestComponent.clickOnPaymentLink();
			} else if (paymentType.equals(Constant.CREATE_PAYMENT_LINK)) {
				paymentRequestComponent.clickOnCreatePaymentLink();
			} else if (paymentType.equals(Constant.CHARGE_NOW)) {
				paymentRequestComponent.clickOnChargeNow_WithoutCreateLink();
			} else if (paymentType.equals(Constant.SCHEDULE_CHARGE)) {
				paymentRequestComponent.clickOnScheduleCharge();
			}

			String formStatus = paymentRequestComponent.validatePaymentForm();
			String paymentRequestId = null;
			if (formStatus.equals("null")) {
				if (paymentType.equals(Constant.SEND_PAYMENT_LINK)) {
					String paymentLink = paymentRequestComponent.getFromLink();
					paymentRequestId = paymentRequestComponent.getRequestId_FromLink(paymentLink);
				} else if (paymentType.equals(Constant.CREATE_PAYMENT_LINK)) {
					String paymentLink = paymentRequestComponent.getScheduleCreatePaymentLink();
					paymentRequestId = paymentRequestComponent.getRequestId_FromLink(paymentLink);
				} else if (paymentType.equals(Constant.CHARGE_NOW)) {
					paymentRequestComponent.getChargeNowWithoutCreateLink();
					String toastMsg = paymentRequestComponent.getToastMsg();
					System.out.println("Toast Message ---------------- " + toastMsg);
				} else if (paymentType.equals(Constant.SCHEDULE_CHARGE)) {
					paymentRequestComponent.getChargeSchedule();
					String paymentLink = paymentRequestComponent.getFromLink();
					paymentRequestId = paymentRequestComponent.getRequestId_FromLink(paymentLink);
					DynamicWait.waitUntil(3);
				}
				String icon = paymentRequestComponent.verifyIcon(3);
				System.out.println("Payment Icon ---------- " + icon);
				String status = paymentRequestComponent.verifyPendingAfterCreateRequest();
				String requestId = paymentRequestComponent.getRequestId(3);
				String paymentIcon = null;
				if (chargeProtection.equals("true")) {
					paymentIcon = paymentRequestComponent.verifySheild(3);
				}
				String amounts = paymentRequestComponent.getAmount(3);
				String emails = paymentRequestComponent.getEmailId(3);
				if (paymentType.equals(Constant.CREATE_PAYMENT_LINK) || paymentType.equals(Constant.SEND_PAYMENT_LINK)
						|| paymentType.equals(Constant.SCHEDULE_CHARGE)) {
					dueDate = paymentRequestComponent.getDueDate(3);
				}
				String createdDate = paymentRequestComponent.getCreatedDate(3);
				String currentDate = paymentRequestComponent.getcurrentDate();
				paymentRequestComponent.clickOnOpenMoreOptionInRow(3, paymentType, status);
				String scheduleDate_InMore = paymentRequestComponent.getScheduleDate();
				String expiredDate_InMore = paymentRequestComponent.getExpiredDate();
				String chargeProtected = paymentRequestComponent.getChargeProtected();
				String paymentMethod = paymentRequestComponent.getPaymentMethod();
				String description_InMore = paymentRequestComponent.getDescription();
				String terms_InMore = paymentRequestComponent.getTermCondition();

				System.out.println("Schedule Date in More Option --------- " + scheduleDate_InMore);
				System.out.println("Expired Date in More Option ------------- " + expiredDate_InMore);
				System.out.println("Charge Protection in More Option --------- " + chargeProtected);
				System.out.println("Payment Method in More Option --------------- " + paymentMethod);
				System.out.println("Payment Description in More Option ----------- " + description_InMore);
				System.out.println("Payment Terms and Condition ------------------- " + terms_InMore);

				System.out.println("Payment Request Id ----------- " + paymentRequestId);
				System.out.println("Status of the Payment ------ " + status);
				System.out.println("Request Id of the payment request ------- " + requestId);
				System.out.println("Payment Icon ------- " + paymentIcon);
				System.out.println("Amount --------- " + amounts);
				System.out.println("Guest Email ---------- " + emails);
				System.out.println("Due date -------------" + dueDate);
				System.out.println("Created Date -------- " + createdDate);
				System.out.println("Current Date ---------- " + currentDate);

				Assert.assertEquals(icon, "Dollar");
				if (chargeProtection.equals("true")) {
					Assert.assertEquals(paymentIcon, "shield");
				}
				Assert.assertEquals(amounts, amount);
				if (paymentType.equals(Constant.CREATE_PAYMENT_LINK) || paymentType.equals(Constant.SEND_PAYMENT_LINK)
						|| paymentType.equals(Constant.SCHEDULE_CHARGE)) {
					Assert.assertEquals(status, Constant.STATUS_SCHEDULED);
					Assert.assertEquals(paymentRequestId, requestId);
					//Assert.assertEquals(createdDate, currentDate);
					//Assert.assertTrue(scheduleDate_InMore.contains(scheduleVal));
					//Assert.assertTrue(expiredDate_InMore.contains(expireVal));
					if (chargeProtection.equals("true")) {
						if (chargeProtected.equals("Yes")) {
							Assert.assertEquals("true", chargeProtection);
						}
					}
				} else if (paymentType.equals(Constant.CHARGE_NOW)) {
					Assert.assertEquals(status, Constant.AWAITING_APPROVAL);
				}
				Assert.assertEquals(guestEmail, emails);
				Assert.assertEquals(description, description_InMore);
				Assert.assertEquals(termCondition, terms_InMore);

				ExtentReport.extentTest.info("Payment Request Tyep ----------- " + paymentType);
				ExtentReport.extentTest.info("User Email ------------ " + emailId);
				ExtentReport.extentTest.info("Guest Email Id ----------- " + guestEmail);
				ExtentReport.extentTest.info("Amount needs to pay " + amount);
				if (currency.length() > 0) {
					ExtentReport.extentTest.info("Guest needs to pay in " + currency + " Currency");
				}
				ExtentReport.extentTest.info("Status of the payment ------- " + status);
				ExtentReport.extentTest.info("Request Id of the payment ---------- " + requestId);
				ExtentReport.extentTest.info("Created date of the payment ----------- " + createdDate);
				ExtentReport.extentTest.info("Due date of the payment ---------- " + dueDate);
			} else {
				ExtentReport.extentTest.info("User ----------- " + emailId);
				ExtentReport.extentTest.warning("Status of the Process ---------- " + formStatus);
			}
		}
	}

	@Test(enabled = true, priority = 18, dataProvider = "createAuthorizePaymentWithMoreSetting", dataProviderClass = PaymentRequestDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void paymentRequestFromManageRequestPage_Authorize(String emailId, String pwd, String guestEmail,
			String amount, String currency, String description, String scheduleDate, String expireDate,
			String chargeProtection, String termCondition, String paymentType) {
		String scheduleVal = null;
		String ExpireVal = null;
		String paymentRequestId = null;
		String dueDate = null;
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		PaymentRequestComponent paymentRequestComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			// loginComponent.clickOnDashBoard();
			paymentRequestComponent = new PaymentRequestComponent(WebDriverFactory.getDriver());
			paymentRequestComponent.clickOnPaymentRequest();
			paymentRequestComponent.clickOnManageRequest();
			paymentRequestComponent.clickOnAddNewRequestBtn();
			paymentRequestComponent.clickOnAuthorizationTab();
			paymentRequestComponent.enterEmail_inForm(guestEmail);
			paymentRequestComponent.enterAmount(amount);
			if (currency.length() > 0) {
				paymentRequestComponent.changeCurrency(currency);
			}
			paymentRequestComponent.enterDescription(description);
			paymentRequestComponent.clickOnMoreSetting();
			if (chargeProtection.equals("true")) {
				paymentRequestComponent.setChargeProtection();
			}
			paymentRequestComponent.setTermsCondition(termCondition);

			if (paymentType.equals(Constant.CREATE_AUTHORIZE_LINK) || paymentType.equals(Constant.SEND_AUTHORIZE_LINK)
					|| paymentType.equals(Constant.SCHEDULE_AUTHORIZE)) {
				paymentRequestComponent.setScheduleDate(scheduleDate);
				paymentRequestComponent.setScheduleDateTime(scheduleDate);
				//paymentRequestComponent.clickSetDateTime();
				paymentRequestComponent.setExpiryDate(expireDate);
				ExpireVal = paymentRequestComponent.getExpiryDate();
				scheduleVal = paymentRequestComponent.getScheduleDateFromForm();
				System.out.println("Schedule Date -------------- " + scheduleVal);
				System.out.println("Expiry Date ---------- " + ExpireVal);
			}

			if (paymentType.equals(Constant.SEND_AUTHORIZE_LINK)) {
				DynamicWait.waitUntil(2);
				paymentRequestComponent.clickOnPaymentLink();
			} else if (paymentType.equals(Constant.CREATE_AUTHORIZE_LINK)) {
				DynamicWait.waitUntil(2);
				paymentRequestComponent.clickOnMorePaymentOption();
				paymentRequestComponent.clickOnCreateAuthorizationLink();
			} else if (paymentType.equals(Constant.AUTHORIZE_NOW)) {
				DynamicWait.waitUntil(2);
				paymentRequestComponent.clickOnMorePaymentOption();
				paymentRequestComponent.clickOnAuthorizeNow();
			} else if (paymentType.equals(Constant.SCHEDULE_AUTHORIZE)) {
				DynamicWait.waitUntil(2);
				paymentRequestComponent.clickOnScheduleCharge();
			}

			String formStatus = paymentRequestComponent.validatePaymentForm();
			if (formStatus.equals("null")) {
				if (paymentType.equals(Constant.SEND_AUTHORIZE_LINK)) {
					String paymentLink = paymentRequestComponent.getFromLink();
					paymentRequestId = paymentRequestComponent.getRequestId_FromLink(paymentLink);
					System.out.println("Request Id from the link ----------- " + paymentRequestId);
				} else if (paymentType.equals(Constant.CREATE_AUTHORIZE_LINK)) {
					String paymentLink = paymentRequestComponent.getScheduleCreatePaymentLink();
					paymentRequestId = paymentRequestComponent.getRequestId_FromLink(paymentLink);
					System.out.println("Request Id from the link ---------- " + paymentRequestId);
				} else if (paymentType.equals(Constant.AUTHORIZE_NOW)) {
					paymentRequestComponent.getChargeWithoutAuthorizeLink();
					String toastMsg = paymentRequestComponent.getToastMsg();
					System.out.println("Toast Message -------------- " + toastMsg);
				} else if (paymentType.equals(Constant.SCHEDULE_AUTHORIZE)) {
					paymentRequestComponent.getChargeSchedule();
					String paymentLink = paymentRequestComponent.getRequestIdFromLinkScheduleCharge();
					paymentRequestId = paymentRequestComponent.getRequestId_FromLink(paymentLink);
					System.out.println("Request Id from the Link -------------- " + paymentRequestId);
					// DynamicWait.waitUntil(10);
				}

				DynamicWait.waitUntil(3);
				String icon = paymentRequestComponent.verifyIcon(3);
				System.out.println("Payment Icon ---------- " + icon);
				String status = paymentRequestComponent.verifyPendingAfterCreateRequest();
				String requestId = paymentRequestComponent.getRequestId(3);
				String paymentIcon = null;
				if (chargeProtection.equals("true")) {
					paymentIcon = paymentRequestComponent.verifySheild(3);
					System.out.println(paymentIcon);
				}
				String amounts = paymentRequestComponent.getAmount(3);
				String emails = paymentRequestComponent.getEmailId(3);
				System.out.println("Amounts from the card ---------- " + amounts);
				System.out.println("Email from the card -------------- " + emails);

				if (paymentType.equals(Constant.CREATE_AUTHORIZE_LINK)
						|| paymentType.equals(Constant.SEND_AUTHORIZE_LINK)
						|| paymentType.equals(Constant.SCHEDULE_AUTHORIZE)) {
					dueDate = paymentRequestComponent.getDueDate(3);
					System.out.println("Due date ------ " + dueDate);
				}
				String createdDate = paymentRequestComponent.getCreatedDate(3);
				String currentDate = paymentRequestComponent.getcurrentDate();
				System.out.println("Created date from the card --------- " + createdDate);
				System.out.println("Custom Current date ------------ " + currentDate);

				if (status.equals(Constant.AWAITING_APPROVAL)) {
					paymentRequestComponent.clickOnOpenMoreOptionInRow(3, Constant.AUTHORIZE_NOW, status);
				} else {
					paymentRequestComponent.clickOnOpenMoreOptionInRow(3, Constant.AUTHORIZE, status);
				}

				String scheduleDate_InMore = paymentRequestComponent.getScheduleDate();
				String expiredDate_InMore = paymentRequestComponent.getExpiredDate();
				String chargeProtected = paymentRequestComponent.getChargeProtected();
				String paymentMethod = paymentRequestComponent.getPaymentMethod();
				String description_inMore = paymentRequestComponent.getDescription();
				String terms_InMore = paymentRequestComponent.getTermCondition();

				System.out.println("Schedule Date in More Option ------------- " + scheduleDate_InMore);
				System.out.println("Expired date in More Option --------------- " + expiredDate_InMore);
				System.out.println("Charge Protection in More Option --------- " + chargeProtected);
				System.out.println("Payment Method in More Option --------- " + paymentMethod);
				System.out.println("Description in More Option ------------- " + description_inMore);
				System.out.println("Terms and Condition in More Option -------- " + terms_InMore);

				Assert.assertEquals(icon, "Lock");
				Assert.assertEquals(paymentIcon, "shield");
				Assert.assertEquals(amounts, amount);

				if (paymentType.equals(Constant.SEND_AUTHORIZE_LINK)
						|| paymentType.equals(Constant.CREATE_AUTHORIZE_LINK)
						|| paymentType.equals(Constant.SCHEDULE_AUTHORIZE)) {
					Assert.assertEquals(status, Constant.STATUS_SCHEDULED);
					Assert.assertEquals(paymentRequestId, requestId);
					//Assert.assertEquals(createdDate, currentDate);
					System.out.println("ScheduleDate_InMore ------- " + scheduleDate_InMore);
					System.out.println("ScheduleDateVal --------- " + scheduleVal);
					//Assert.assertTrue(scheduleDate_InMore.contains(scheduleVal));
					System.out.println("ExpiredDate_InMore ------ " + expiredDate_InMore);
					System.out.println("Expire Value ------------- " + ExpireVal);
					//Assert.assertTrue(expiredDate_InMore.contains(ExpireVal));
					if (chargeProtection.equals("true")) {
						if (chargeProtected.equals("Yes")) {
							Assert.assertEquals("true", chargeProtection);
						}
					}
					Assert.assertEquals(description, description_inMore);
					Assert.assertEquals(termCondition, terms_InMore);
				} else if (paymentType.equals(Constant.AUTHORIZE_NOW)) {
					Assert.assertEquals(status, Constant.AWAITING_APPROVAL);
				}
				Assert.assertEquals(emails, guestEmail);

				ExtentReport.extentTest.info("Payment Request Type ------------ " + paymentType);
				ExtentReport.extentTest.info("User Email -------------- " + emailId);
				ExtentReport.extentTest.info("Guest Email Id ----------- " + guestEmail);
				ExtentReport.extentTest.info("Amount needs to Pay --------- " + amount);
				if (currency.length() > 0) {
					ExtentReport.extentTest.info("Guest needs to pay in " + currency + " Currency");
				}
				ExtentReport.extentTest.info("Status of the payment ---------- " + status);
				ExtentReport.extentTest.info("Request Id of the payment ------- " + requestId);
				ExtentReport.extentTest.info("Created Date of the Payment ------- " + createdDate);
				ExtentReport.extentTest.info("Due date of the payment ---------- " + dueDate);

			} else {
				ExtentReport.extentTest.info("User ----------- " + emailId);
				ExtentReport.extentTest.warning("Status of the Process ---------- " + formStatus);
			}
		}
	}

	@Test(enabled = true, priority = 19, dataProvider = "searchingData", dataProviderClass = PaymentRequestDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void searchByIDorEmail(String emailId, String pwd, String emailorId) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		PaymentRequestComponent paymentRequestComponent;
		loginComponent.clickOnLoginAtHomePage();
		loginComponent.enterEmail(emailId);
		loginComponent.enterPassword(pwd);
		loginComponent.clickOnLogin();
		if (loginComponent.validateLogin()) {
			// loginComponent.clickOnDashBoard();
			paymentRequestComponent = new PaymentRequestComponent(WebDriverFactory.getDriver());
			paymentRequestComponent.clickOnPaymentRequest();
			paymentRequestComponent.clickOnManageRequest();
			paymentRequestComponent.searchKey(emailorId);
			String msg = paymentRequestComponent.getSearchResult(emailorId);
			if (msg.equals("No Record found.")) {
				ExtentReport.extentTest.warning("Search data not found -------- " + msg);
			} else if (!msg.equals("null")) {
				if (emailorId.contains("@")) {
					ExtentReport.extentTest.info("Email id verification------------ ");
					Assert.assertEquals(emailorId, msg);
					ExtentReport.extentTest.info("After Search and verification Email Id -------- " + msg);
				} else {
					ExtentReport.extentTest.info("Request Id  verification ------------ ");
					Assert.assertEquals(emailorId, msg);
					ExtentReport.extentTest.info("After Search and verification Request Id ---------- " + msg);
				}
			} else {
				System.out.println("Not able to search ");
				ExtentReport.extentTest.info("Not able to search");
			}
		}
	}
}
