package com.testScripts;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import com.driverFactory.WebDriverFactory;
import com.utilities.ConfigReader;
import com.utilities.Constant;

public class BaseTest {

	private WebDriverFactory driverFactory;
	private WebDriver driver;
	private ConfigReader configReader;
	Properties prop;

	@BeforeMethod
	public void setConfig() {
		configReader = new ConfigReader();
		prop = configReader.setPropertiesFile(Constant.CONFIG_FILE);
		String browserName = configReader.getPropertyValue("browser");
		System.out.println("Browser Value ------ " + browserName);
		driverFactory = new WebDriverFactory();
		driver = driverFactory.init_Driver(browserName);
		String url = prop.getProperty("url");
		System.out.println("URL Value is ------------ " + url);
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@BeforeMethod
	public void getdataFromFaker() {
		// LoginTestScript.credential.put("shahbaz.f@yopmail.com", "Admin#123");
		
	}

	@AfterMethod
	public void quitBrowser() {
		driver.quit();
	}

}
