package com.testScripts;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.driverFactory.WebDriverFactory;
import com.page.RegistrationComponent;
import com.utilities.Constant;
import com.utilities.ExtentReport;
import com.utilities.RegistrationDataProvider;

public class RegistrationTestScript extends BaseTest {

	@Test(enabled = true, dataProvider = "RegistrationData", dataProviderClass = RegistrationDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void doRegistration(String fullName, String companyName, String telePhone, String email, String password,
			String confirmPassword, String pms, boolean checked) {
		RegistrationComponent registration = PageFactory.initElements(WebDriverFactory.getDriver(),
				RegistrationComponent.class);
		System.out.println("Page Title ------------- " + registration.getPageTitle());
		registration.clickOnRegisterBtn();
		registration.enterFullName(fullName);
		registration.enterCompanyName(companyName);
		registration.enterPhoneNumber(telePhone);
		registration.enterEmailId(email);
		registration.enterPassword(password);
		registration.enterConfirmPWD(confirmPassword);
		registration.enterPMS(pms);
		if (checked) {
			registration.checkedAgree();
		}
		registration.submitForm();

		boolean status = registration.isRegisterSuccess();
		if (status) {
			String emailHeading = registration.verifyEmailHeading();
			String verifyFullName = registration.verifyFullName();
			String verifyEmail = registration.verifyEmail();
			Assert.assertEquals(emailHeading, Constant.CONFIRM_EMAIL_HEADING);
			Assert.assertEquals(verifyFullName, "Hello " + fullName + ",");
			if (verifyFullName.contains(fullName)) {
				Assert.assertTrue(true);
			} else {
				Assert.assertTrue(false);
			}
			if (verifyEmail.contains(email)) {
				Assert.assertTrue(true);
			} else {
				Assert.assertTrue(false);
			}
			ExtentReport.extentTest.info("User Full Name --- " + fullName);
			ExtentReport.extentTest.info("User Company Name --- " + companyName);
			ExtentReport.extentTest.info("User Telephone Number --- " + telePhone);
			ExtentReport.extentTest.info("User Email id --- " + email);
			ExtentReport.extentTest.info("User PMS details --- " + pms);
		} else {
			ExtentReport.extentTest.info("Enter proper data into the registration form");
			ExtentReport.extentTest.info("User Full Name --- " + fullName);
			ExtentReport.extentTest.info("User Company Name --- " + companyName);
			ExtentReport.extentTest.info("User Telephone Number --- " + telePhone);
			ExtentReport.extentTest.info("User Email id --- " + email);
			ExtentReport.extentTest.info("User PMS details --- " + pms);
			// ExtentReport.extentTest.info(status);
			System.out.println("Enter proper data into the registration form");
		}
	}

}
