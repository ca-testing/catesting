package com.testScripts;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.driverFactory.WebDriverFactory;
import com.page.LoginComponent;
import com.utilities.ExtentReport;

public class LoginFailTestScript extends BaseTest {

	@Test
	public void emailTextboxNotDetect() {
		LoginComponent login = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		ExtentReport.extentTest.info("Providing wrong locators for email textbox to forcefully fail the test case.");
		System.out.println(login.getPageTitle());
		login.clickOnLoginAtHomePage();
		System.out.println("Clicked on Login page");
		System.out.println(login.getPageTitle());
		login.enterEmails("shahbaz.f@yopmail.com");
		login.enterPassword("Admin#123");
		login.clickOnLogin();
	}
	
	@Test
	public void pwdTextboxNotDetect() {
		LoginComponent login = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		ExtentReport.extentTest.info("Providing wrong locators for password textbox to forcefully fail the test case.");
		System.out.println(login.getPageTitle());
		login.clickOnLoginAtHomePage();
		System.out.println("Clicked on Login page");
		System.out.println(login.getPageTitle());
		login.enterEmail("shahbaz.f@yopmail.com");
		login.enterPasswords("Admin#123");
		login.clickOnLogin();
	}

}
