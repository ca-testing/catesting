package com.testScripts;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.driverFactory.WebDriverFactory;
import com.page.LoginComponent;
import com.page.PaymentRequestComponent;
import com.utilities.CredentialDataProvider;
import com.utilities.ExtentReport;
import com.utilities.RegistrationDataProvider;

public class LoginTestScript extends BaseTest {

	// public static Map<String, String> credential = new HashMap<String, String>();

	@Test(enabled = true, dataProvider = "fakeCredential", dataProviderClass = CredentialDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void doLogin(String emailId, String password) {
		LoginComponent login = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		PaymentRequestComponent paymentRequestComponent;
		System.out.println(login.getPageTitle());
		login.clickOnLoginAtHomePage();
		System.out.println("Clicked on Login page");
		System.out.println(login.getPageTitle());
		// login.enterEmail("shahbaz.f@yopmail.com");
		login.enterEmail(emailId);
		// login.enterPassword("Admin#12");
		login.enterPassword(password);
		login.clickOnLogin();
		new RegistrationDataProvider().getregistrationFakeData();
		if (login.validateLogin() == true) {
			//login.clickOnDashBoard();
			paymentRequestComponent = new PaymentRequestComponent(WebDriverFactory.getDriver());
			String paymentPaheHeading = paymentRequestComponent.getPaymentRequestHeading();
			ExtentReport.extentTest.info("Payment Page Heading is - " + paymentPaheHeading);
			String paymentPageTitle = paymentRequestComponent.getPageTitle();
			ExtentReport.extentTest.info("Page Title of Payment Page is " + paymentPageTitle);
			//Assert.assertEquals(paymentPaheHeading, "Payment Requests");
			ExtentReport.extentTest.pass(emailId + " is successfully able to Login");
		} else {
			ExtentReport.extentTest.info("Wrong Email id or password......!!!");
			ExtentReport.extentTest.info(emailId + " unable to login....!!!");
			System.out.println("User unable to login....!!!");
			System.out.println("Wrong Email id or password......!!!");
			// Assert.fail("User unable to login....!!!");
		}

	}

}
