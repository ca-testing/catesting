package com.testScripts;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.driverFactory.WebDriverFactory;
import com.page.ForgotPasswordComponent;
import com.page.LoginComponent;
import com.utilities.Constant;
import com.utilities.ExtentReport;
import com.utilities.ForgotPasswordDataProvider;

public class ForgotPasswordTestScript extends BaseTest {

	@Test(enabled = true, dataProvider = "FogorPasswordEmail", dataProviderClass = ForgotPasswordDataProvider.class, retryAnalyzer = RetryAnalyzer.class)
	public void verifyForgotPWD(String emails) {
		LoginComponent loginComponent = PageFactory.initElements(WebDriverFactory.getDriver(), LoginComponent.class);
		ForgotPasswordComponent forgotPasswordComponent = PageFactory.initElements(WebDriverFactory.getDriver(),
				ForgotPasswordComponent.class);

		loginComponent.clickOnLoginAtHomePage();

		forgotPasswordComponent.clickOnForgotPWD();
		forgotPasswordComponent.enterForgotEmail(emails);
		forgotPasswordComponent.clickOnChangePWDBtn();
		String successMsg = forgotPasswordComponent.getToastMessage();

		if (successMsg.equals(Constant.FORGOT_PWD_MAIL_SENT_MSG)) {
			Assert.assertEquals(successMsg, Constant.FORGOT_PWD_MAIL_SENT_MSG);
			ExtentReport.extentTest.info("Email Id ------ " + emails);
			ExtentReport.extentTest.pass(successMsg);
		} else if (successMsg.equals(Constant.FORGOT_INVALID_EMAIL)
				|| successMsg.equals(Constant.PROVIDE_REGISTERED_EMAIL)) {
			if (successMsg.equals(Constant.FORGOT_INVALID_EMAIL)) {
				Assert.assertEquals(successMsg, Constant.FORGOT_INVALID_EMAIL);
				ExtentReport.extentTest.warning(Constant.FORGOT_INVALID_EMAIL);
			} else if (successMsg.equals(Constant.PROVIDE_REGISTERED_EMAIL)) {
				Assert.assertEquals(successMsg, Constant.PROVIDE_REGISTERED_EMAIL);
				ExtentReport.extentTest.warning(Constant.PROVIDE_REGISTERED_EMAIL);
			}
		}
	}

}
