package com.utilities;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.testng.annotations.DataProvider;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class CredentialDataProvider {
	
	public static Map<String, String> credential = new HashMap<String, String>();
	
	@DataProvider(name="CredentialProvider")
	public static Object[][] credentials() {
		return new Object[][] {
            { "shahbaz.f@yopmail.com", "Admin#123" },
				
				 { "saud@yopmail.com", "123456" }, 
				 /*{ "rajan@gmail.com", "654321" },
				 *  {"thomas@canada.com", "canada" }, 
				 *  { "<script>javascript:void()</script>\r\n" +"", "canada" }, 
				 * {"",""}
				 */
       };  
	}
	
	@DataProvider(name="fakeCredential")
	public static Object[][] fakerCredential(){
		
		credential.clear();
		credential.put("shahbaz.f@yopmail.com", "Admin#123");
		for (int i = 0; i < 4; i++) {
			String email = new FakerData().getFakerEmail();
			String pwd = new FakerData().getFakePassword();
			credential.put(email, pwd);
		}
		
		GsonBuilder gsonMapBuilder = new GsonBuilder();

		Gson gsonObject = gsonMapBuilder.create();

		String JSONObject = gsonObject.toJson(credential);
		System.out.println("JSON Object using GSON ----- > " + JSONObject);
		Set<?> entries = credential.entrySet();
		Iterator<?> entriesIterator = entries.iterator();
		int size = credential.size();
		Object[][] credentialArray = new Object[size][2];
		
		int i = 0;
		while(entriesIterator.hasNext()){

		    Map.Entry<String,String> mapping = (Map.Entry) entriesIterator.next();

		    credentialArray[i][0] = mapping.getKey();
		    credentialArray[i][1] = mapping.getValue();

		    i++;
		}
		
		// array to store keys of the `HashMap`
        //String[] key = new String[LoginTestScript.credential.size()];
 
        // array to store values of the `HashMap`
       // String[] value = new String[LoginTestScript.credential.size()];
		//for(int i =0; i<size;i++) {
		//	credentialArray[i][0] = LoginTestScript.credential.keySet().toArray(new String[0]);
		//	credentialArray[i][1] = LoginTestScript.credential.values().toArray(new String[0]);
		//}
		System.out.println("Credential Array ---- >"+credentialArray[1][1]);
		return credentialArray;
		//return JSONObject;
	}
	
	@DataProvider(name = "validCredentialData")
	public Object[][] getValidCredential() {
		//int row = 1;
		int row = 5;
		int column = 2;
		Object[][] data = new Object[row][column];
		data[0][0] = "pgtest@yopmail.com";
		data[0][1] = "Rajan@123";
		data[1][0] = "smoobu_new@yopmail.com";
		data[1][1] = "Rajan@123";
		data[2][0] = "cv1692@yopmail.com";
		data[2][1] = "Rajan@123";
		data[3][0] = "rajanbed24_app@yopmail.com";
		data[3][1] = "Rajan@123";
		data[4][0] = "shahbaz.f@yopmail.com";
		data[4][1] = "Admin#123";
		
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				System.out.println(data[i][j]);
			}
		}
		
		return data;
	}

}
