package com.utilities;

import com.github.javafaker.Faker;

public class Registration {
	
	private String fullName;
	private String companyName;
	private String telePhone;
	private String email;
	private String password;
	private String confirmPassword;
	private String pms;
	
	Faker faker = new Faker();
	
	public String getFullName() {
		fullName = faker.name().nameWithMiddle();
		return fullName;
	}
	public String getCompanyName() {
		companyName = faker.company().name();
		return companyName;
	}
	public String getTelePhone() {
		telePhone = faker.phoneNumber().phoneNumber();
		return telePhone;
	}
	public String getEmail() {
		email = faker.internet().emailAddress();
		return email;
	}
	public String getPassword() {
		password = faker.internet().password();
		return password;
	}
	public String getConfirmPassword() {
		confirmPassword = password;
		return confirmPassword;
	}
	public String getPms() {
		pms = faker.gameOfThrones().dragon();
		return pms;
	}
	

}
