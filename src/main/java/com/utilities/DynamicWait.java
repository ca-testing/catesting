package com.utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.driverFactory.WebDriverFactory;

public class DynamicWait {

	public static void waitForVisibility(By locator) {
		WebDriverWait wait = new WebDriverWait(WebDriverFactory.getDriver(), Constant.MAX_WAIT);
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
	}

	public static void waitForClick(By locator) {
		WebDriverWait wait = new WebDriverWait(WebDriverFactory.getDriver(), Constant.MAX_WAIT);
		wait.until(ExpectedConditions.elementToBeClickable(locator));
	}

	public static void waitForClickWebElement(WebElement element) {
		WebDriverWait wait = new WebDriverWait(WebDriverFactory.getDriver(), Constant.MAX_WAIT);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public static void waitUntil(int sec) {
		try {
			Thread.sleep(sec * 1000);
			System.out.println("Waited for " + sec + " Seconds");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static void waitForSwitchFrame(By locator) {
		WebDriverWait wait = new WebDriverWait(WebDriverFactory.getDriver(),Constant.MAX_WAIT);
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(locator));
	}

}
