package com.utilities;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.testng.annotations.DataProvider;

import com.github.javafaker.Faker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class RegistrationDataProvider {

	// @DataProvider(name = "registrationFakeData")
	public void getregistrationFakeData() {
		String fullName = null;
		String companyName = null;
		String telePhone = null;
		String email = null;
		String password = null;
		String confirmPassword = null;
		String pms = null;

		Map<String, String> registrationdata = new HashMap<String, String>();

		for (int i = 0; i < 4; i++) {
			Faker faker = new Faker();
			fullName = faker.name().nameWithMiddle();
			companyName = faker.company().name();
			telePhone = faker.phoneNumber().phoneNumber();
			email = faker.internet().emailAddress();
			password = faker.internet().password();
			confirmPassword = password;
			pms = faker.gameOfThrones().dragon();
			registrationdata.put("FullName", fullName);
			registrationdata.put("CompanyName", companyName);
			registrationdata.put("TelePhone", telePhone);
			registrationdata.put("EmailId", email);
			registrationdata.put("Password", password);
			registrationdata.put("ConfirmPassword", confirmPassword);
			registrationdata.put("PMS", pms);
		}
		System.out.println("HashMap data before converting ----- >>> " + registrationdata);
		System.out.println("HashMap data size before converting ----- >>> " + registrationdata.size());

		GsonBuilder gsonMapBuilder = new GsonBuilder();

		Gson gsonObject = gsonMapBuilder.create();

		String JSONObject = gsonObject.toJson(CredentialDataProvider.credential);
		System.out.println("JSON Object using GSON ----- > " + JSONObject);
		Set<?> entries = CredentialDataProvider.credential.entrySet();
		Iterator<?> entriesIterator = entries.iterator();
		int size = CredentialDataProvider.credential.size();
		Object[][] credentialArray = new Object[size][7];

		int i = 0;
		while (entriesIterator.hasNext()) {

			Map.Entry<String, String> mapping = (Map.Entry) entriesIterator.next();

			credentialArray[i][0] = mapping.getKey();
			credentialArray[i][1] = mapping.getValue();

			i++;
		}
	}

	@DataProvider(name = "RegistrationData")
	public Object[][] getdataForRegistration() {
		Faker faker = new Faker();
		int row = 20;
		int column = 8;
		Object[][] data = new Object[row][column];
		String fullName = null;
		String companyName = null;
		String telePhone = null;
		String email = null;
		String password = null;
		String confirmPassword = null;
		String pms = null;
		// Object[][] extraData = new Object[8][column];

		for (int i = 0; i < row; i++) {
			fullName = faker.name().nameWithMiddle();
			// companyName = faker.company().name();
			companyName = "ChargeAutomation";
			telePhone = faker.phoneNumber().phoneNumber();
			email = faker.internet().emailAddress();
			password = faker.internet().password();
			confirmPassword = password;
			pms = faker.gameOfThrones().dragon();

			if (i == 6) {
				data[i][0] = fullName.replace(fullName, "7635427199");
			} else if (i == 7) {
				data[i][0] = fullName.replace(fullName, "!@#$%^&*()_+~");
			} else if (i == 8) {
				data[i][0] = fullName.replace(fullName, fullName + "@#$^&8762345");
			} else if (i == 13) {
				data[i][0] = fullName.replace(fullName, "");
			} else {
				data[i][0] = fullName;
			}

			if (i == 4) {
				if (companyName.contains(",")) {
					companyName.replace(",", "ChargeAutomation");
				}
				data[i][1] = companyName;
			} else if (i == 5) {
				if (companyName.contains(",")) {
					companyName.replace(",", "ChargeAutomation");
				}
				data[i][1] = companyName;
			} else if (i == 6) {
				if (companyName.contains(",")) {
					companyName.replace(",", "ChargeAutomation");
				}
				data[i][1] = companyName;
			} else if (i == 7) {
				if (companyName.contains(",")) {
					companyName.replace(",", "ChargeAutomation");
				}
				data[i][1] = companyName;
			} else if (i == 8) {
				if (companyName.contains(",")) {
					companyName.replace(",", "ChargeAutomation");
				}
				data[i][1] = companyName;
			} else if (i == 9) {
				if (companyName.contains(",")) {
					companyName.replace(",", "ChargeAutomation");
				}
				data[i][1] = companyName;
			} else if (i == 10) {
				if (companyName.contains(",")) {
					companyName.replace(",", "ChargeAutomation");
				}
				data[i][1] = companyName;
			} else if (i == 11) {
				if (companyName.contains(",")) {
					companyName.replace(",", "ChargeAutomation");
				}
				data[i][1] = companyName;
			} else if (i == 12) {
				if (companyName.contains(",")) {
					companyName.replace(",", "ChargeAutomation");
				}
				data[i][1] = companyName;
			} else if (i == 14) {
				data[i][1] = companyName.replace(companyName, "");
			} else {
				data[i][1] = companyName;
			}

			if (i == 4) {
				data[i][2] = telePhone.replace(telePhone, "ChargeAutomation");
			} else if (i == 5) {
				data[i][2] = telePhone.replace(telePhone, "!@#$%^&*()_+~");
			} else if (i == 15) {
				data[i][2] = telePhone.replace(telePhone, "");
			} else {
				data[i][2] = telePhone;
			}

			if (i == 9) {
				data[i][3] = email.replace(email, "ChargeAutomation");
			} else if (i == 10) {
				data[i][3] = email.replace(email, "763542874321");
			} else if (i == 11) {
				data[i][3] = email.replace(email, email + "*&%$");
			} else if (i == 16) {
				data[i][3] = email.replace(email, "");
			} else {
				data[i][3] = email;
			}

			if (i == 17) {
				data[i][4] = password.replace(password, "");
			} else {
				data[i][4] = password;
			}

			if (i == 18) {
				data[i][5] = confirmPassword.replace(confirmPassword, "");
			} else {
				data[i][5] = confirmPassword;
			}

			data[i][6] = pms;
			if (i == 12) {
				data[i][7] = false;
			} else {
				data[i][7] = true;
			}
		}
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				System.out.println("Data Array ==== " + data[i][j]);
			}
		}

		return data;
	}

}
