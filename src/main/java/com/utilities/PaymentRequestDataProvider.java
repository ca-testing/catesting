package com.utilities;

import org.testng.annotations.DataProvider;

public class PaymentRequestDataProvider {

	@DataProvider(name = "createPaymentRequest")
	public Object[][] getPaymentRequestData() {
		int row = 1;
		int column = 6;

		Object[][] data = new Object[row][column];

		data[0][0] = "smoobu_new@yopmail.com";
		data[0][1] = "Rajan@123";

		data[0][2] = "pgtest@yopmail.com";
		// data[0][2] = "";
		data[0][3] = "100";
		// data[0][3] = "";
		data[0][4] = "INR";
		data[0][5] = "Testing Payment";

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				System.out.println(data[i][j]);
			}
		}

		return data;
	}

	@DataProvider(name = "createPaymentWithMoreSetting")
	public Object[][] getPaymentRequestDataWithMoreSetting() {
		int row = 4;
		int column = 11;

		Object[][] data = new Object[row][column];

		data[0][0] = "smoobu_new@yopmail.com";
		data[0][1] = "Rajan@123";

		data[0][2] = "pgtest@yopmail.com";
		data[0][3] = "100";
		data[0][4] = "INR";
		data[0][5] = "Testing Payment with more setting";
		data[0][6] = "Mon, Dec 6, 2021 2:10 PM";
		data[0][7] = "Mon, Dec 6, 2021 2:10 PM";
		data[0][8] = "true";
		data[0][9] = "This is the test payment Terms & Condition";
		// data[0][10] = "Send Payment Link";
		data[0][10] = "Create Payment Link";
		// data[0][10] = "Charge Now";

		data[1][0] = "smoobu_new@yopmail.com";
		data[1][1] = "Rajan@123";

		data[1][2] = "pgtest@yopmail.com";
		data[1][3] = "100";
		data[1][4] = "INR";
		data[1][5] = "Testing Payment with more setting";
		data[1][6] = "Mon, Dec 6, 2021 2:10 PM";
		data[1][7] = "Mon, Dec 6, 2021 2:10 PM";
		data[1][8] = "true";
		data[1][9] = "This is the test payment Terms & Condition";
		// data[1][10] = "Create Payment Link";
		data[1][10] = "Send Payment Link";

		data[2][0] = "smoobu_new@yopmail.com";
		data[2][1] = "Rajan@123";

		data[2][2] = "pgtest@yopmail.com";
		data[2][3] = "100";
		data[2][4] = "INR";
		data[2][5] = "Testing Payment with more setting";
		data[2][6] = "Mon, Dec 6, 2021 2:10 PM";
		data[2][7] = "Mon, Dec 6, 2021 2:10 PM";
		data[2][8] = "true";
		data[2][9] = "This is the test payment Terms & Condition";
		data[2][10] = "Charge Now";

		data[3][0] = "smoobu_new@yopmail.com";
		data[3][1] = "Rajan@123";

		data[3][2] = "pgtest@yopmail.com";
		data[3][3] = "100";
		data[3][4] = "INR";
		data[3][5] = "Testing Payment with more setting";
		data[3][6] = "Mon, Dec 6, 2021 2:10 PM";
		data[3][7] = "Mon, Dec 6, 2021 2:10 PM";
		data[3][8] = "true";
		data[3][9] = "This is the test payment Terms & Condition";
		data[3][10] = "Schedule Charge";

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				System.out.println(data[i][j]);
			}
		}
		return data;
	}

	@DataProvider(name = "createAuthorizePaymentWithMoreSetting")
	public Object[][] getAuthorizePaymentRequestDataWithMoreSetting() {
		int row = 4;
		int column = 11;

		Object[][] data = new Object[row][column];

		data[0][0] = "smoobu_new@yopmail.com";
		data[0][1] = "Rajan@123";

		data[0][2] = "pgtest@yopmail.com";
		data[0][3] = "100";
		data[0][4] = "INR";
		data[0][5] = "Testing Payment with more setting";
		data[0][6] = "Mon, Dec 6, 2021 2:10 PM";
		data[0][7] = "Mon, Dec 6, 2021 2:10 PM";
		data[0][8] = "true";
		data[0][9] = "This is the test payment Terms & Condition";
		// data[0][10] = "Send Payment Link";
		// data[0][10] = "Schedule Authorize";
		data[0][10] = "Send Authorize Link";
		// data[0][10] = "Authorize Now";

		data[1][0] = "smoobu_new@yopmail.com";
		data[1][1] = "Rajan@123";

		data[1][2] = "pgtest@yopmail.com";
		data[1][3] = "100";
		data[1][4] = "INR";
		data[1][5] = "Testing Payment with more setting";
		data[1][6] = "Mon, Dec 6, 2021 2:10 PM";
		data[1][7] = "Mon, Dec 6, 2021 2:10 PM";
		data[1][8] = "true";
		data[1][9] = "This is the test payment Terms & Condition";
		// data[1][10] = "Create Payment Link";
		// data[1][10] = "Send Authorize Link";
		data[1][10] = "Create Authorize Link";

		data[2][0] = "smoobu_new@yopmail.com";
		data[2][1] = "Rajan@123";

		data[2][2] = "pgtest@yopmail.com";
		data[2][3] = "100";
		data[2][4] = "INR";
		data[2][5] = "Testing Payment with more setting";
		data[2][6] = "Mon, Dec 6, 2021 2:10 PM";
		data[2][7] = "Mon, Dec 6, 2021 2:10 PM";
		data[2][8] = "true";
		data[2][9] = "This is the test payment Terms & Condition";
		data[2][10] = "Authorize Now";

		data[3][0] = "smoobu_new@yopmail.com";
		data[3][1] = "Rajan@123";

		data[3][2] = "pgtest@yopmail.com";
		data[3][3] = "100";
		data[3][4] = "INR";
		data[3][5] = "Testing Payment with more setting";
		data[3][6] = "Mon, Dec 6, 2021 2:10 PM";
		data[3][7] = "Mon, Dec 6, 2021 2:10 PM";
		data[3][8] = "true";
		data[3][9] = "This is the test payment Terms & Condition";
		data[3][10] = "Schedule Authorize";

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				System.out.println(data[i][j]);
			}
		}
		return data;
	}

	@DataProvider(name = "refundPayment")
	public Object[][] refundPaymentRequestData() {
		int row = 2;
		int column = 5;

		Object[][] data = new Object[row][column];

		/*
		 * Data for Authorized
		 */

		data[0][0] = "smoobu_new@yopmail.com";
		data[0][1] = "Rajan@123";

		data[0][2] = "pgtest@yopmail.com";
		data[0][3] = "100";
		data[0][4] = "Testing Payment";

		data[1][0] = "smoobu_new@yopmail.com";
		data[1][1] = "Rajan@123";

		data[1][2] = "pgtest@yopmail.com";
		data[1][3] = "50";
		data[1][4] = "Testing partial Refund Payment";

		/*
		 * Data for Paid
		 */

		/*
		 * data[2][0] = "smoobu_new@yopmail.com"; data[2][1] = "Rajan@123";
		 * 
		 * data[2][2] = "pgtest@yopmail.com"; data[2][3] = "100"; data[2][4] =
		 * "Testing Refund Payent";
		 * 
		 * 
		 * data[3][0] = "smoobu_new@yopmail.com"; data[3][1] = "Rajan@123";
		 * 
		 * data[3][2] = "pgtest@yopmail.com"; data[3][3] = "50"; data[3][4] =
		 * "Testing partial Refund Payment";
		 * 
		 */

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				System.out.println(data[i][j]);
			}
		}

		return data;
	}

	@DataProvider(name = "paymentCredential")
	public Object[][] getCredential() {
		int row = 1;
		int column = 2;
		Object[][] data = new Object[row][column];

		data[0][0] = "smoobu_new@yopmail.com";
		data[0][1] = "Rajan@123";

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				System.out.println(data[i][j]);
			}
		}

		return data;
	}

	@DataProvider(name="searchingData")
	public Object[][] searchData() {
		int row = 3;
		int column = 3;
		Object[][] data = new Object[row][column];

		data[0][0] = "smoobu_new@yopmail.com";
		data[0][1] = "Rajan@123";
		data[0][2] = "YWwsH8";

		data[1][0] = "smoobu_new@yopmail.com";
		data[1][1] = "Rajan@123";
		data[1][2] = "shahbaz.f@yopmail.com";
		
		data[2][0] = "smoobu_new@yopmail.com";
		data[2][1] = "Rajan@123";
		data[2][2] = "cv1692@yopmail.com";

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				System.out.println(data[i][j]);
			}
		}
		return data;
	}

}
