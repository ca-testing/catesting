package com.utilities;

import org.testng.annotations.DataProvider;

public class BookARoomDataProvider {

	@DataProvider(name = "RoomBooking")
	public Object[][] bookingRoomData() {
		int row = 1;
		int column = 22;

		Object[][] data = new Object[row][column];

		data[0][0] = "success@chargeautomation.com";
		data[0][1] = "Rajan@123";

		data[0][2] = "Single Booking";
		// data[0][3] = "1461 -- Hamill-Paucek";
		//data[0][3] = "1461 -- Automation Test Account";
		//data[0][3] = "1461 -- O'Hara Inc";
		data[0][3] = "1461 -- Roob LLC";
		data[0][4] = "The Grand Hotel Sharadanagar Kanpur";
		data[0][5] = "Single Room";
		// data[0][6] = "Expedia - 14";Booking.com - 19
		 data[0][6] = "Google - 58";
		//data[0][6] = "Booking.com - 19";
		//data[0][7] = "Virtual Card";
		data[0][7] = "Credit/Debit Card";
		data[0][8] = "pgtest@yopmail.com";
		data[0][9] = "Test Booking";
		data[0][10] = "VISA";

		data[0][11] = "pgtest@yopmail.com";
		data[0][12] = "Rajan@123";

		data[0][13] = "10";
		data[0][14] = "5";
		// data[0][15] = "Car";
		data[0][15] = "Flight";
		// data[0][15] = "Other";
		data[0][16] = "13:00";
		data[0][17] = "30";
		data[0][18] = "4242424242424242";
		data[0][19] = "1230";
		data[0][20] = "999";
		data[0][21] = "22222";

		/*
		 * data[1][0] = "success@chargeautomation.com"; data[1][1] = "Rajan@123";
		 * 
		 * data[1][2] = "Single Booking"; //data[0][3] = "1461 -- Hamill-Paucek";
		 * data[1][3] = "1461 -- Automation Test Account"; data[1][4] =
		 * "The Grand Hotel Sharadanagar Kanpur"; data[1][5] = "Single Room";
		 * //data[0][6] = "Expedia - 14";Booking.com - 19 data[1][6] = "Google - 58";
		 * //data[0][6] = "Booking.com - 19"; data[1][7] = "Virtual Card"; data[1][8] =
		 * "pgtest@yopmail.com"; data[1][9] = "Test Booking"; data[1][10] = "VISA";
		 * 
		 * data[1][11] = "pgtest@yopmail.com"; data[1][12] = "Rajan@123";
		 */

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				System.out.println(data[i][j]);
			}
		}
		return data;
	}

	@DataProvider(name = "sendPre_CheckinEmail")
	public Object[][] bookARoomFor_SendPre_CheckinEmail() {
		int row = 1;
		int column = 22;

		Object[][] data = new Object[row][column];

		data[0][0] = "success@chargeautomation.com";
		data[0][1] = "Rajan@123";

		data[0][2] = "Single Booking";
		// data[0][3] = "1461 -- Hamill-Paucek";
		// data[0][3] = "1461 -- Automation Test Account";
		//data[0][3] = "1461 -- O'Hara Inc";
		data[0][3] = "1461 -- Roob LLC";
		data[0][4] = "The Grand Hotel Sharadanagar Kanpur";
		data[0][5] = "Single Room";
		// data[0][6] = "Expedia - 14";Booking.com - 19
		 data[0][6] = "Google - 58";
		//data[0][6] = "Booking.com - 19";
		data[0][7] = "Virtual Card";
		data[0][8] = "pgtest@yopmail.com";
		data[0][9] = "Test Booking";
		data[0][10] = "VISA";

		data[0][11] = "pgtest@yopmail.com";
		data[0][12] = "Rajan@123";

		data[0][13] = "10";
		data[0][14] = "5";
		// data[0][15] = "Car";
		data[0][15] = "Flight";
		// data[0][15] = "Other";
		data[0][16] = "13:00";
		data[0][17] = "30";
		data[0][18] = "4242424242424242";
		data[0][19] = "1230";
		data[0][20] = "999";
		data[0][21] = "22222";

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				System.out.println(data[i][j]);
			}
		}
		return data;
	}
	
	@DataProvider(name = "differentPayment")
	public Object[][] bookARoomFor_differentPayment() {
		int row = 1;
		int column = 22;

		Object[][] data = new Object[row][column];

		data[0][0] = "success@chargeautomation.com";
		data[0][1] = "Rajan@123";

		data[0][2] = "Single Booking";
		// data[0][3] = "1461 -- Hamill-Paucek";
		// data[0][3] = "1461 -- Automation Test Account";
		//data[0][3] = "1461 -- O'Hara Inc";
		data[0][3] = "1461 -- Roob LLC";
		data[0][4] = "The Grand Hotel Sharadanagar Kanpur";
		data[0][5] = "Single Room";
		// data[0][6] = "Expedia - 14";Booking.com - 19
		// data[0][6] = "Google - 58";
		data[0][6] = "Booking.com - 19";
		data[0][7] = "Virtual Card";
		data[0][8] = "pgtest@yopmail.com";
		data[0][9] = "Test Booking";
		data[0][10] = "VISA";

		data[0][11] = "pgtest@yopmail.com";
		data[0][12] = "Rajan@123";

		data[0][13] = "10";
		data[0][14] = "5";
		// data[0][15] = "Car";
		data[0][15] = "Flight";
		// data[0][15] = "Other";
		data[0][16] = "13:00";
		data[0][17] = "30";
		data[0][18] = "4242424242424242";
		data[0][19] = "1230";
		data[0][20] = "999";
		data[0][21] = "22222";

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				System.out.println(data[i][j]);
			}
		}
		return data;
	}
	
	@DataProvider(name = "bookingData")
	public Object[][] bookingData_FromBookingList() {
		int row = 1;
		int column = 22;

		Object[][] data = new Object[row][column];

		data[0][0] = "success@chargeautomation.com";
		data[0][1] = "Rajan@123";

		data[0][2] = "Single Booking";
		// data[0][3] = "1461 -- Hamill-Paucek";
		// data[0][3] = "1461 -- Automation Test Account";
		//data[0][3] = "1461 -- O'Hara Inc";
		data[0][3] = "1461 -- Roob LLC";
		data[0][4] = "The Grand Hotel Sharadanagar Kanpur";
		data[0][5] = "Single Room";
		// data[0][6] = "Expedia - 14";Booking.com - 19
		 data[0][6] = "Google";
		//data[0][6] = "Booking.com";
		data[0][7] = "Virtual Card";
		data[0][8] = "pgtest@yopmail.com";
		data[0][9] = "Test Booking";
		data[0][10] = "VISA";

		data[0][11] = "pgtest@yopmail.com";
		data[0][12] = "Rajan@123";

		data[0][13] = "10";
		data[0][14] = "5";
		// data[0][15] = "Car";
		data[0][15] = "Flight";
		// data[0][15] = "Other";
		data[0][16] = "13:00";
		data[0][17] = "30";
		data[0][18] = "4242424242424242";
		data[0][19] = "1230";
		data[0][20] = "999";
		data[0][21] = "22222";

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				System.out.println(data[i][j]);
			}
		}
		return data;
	}
	
	@DataProvider(name = "chargewithout3D")
	public Object[][] bookingData_bookingcom_FromBookingList() {
		int row = 1;
		int column = 22;

		Object[][] data = new Object[row][column];

		data[0][0] = "success@chargeautomation.com";
		data[0][1] = "Rajan@123";

		data[0][2] = "Single Booking";
		// data[0][3] = "1461 -- Hamill-Paucek";
		// data[0][3] = "1461 -- Automation Test Account";
		//data[0][3] = "1461 -- O'Hara Inc";
		data[0][3] = "1461 -- Roob LLC";
		data[0][4] = "The Grand Hotel Sharadanagar Kanpur";
		data[0][5] = "Single Room";
		// data[0][6] = "Expedia - 14";Booking.com - 19
		// data[0][6] = "Google";
		data[0][6] = "Booking.com";
		data[0][7] = "Virtual Card";
		data[0][8] = "pgtest@yopmail.com";
		data[0][9] = "Test Booking";
		data[0][10] = "VISA";

		data[0][11] = "pgtest@yopmail.com";
		data[0][12] = "Rajan@123";

		data[0][13] = "10";
		data[0][14] = "5";
		// data[0][15] = "Car";
		data[0][15] = "Flight";
		// data[0][15] = "Other";
		data[0][16] = "13:00";
		data[0][17] = "30";
		data[0][18] = "4242424242424242";
		data[0][19] = "1230";
		data[0][20] = "999";
		data[0][21] = "22222";

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				System.out.println(data[i][j]);
			}
		}
		return data;
	}

}
