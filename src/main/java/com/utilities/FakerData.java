package com.utilities;

import com.github.javafaker.Faker;

public class FakerData {

	public String getFakerEmail() {
		Faker faker = new Faker();
		String email = faker.internet().emailAddress();
		System.out.println("Email Id == " + email);
		return email;
	}
	
	public String getFakePassword() {
		Faker faker = new Faker();
		String pwd = faker.internet().password();
		return pwd;
	}

}
