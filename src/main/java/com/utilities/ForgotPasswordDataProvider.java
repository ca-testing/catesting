package com.utilities;

import org.testng.annotations.DataProvider;

import com.github.javafaker.Faker;

public class ForgotPasswordDataProvider {

	@DataProvider(name = "FogorPasswordEmail")
	public Object[][] getForgotPWDData() {
		Object[][] data = new Object[6][1];
		Faker faker = new Faker();
		data[0][0] = "shahbaz.f@yopmail.com";
		data[1][0] = "testautomation@mailsac.com";
		data[2][0] = "12345";

		for (int i = 3; i < 5; i++) {
			data[i][0] = faker.internet().emailAddress();
		}
		return data;
	}

}
