package com.utilities;

import java.util.ArrayList;

import org.testng.annotations.DataProvider;
import com.github.javafaker.Faker;
import com.page.TeamsComponent;

public class UpdateProfileDataProvider {

	@DataProvider(name = "ProfileUpdateDataProvider")
	public Object[][] getDataForUpdateProfile() {
		int row = 5;
		int column = 8;
		Object[][] data = new Object[row][column];
		Faker faker = new Faker();

		for (int i = 0; i < row; i++) {
			if (i == 0) {
				data[i][0] = "smoobu_new@yopmail.com";
				data[i][1] = "Rajan@123";
			} else if (i == 1) {
				data[i][0] = "shahbaz.f@yopmail.com";
				data[i][1] = "Admin#123";
			} else if (i == 2) {
				data[i][0] = "cv1692@yopmail.com";
				data[i][1] = "Rajan@123";
			} else if (i == 3) {
				data[i][0] = "rajanbed24_app@yopmail.com";
				data[i][1] = "Rajan@123";
			} else if (i == 4) {
				data[i][0] = "shahbaz.f@yopmail.com";
				data[i][1] = "Admin#123";
			}
			data[i][2] = faker.name().fullName();
			data[i][3] = faker.phoneNumber().cellPhone();
			data[i][4] = faker.address().cityName();
			data[i][5] = faker.company().name();
			data[i][6] = faker.internet().emailAddress();
			data[i][7] = faker.address().cityName();
		}

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				System.out.println(data[i][j]);
			}
		}

		return data;
	}

	@DataProvider(name = "passwordUpdateDataProvider")
	public Object[][] UpdatePassword() {
		int row = 6;
		int column = 5;

		Object[][] data = new Object[row][column];

		Faker faker = new Faker();

		for (int i = 0; i < row; i++) {
			if (i == 0) {
				data[i][0] = "pgtest@yopmail.com";
				data[i][1] = "Rajan@123";
				data[i][2] = "Rajan@123";
				data[i][3] = faker.internet().password();
				data[i][4] = faker.internet().password();
			} else if (i == 1) {
				data[i][0] = "smoobu_new@yopmail.com";
				data[i][1] = "Rajan@123";
				data[i][2] = "Rajan#123";
				data[i][3] = faker.internet().password();
				data[i][4] = faker.internet().password();
			} else if (i == 2) {
				data[i][0] = "cv1692@yopmail.com";
				data[i][1] = "Rajan@123";
				data[i][2] = "Rajan@123";
				data[i][3] = faker.internet().password();
				data[i][4] = faker.internet().password();
			} else if (i == 3) {
				data[i][0] = "rajanbed24_app@yopmail.com";
				data[i][1] = "Rajan@123";
				data[i][2] = "Rajan#123";
				// data[i][2] = "";
				data[i][3] = faker.internet().password();
				data[i][4] = faker.internet().password();
			} else if (i == 4) {
				data[i][0] = "shahbaz.f@yopmail.com";
				data[i][1] = "Admin#123";
				data[i][2] = "Admin#123";
				data[i][3] = "Admin#123";
				data[i][4] = "Admin#123";
			} else {
				data[i][0] = faker.internet().emailAddress();
				data[i][1] = faker.internet().password();
				data[i][2] = "Admin#123";
				data[i][3] = "Admin#123";
				data[i][4] = "Admin#123";
			}

		}
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				System.out.println(data[i][j]);
			}
		}

		return data;
	}

	@DataProvider(name = "TeamsData")
	public Object[][] teamsData() {
		Faker faker = new Faker();
		ArrayList<String> teamsEmailPg = new ArrayList<String>();
		ArrayList<String> teamsEmailsmo = new ArrayList<String>();
		ArrayList<String> teamsEmailcv = new ArrayList<String>();
		//ArrayList<String> teamEmailshah = new ArrayList<String>();
		ArrayList<String> teamEmailRajan = new ArrayList<String>();
		//int row = 12;
		int row = 3;
		int column = 12;
		Object[][] data = new Object[row][column];

		for (int i = 0; i < row; i++) {
			if (i == 0) {
				data[i][0] = "pgtest@yopmail.com";
				data[i][1] = "Rajan@123";
				data[i][2] = faker.name().firstName();
				data[i][3] = faker.name().lastName();
				String Email1 = faker.internet().emailAddress();
				data[i][4] = Email1;
				teamsEmailPg.add(Email1);
				TeamsComponent.teamsEmailList.put(data[i][0].toString(), teamsEmailPg);
				data[i][5] = faker.phoneNumber().cellPhone();
				data[i][6] = Constant.MANAGER_ROLE;
				data[i][7] = Constant.ACCOUNT_SETUP;
				data[i][8] = Constant.BOOKINGS;
				data[i][9] = Constant.GUEST_EXPERIENCE;
				data[i][10] = Constant.PREFERENCES;
				data[i][11] = Constant.PROPERTIES;
			} else if (i == 1) {
				data[i][0] = "pgtest@yopmail.com";
				data[i][1] = "Rajan@123";
				data[i][2] = faker.name().firstName();
				data[i][3] = faker.name().lastName();
				String Email2 = faker.internet().emailAddress();
				data[i][4] = Email2;
				teamsEmailPg.add(Email2);
				TeamsComponent.teamsEmailList.put(data[i][0].toString(), teamsEmailPg);
				data[i][5] = faker.phoneNumber().cellPhone();
				data[i][6] = Constant.MANAGER_ROLE;
				data[i][7] = "null";
				data[i][8] = "null";
				data[i][9] = Constant.GUEST_EXPERIENCE;
				data[i][10] = "null";
				data[i][11] = "null";
			} else if (i == 2) {
				data[i][0] = "pgtest@yopmail.com";
				data[i][1] = "Rajan@123";
				data[i][2] = faker.name().firstName();
				data[i][3] = faker.name().lastName();
				String Email3 = faker.internet().emailAddress();
				data[i][4] = Email3;
				teamsEmailPg.add(Email3);
				TeamsComponent.teamsEmailList.put(data[i][0].toString(), teamsEmailPg);
				data[i][5] = faker.phoneNumber().cellPhone();
				data[i][6] = Constant.MANAGER_ROLE;
				data[i][7] = Constant.ACCOUNT_SETUP;
				data[i][8] = "null";
				data[i][9] = "null";
				data[i][10] = "null";
				data[i][11] = "null";
			} else if (i == 3) {
				data[i][0] = "smoobu_new@yopmail.com";
				data[i][1] = "Rajan@123";
				data[i][2] = faker.name().firstName();
				data[i][3] = faker.name().lastName();
				String Email4 = faker.internet().emailAddress();
				data[i][4] = Email4;
				teamsEmailsmo.add(Email4);
				TeamsComponent.teamsEmailList.put(data[i][0].toString(), teamsEmailsmo);
				data[i][5] = faker.phoneNumber().cellPhone();
				data[i][6] = Constant.MANAGER_ROLE;
				data[i][7] = "null";
				data[i][8] = Constant.BOOKINGS;
				data[i][9] = "null";
				data[i][10] = "null";
				data[i][11] = "null";
			} else if (i == 4) {
				data[i][0] = "smoobu_new@yopmail.com";
				data[i][1] = "Rajan@123";
				data[i][2] = faker.name().firstName();
				data[i][3] = faker.name().lastName();
				String Email5 = faker.internet().emailAddress();
				data[i][4] = Email5;
				teamsEmailsmo.add(Email5);
				TeamsComponent.teamsEmailList.put(data[i][0].toString(), teamsEmailsmo);
				data[i][5] = faker.phoneNumber().cellPhone();
				data[i][6] = Constant.MANAGER_ROLE;
				data[i][7] = "null";
				data[i][8] = "null";
				data[i][9] = "null";
				data[i][10] = Constant.PREFERENCES;
				data[i][11] = "null";
			} else if (i == 5) {
				data[i][0] = "cv1692@yopmail.com";
				data[i][1] = "Rajan@123";
				data[i][2] = faker.name().firstName();
				data[i][3] = faker.name().lastName();
				String Email6 = faker.internet().emailAddress();
				data[i][4] = Email6;
				teamsEmailcv.add(Email6);
				TeamsComponent.teamsEmailList.put(data[i][0].toString(), teamsEmailcv);
				data[i][5] = faker.phoneNumber().cellPhone();
				data[i][6] = Constant.MANAGER_ROLE;
				data[i][7] = "null";
				data[i][8] = "null";
				data[i][9] = "null";
				data[i][10] = "null";
				data[i][11] = Constant.PROPERTIES;
			} else if (i == 6) {
				data[i][0] = "cv1692@yopmail.com";
				data[i][1] = "Rajan@123";
				data[i][2] = faker.name().firstName();
				data[i][3] = faker.name().lastName();
				String Email7 = faker.internet().emailAddress();
				data[i][4] = Email7;
				teamsEmailcv.add(Email7);
				TeamsComponent.teamsEmailList.put(data[i][0].toString(), teamsEmailcv);
				data[i][5] = faker.phoneNumber().cellPhone();
				data[i][6] = Constant.TEAM_ROLE;
				data[i][7] = Constant.ACCOUNT_SETUP;
				data[i][8] = Constant.BOOKINGS;
				data[i][9] = "null";
				data[i][10] = "null";
				data[i][11] = "null";
			} else if (i == 7) {
				data[i][0] = "rajanbed24_app@yopmail.com";
				data[i][1] = "Rajan@123";
				data[i][2] = faker.name().firstName();
				data[i][3] = faker.name().lastName();
				String Email8 = faker.internet().emailAddress();
				data[i][4] = Email8;
				teamEmailRajan.add(Email8);
				TeamsComponent.teamsEmailList.put(data[i][0].toString(), teamEmailRajan);
				data[i][5] = faker.phoneNumber().cellPhone();
				data[i][6] = Constant.TEAM_ROLE;
				data[i][7] = "null";
				data[i][8] = Constant.BOOKINGS;
				data[i][9] = Constant.GUEST_EXPERIENCE;
				data[i][10] = "null";
				data[i][11] = "null";
			} else if (i == 8) {
				data[i][0] = "rajanbed24_app@yopmail.com";
				data[i][1] = "Rajan@123";
				data[i][2] = faker.name().firstName();
				data[i][3] = faker.name().lastName();
				String Email9 = faker.internet().emailAddress();
				data[i][4] = Email9;
				teamEmailRajan.add(Email9);
				TeamsComponent.teamsEmailList.put(data[i][0].toString(), teamEmailRajan);
				data[i][5] = faker.phoneNumber().cellPhone();
				data[i][6] = Constant.MANAGER_ROLE;
				data[i][7] = "null";
				data[i][8] = "null";
				data[i][9] = "null";
				data[i][10] = Constant.PREFERENCES;
				data[i][11] = Constant.PROPERTIES;
			} else if (i == 9) {
				data[i][0] = "shahbaz.f@yopmail.com";
				data[i][1] = "Admin#123";
				data[i][2] = "null";
				data[i][3] = faker.name().lastName();
				data[i][4] = faker.internet().emailAddress();
				data[i][5] = faker.phoneNumber().cellPhone();
				data[i][6] = Constant.MANAGER_ROLE;
				data[i][7] = Constant.ACCOUNT_SETUP;
				data[i][8] = Constant.BOOKINGS;
				data[i][9] = Constant.GUEST_EXPERIENCE;
				data[i][10] = Constant.PREFERENCES;
				data[i][11] = Constant.PROPERTIES;
			} else if (i == 10) {
				data[i][0] = "shahbaz.f@yopmail.com";
				data[i][1] = "Admin#123";
				data[i][2] = faker.name().firstName();
				data[i][3] = "null";
				data[i][4] = faker.internet().emailAddress();
				data[i][5] = faker.phoneNumber().cellPhone();
				data[i][6] = Constant.MANAGER_ROLE;
				data[i][7] = Constant.ACCOUNT_SETUP;
				data[i][8] = Constant.BOOKINGS;
				data[i][9] = Constant.GUEST_EXPERIENCE;
				data[i][10] = Constant.PREFERENCES;
				data[i][11] = Constant.PROPERTIES;
			} else if (i == 11) {
				data[i][0] = "shahbaz.f@yopmail.com";
				data[i][1] = "Admin#123";
				data[i][2] = faker.name().firstName();
				data[i][3] = faker.name().lastName();
				data[i][4] = "null";
				data[i][5] = faker.phoneNumber().cellPhone();
				data[i][6] = Constant.MANAGER_ROLE;
				data[i][7] = Constant.ACCOUNT_SETUP;
				data[i][8] = Constant.BOOKINGS;
				data[i][9] = Constant.GUEST_EXPERIENCE;
				data[i][10] = Constant.PREFERENCES;
				data[i][11] = Constant.PROPERTIES;
			}
		}
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				System.out.println(data[i][j]);
			}
		}
		System.out.println("Teams HasMap --------------- " + TeamsComponent.teamsEmailList.values());
		return data;
	}

}
