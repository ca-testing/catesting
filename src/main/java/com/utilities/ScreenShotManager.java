package com.utilities;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.driverFactory.WebDriverFactory;

public class ScreenShotManager {
	
	public static String takeScreenShot(String screenShotName) {
		String dateFormat = new SimpleDateFormat("yyyyMMHHmmss").format(new Date());
		TakesScreenshot screenshot = (TakesScreenshot)WebDriverFactory.getDriver();
		File src = screenshot.getScreenshotAs(OutputType.FILE);
		String destination = System.getProperty("user.dir") + "/screenshots/" + screenShotName + dateFormat + ".png";
		File finalDestination = new File(destination);
		try {
			FileUtils.copyFile(src, finalDestination);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return destination;
	}

}
