package com.utilities;

public class Constant {
	
	public static final String CHROME_BROWSER = "CHROME";
	public static final String FIREFOX_BROWSER = "FIREFOX";
	public static final String CONFIG_FILE = "config";
	public static final int MAX_WAIT = 15;
	public static final String CONFIRM_EMAIL_HEADING = "Confirm Your Email";
	public static final String MAILSAC_MAIL_HEADING_Verify_EMAIL = "ChargeAutomation: Verify Your Email Address";
	public static final String MAILSAC_FORGOT_PWD_MAIL = "Reset Your ChargeAutomation Password";
	public static final String FORGOT_PWD_BLANK_EMAIL_ERROR_MSG = "The email field is required.";
	public static final String FORGOT_PWD_MAIL_SENT_MSG = "Email has been sent.";
	public static final String FORGOT_INVALID_EMAIL = "The email must be a valid email address.";
	public static final String PASSWORD_UPDATE_SUCCESS = "Password successfully updated.";
	public static final String PROVIDE_REGISTERED_EMAIL = "Please provide your registered email with ChargeAutomation.";
	
	
	/*
	 *  Teams Account access
	 */
	
	public static final String MANAGER_ROLE = "Manger";
	public static final String TEAM_ROLE = "Team";
	public static final String ACCOUNT_SETUP = "Account Setup";
	public static final String BOOKINGS = "Bookings";
	public static final String GUEST_EXPERIENCE = "Guest Experience";
	public static final String PREFERENCES = "Preferences";
	public static final String PROPERTIES = "Properties";
	public static final String TEAM_CREATE_SUCCESS = "Team Member successfully added.";
	public static final String INVITATION_RESEND = "Invitation successfully resend to team member.";
	public static final String DEACTIVATE_SUCCESS = "Team member successfully deactivated.";
	public static final String ACTIVATE_SUCCESS = "Team member successfully activated.";
	public static final String MEMBER_UPDATED = "Team Member record successfully update.";
	public static final String MEMBER_DELETED = "Team member successfully deleted.";
	
	
	/*
	 *  Payment Types
	 */
	public static final String SEND_PAYMENT_LINK = "Send Payment Link";
	public static final String CREATE_PAYMENT_LINK = "Create Payment Link";
	public static final String CHARGE_NOW = "Charge Now";
	public static final String SCHEDULE_CHARGE = "Schedule Charge";
	
	public static final String SEND_AUTHORIZE_LINK = "Send Authorize Link";
	public static final String CREATE_AUTHORIZE_LINK = "Create Authorize Link";
	public static final String AUTHORIZE_NOW = "Authorize Now";
	public static final String SCHEDULE_AUTHORIZE = "Schedule Authorize";
	
	/*
	 *  Payment Request
	 */
	
	public static final String PAYMENT_REQUEST = "Payment Request";
	public static final String PAYMENT_SUCCESS = "Your submission was successful";
	public static final String STATUS_PENDING = "Pending";
	public static final String STATUS_PAID = "Paid";
	public static final String MARK_AS_PAID = "Marked As Paid";
	public static final String PAYMENT_LINK_SUCCESS = "Payment link successfully sent!";
	public static final String STATUS_VOIDED = "Voided";
	public static final String STATUS_REFUND = "Refunded";
	public static final String PAYMENT_LINK_CREATE_SUCCESS = "Payment link successfully created!";
	public static final String AUTHORIZE = "Authorized";
	public static final String CAPTURED = "Captured";
	public static final String PARTIAL_RELEASE = "Partially Released";
	public static final String RELEASED = "Released";
	public static final String ALL_CURRENCIES = "All Currencies";
	public static final String POPULAR_CURRENCY = "Popular Currencies";
	public static final String PAYMENT_DECLINED = "Payment Declined! Try with another Card.";
	public static final String PAYMENT_CHARGED_SUCCESS = "Payment successfully charged.";
	public static final String ALL_PAYMENT_SUCCESS = "Payment success";
	public static final String STATUS_SCHEDULED = "Scheduled";
	public static final String AWAITING_APPROVAL = "Awaiting Approval";
	
	
	/*
	 *  Booking
	 */
	
	public static final String SINGLE_BOOKING = "Single Booking";
	public static final String GROUP_BOOKING = "Group Booking";
	
	/*
	 *  Booking Steps
	 */
	
	public static final String BASIC_INFO = "Basic Info";
	public static final String ARRIVAL = "Arrival";
	public static final String VERIFICATION = "Verification";
	public static final String SELF_PORTRAIT = "Self Portrait";
	public static final String ADD_ON_SERVICES = "Add-on Services";
	public static final String CREDIT_CARD = "Credit Card";
	public static final String QUESTIONNAIRE = "Questionnaire";

}
