package com.utilities;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentReport {
	
	public static ExtentReports extent;
	public static ExtentTest extentTest;
	public static ExtentReports extentSetup() {
		extent = new ExtentReports();
		
		String path = System.getProperty("user.dir")+"/Reports/ChargeAutomation.html";
		ExtentSparkReporter reporter = new ExtentSparkReporter(path);
		reporter.config().setTheme(Theme.DARK);
		reporter.config().setDocumentTitle("Test Result");
		reporter.config().setReportName("ChargeAutomation");
		
		extent.attachReporter(reporter);
		
		return extent;
		
	}

}
