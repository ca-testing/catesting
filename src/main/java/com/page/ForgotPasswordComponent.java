package com.page;

import org.openqa.selenium.WebDriver;

import com.pageLocators.ForgotPasswordLocators;
import com.utilities.Constant;
import com.utilities.DynamicWait;

public class ForgotPasswordComponent extends BasePage implements ForgotPasswordLocators {

	public ForgotPasswordComponent(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void clickOnForgotPWD() {
		DynamicWait.waitForClick(forgotPWDLink);
		clickOnButton(forgotPWDLink);
	}

	public void enterForgotEmail(String emails) {
		DynamicWait.waitForClick(forgotEmail);
		sendValueToTextBox(forgotEmail, emails);
	}

	public void clickOnChangePWDBtn() {
		DynamicWait.waitForClick(changePWDBtn);
		clickOnButton(changePWDBtn);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getValidationMsg() {
		String validationMsg = null;
		if (isElementPresent(errorAlert) > 0) {
			validationMsg = getTextFromElement(errorAlert);
		} else {
			DynamicWait.waitForClick(toaster);
			if (isElementPresent(toaster) > 0) {
				validationMsg = getTextFromElement(toaster);
			}
		}
		return validationMsg;
	}
	
	public String getToastMessage() {
		String textMsg = null;
		DynamicWait.waitForClick(toaster);
		if (isElementPresent(toaster) > 0) {
			textMsg = getTextFromElement(toaster);
		}
		System.out.println("Toast Message ---------------- "+textMsg);
		return textMsg;
	}

	public boolean isEmailCorrect() {
		boolean status = false;
		if (isElementPresent(errorAlert) > 0) {
			status = true;
		} else if (isElementPresent(toaster) > 0) {
			String errorMsg = getTextFromElement(toaster);
			if (errorMsg.equals(Constant.FORGOT_INVALID_EMAIL)) {
				status = true;
			}
		}
		return status;
	}

}
