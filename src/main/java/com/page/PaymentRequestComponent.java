package com.page;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.driverFactory.WebDriverFactory;
import com.pageLocators.ForgotPasswordLocators;
import com.pageLocators.PaymentsRequestsLocators;
import com.utilities.Constant;
import com.utilities.DynamicWait;

public class PaymentRequestComponent extends BasePage implements PaymentsRequestsLocators, ForgotPasswordLocators {

	public PaymentRequestComponent(WebDriver driver) {
		super(driver);
	}

	public String getPaymentRequestHeading() {
		String heading = null;
		DynamicWait.waitForClick(paymentRequestsHeading);
		int size = isElementPresent(paymentRequestsHeading);
		if (size > 0) {
			heading = getTextFromElement(paymentRequestsHeading);
		}
		return heading;
	}

	public void clickOnPaymentRequest() {
		DynamicWait.waitUntil(3);
		DynamicWait.waitForClick(paymentRequestMenus);
		List<WebElement> menu = WebDriverFactory.getDriver().findElements(paymentRequestMenus);
		for (WebElement element : menu) {
			if (element.getText().equals(Constant.PAYMENT_REQUEST)) {
				element.click();
				break;
			}
		}
	}

	public void createRequest() {
		DynamicWait.waitForClick(createRequestMenu);
		clickOnButton(createRequestMenu);
	}

	public void enterEmail_inForm(String emailId) {
		DynamicWait.waitForClick(email);
		sendValueToTextBox(email, emailId);
	}

	public void enterAmount(String amounts) {
		sendValueToTextBox(amount, amounts);
	}

	public void enterDescription(String descriptions) {
		sendValueToTextBox(description, descriptions);
	}

	public void clickOnPaymentLink() {
		DynamicWait.waitUntil(2);
		if (isElementPresent(sendPaymentLink_Btn) > 0) {
			clickOnButton(sendPaymentLink_Btn);
		} else if (isElementPresent(sendAuthorizeLink) > 0) {
			clickOnButton(sendAuthorizeLink);
		}

		DynamicWait.waitUntil(2);
	}

	public String getFromLink() {
		String value = null;
		String values = null;
		String link = null;
		try {
			DynamicWait.waitForClick(copyLink);
			clickOnButton(copyLink);
			DynamicWait.waitUntil(1);
			WebElement element = WebDriverFactory.getDriver().findElement(copyLinksTextBox);
			value = element.getAttribute("paymentLinkSentModal_linkCopyInput");
			values = element.getText();
			link = getTextJavaSrcipt(copyLinksTextBox);
			System.out.println("Attribute Value ------------ " + value);
			System.out.println("Payment Link ------------- " + link);
			System.out.println("Using getText ------- " + values);
			DynamicWait.waitUntil(2);
			DynamicWait.waitForClick(close_Btn);
			clickOnButton(close_Btn);
		} catch (Exception ex) {
			DynamicWait.waitForClick(copyLink_Schedule);
			clickOnButton(copyLink_Schedule);
			DynamicWait.waitUntil(1);
			WebElement element = WebDriverFactory.getDriver().findElement(copyLinkTextbox_Schedule);
			value = element.getAttribute("paymentScheduleConfirmationModal_linkCopyInput");
			values = element.getText();
			link = getTextJavaSrcipt(copyLinkTextbox_Schedule);
			System.out.println("Attribute Value ------------ " + value);
			System.out.println("Payment Link ------------- " + link);
			System.out.println("Using getText ------- " + values);
			clickOnButton(emailPaymentLink_Btn_Schedule);
			DynamicWait.waitForClick(close_Btn);
			DynamicWait.waitUntil(2);
			clickOnButton(close_Btn);
		}

		return link;
	}

	public String getRequestIdFromLinkScheduleCharge() {
		String value = null;
		String values = null;
		String link = null;
		DynamicWait.waitForClick(copyLink_Schedule);
		clickOnButton(copyLink_Schedule);
		DynamicWait.waitUntil(1);
		WebElement element = WebDriverFactory.getDriver().findElement(copyLinkTextbox_Schedule);
		value = element.getAttribute("paymentScheduleConfirmationModal_linkCopyInput");
		values = element.getText();
		link = getTextJavaSrcipt(copyLinkTextbox_Schedule);
		System.out.println("Attribute Value ------------ " + value);
		System.out.println("Payment Link ------------- " + link);
		System.out.println("Using getText ------- " + values);
		clickOnButton(emailPaymentLink_Btn_Schedule);
		DynamicWait.waitForClick(close_Btn);
		clickOnButton(close_Btn);

		return link;
	}

	public String getScheduleCreatePaymentLink() {
		String value = null;
		String values = null;
		String link = null;
		DynamicWait.waitForClick(copyLink_CreateLink);
		clickOnButton(copyLink_CreateLink);
		DynamicWait.waitUntil(1);
		WebElement element = WebDriverFactory.getDriver().findElement(copyLinkTextbox_CreateLink);
		value = element.getAttribute("paymentLinkCreatedModal_linkCopyInput");
		values = element.getText();
		link = getTextJavaSrcipt(copyLinkTextbox_CreateLink);
		System.out.println("Attribute Value ------------ " + value);
		System.out.println("Payment Link ------------- " + link);
		System.out.println("Using getText ------- " + values);
		clickOnButton(emailPaymentLink_Btn_CreateLink);
		DynamicWait.waitUntil(2);
		DynamicWait.waitForClick(close_Btn);
		clickOnButton(close_Btn);
		return link;
	}

	public String getRequestId_FromLink(String link) {
		String requestId = "null";
		String[] requestIds = link.split("/");
		requestId = requestIds[requestIds.length - 1];
		System.out.println("Extracted Request Id ----------- " + requestId);
		for (int i = 0; i < requestIds.length; i++) {
			System.out.println("All Splitted values --------- " + requestIds[i]);
		}
		return requestId;
	}

	public void payment(String url) {
		WebDriverFactory.getDriver().findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");
		WebDriverFactory.getDriver().navigate().to(url);
	}

	public void payAll() {
		// DynamicWait.waitForClick(checkCard);
		DynamicWait.waitUntil(5);
		clickOnButtonJS(checkCard);
		DynamicWait.waitForClick(payAllBtn);
		clickOnButtonJS(payAllBtn);
		DynamicWait.waitUntil(2);
	}

	public String getSuccessMsg() {
		String msg = "null";
		if (isElementPresent(toaster) > 0) {
			msg = getTextFromElement(toaster);
		} else if (isElementPresent(successMsg) > 0) {
			msg = getTextFromElement(successMsg);
			// clickOnButton(oK_Btn);
		}
		return msg;
	}

	public String isPaymentSuccess() {
		String msg = "null";
		DynamicWait.waitForClick(successMsg);
		msg = getTextFromElement(successMsg);
		System.out.println("Success Message ---------- " + msg);
		return msg;
	}

	public String verifyIcon(int row) {
		String icon = "null";
		String lockLocator = lock_first + row + lock_second + "[" + (row - 2) + "]";
		System.out.println("Payment Icon Locator ------------- " + lockLocator);
		DynamicWait.waitForClick(By.xpath(lockLocator));
		WebElement ele = WebDriverFactory.getDriver().findElement(By.xpath(lockLocator));
		String value = ele.getAttribute("class");
		if (value.contains("dollar-sign")) {
			icon = "Dollar";
		} else if (value.contains("lock")) {
			icon = "Lock";
		}
		return icon;
	}

	public String verifySheild(int row) {
		String shield = "null";
		String shieldLocator = shieldStatusFirst + row + shieldStatusSecond;
		System.out.println("Shield Locator -------- " + shieldLocator);
		DynamicWait.waitForClick(By.xpath(shieldLocator));
		WebElement ele = WebDriverFactory.getDriver().findElement(By.xpath(shieldLocator));
		String value = ele.getAttribute("class");
		if (value.contains("shield")) {
			shield = "shield";
		}
		return shield;
	}

	public boolean isShieldPresent(int row) {
		boolean status = false;
		String shieldLocator = shieldStatusFirst + row + shieldStatusSecond;
		System.out.println("Shield Locator ------- " + shieldLocator);
		//DynamicWait.waitForClick(By.xpath(shieldLocator));
		DynamicWait.waitUntil(5);
		if (isElementPresent(By.xpath(shieldLocator)) > 0) {
			status = true;
		}
		return status;
	}

	public String getEmailId(int row) {
		boolean shield = isShieldPresent(row);
		String email = "null";
		if (shield) {
			String emailLocator = firstLocator + row + secondLocator + 10 + thirdLocator;
			System.out.println("Email Locator --------- " + emailLocator);
			email = getTextFromElement(By.xpath(emailLocator));
		} else {
			String emailLocator = emailIdFirst + row + emailIdSecond;
			System.out.println("Email Locator -----------" + emailLocator);
			email = getTextFromElement(By.xpath(emailLocator));
		}
		return email;
	}

	public String getAmount(int row) {
		boolean shield = isShieldPresent(row);
		String amount = "null";
		if (shield) {
			String amountLocator = firstLocator + row + secondLocator + 4 + thirdLocator;
			System.out.println("Amount Locator ------------- " + amountLocator);
			amount = getTextFromElement(By.xpath(amountLocator));
		} else {
			String amountLocator = paymentAmountFirst + row + paymentAmountSecond;
			System.out.println("Payment Amount Locator ------------ " + amountLocator);
			amount = getTextFromElement(By.xpath(amountLocator));
		}
		amount = amount.replaceAll("[^0-9]", "");
		return amount;
	}

	public String getDueDate(int row) {
		boolean shield = isShieldPresent(row);
		String dueDate = "null";
		if (shield) {
			String dueDateLocator = firstLocator + row + secondLocator + 11 + thirdLocator;
			System.out.println("Due Date Locator ------------ " + dueDateLocator);
			dueDate = getTextFromElement(By.xpath(dueDateLocator));
		} else {
			String dueDateLocator = dueDateFirst + row + dueDateSecond;
			System.out.println("Due date locator -------------- " + dueDateLocator);
			dueDate = getTextFromElement(By.xpath(dueDateLocator));
		}
		return dueDate;
	}

	public String getCreatedDate(int row) {
		boolean shield = isShieldPresent(row);
		String createDate = "null";
		if (shield) {
			String createdDateLocator = firstLocator + row + secondLocator + 12 + thirdLocator;
			System.out.println("Created Date Locator -------------- " + createdDateLocator);
			createDate = getTextFromElement(By.xpath(createdDateLocator));
		} else {
			String createDateLocator = createdDateFirst + row + createdDateSecond;
			System.out.println("Created date locator ---------- " + createDateLocator);
			createDate = getTextFromElement(By.xpath(createDateLocator));
		}
		return createDate;
	}

	public String getcurrentDate() {
		String date = getTodayDate();
		return date;
	}

	public void clickOnOpenMoreOptionInRow(int row, String paymentType) {
		try {
			if (paymentType.equals(Constant.AUTHORIZE)) {
				String moreValueOption = moreValueFirst + row + moreValueSecond + "[" + 8 + "]";
				clickOnButton(By.xpath(moreValueOption));

			} else {
				String moreValueOption = moreValueFirst + row + moreValueSecond + "[" + 9 + "]";
				clickOnButton(By.xpath(moreValueOption));
			}
		}catch(Exception ex) {
			clickOnButton(By.xpath("//*[@id='app']/div[1]/div[1]/div/div[2]/div[2]/div/div[3]/a"));
		}
	}

	public void clickOnOpenMoreOptionInRow(int row, String paymentType, String status) {
		if (paymentType.equals(Constant.AUTHORIZE)) {
			String moreValueOption = moreValueFirst + row + moreValueSecond + "[" + 8 + "]";
			clickOnButton(By.xpath(moreValueOption));
		} else if (status.equals(Constant.AWAITING_APPROVAL) && (!paymentType.equals(Constant.AUTHORIZE))) {
			String moreValueOption = moreValueFirst + row + moreValueSecond + "[" + 6 + "]";
			clickOnButton(By.xpath(moreValueOption));
		} else if (status.equals(Constant.AWAITING_APPROVAL) && (paymentType.equals(Constant.AUTHORIZE_NOW))) {
			String moreValueOption = moreValueFirst + row + moreValueSecond + "[" + 6 + "]";
			clickOnButton(By.xpath(moreValueOption));
		} else {
			String moreValueOption = moreValueFirst + row + moreValueSecond + "[" + 9 + "]";
			clickOnButton(By.xpath(moreValueOption));
		}
	}

	public String getScheduleDate() {
		DynamicWait.waitForClick(schedule_Date);
		String date = getTextFromElement(schedule_Date);
		return date;
	}

	public String getExpiredDate() {
		String date = getTextFromElement(expired_Date);
		return date;
	}

	public String getChargeProtected() {
		String chargeProtected = getTextFromElement(chargeBack);
		return chargeProtected;
	}

	public String getPaymentMethod() {
		String paymentMethods = getTextFromElement(paymentMethod);
		return paymentMethods;
	}

	public String getDescription() {
		String desc = getTextFromElement(description_InMore);
		return desc;
	}

	public String getTermCondition() {
		String terms = getTextFromElement(termsCondition_InMore);
		return terms;
	}

	public void clickOnManageRequest() {
		DynamicWait.waitForClick(manageRequestMenu);
		clickOnButton(manageRequestMenu);
	}

	public int paymentBegins(String status) {
		int row = getRowNumber(status);
		int result = 0;
		if (row > 0) {
			By pending = By.xpath(paymentStatusFirst + row + paymentStatusSecond);
			System.out.println("Pending Status locator ---------------- " + pending);
			if (getTextFromElement(pending).equals(status)) {
				By moreMenu = By.xpath(moreOptionFirst + row + moreOptionSecond);
				System.out.println("More Option Locator --------------- " + moreMenu);
				clickOnButton(moreMenu);
				String chargeNaowStr = chargeNowFirst + row + chargeNowSecond + "[" + 5 + "]";
				By charge = By.xpath(chargeNaowStr);
				System.out.println("Charge Noe Locator ------------------ " + charge);
				DynamicWait.waitForClick(charge);
				if (getTextFromElement(charge).contains("Charge Now")
						|| getTextFromElement(charge).equals("Authorize Now")) {
					clickOnButton(charge);
					result = row;
				} else {
					String chargeNowStr2 = chargeNowFirst + row + chargeNowSecond + "[" + 6 + "]";
					By chargeNow = By.xpath(chargeNowStr2);
					System.out.println("Charge Now locator ------------- " + chargeNow);
					clickOnButton(chargeNow);
					result = row;
				}
			}
		}
		return result;
	}

	public void payPayment() {
		DynamicWait.waitForClick(cancelBtn_AtPopup);
		clickOnButton(paymentCardId);
		DynamicWait.waitForClick(paymentFirstCard);
		clickOnButton(paymentFirstCard);
		DynamicWait.waitForClick(chargeNow_BtnForPayment);
		clickOnButton(chargeNow_BtnForPayment);
	}

	public String getPaymentStatus(int rowNo) {
		DynamicWait.waitUntil(7);
		By statusText = By.xpath(paymentStatusFirst + rowNo + paymentStatusSecond);
		System.out.println("Status Locator --------------- " + statusText);
		DynamicWait.waitForClick(statusText);
		String status = getTextFromElement(statusText);
		System.out.println("Status of the Payment ------------- " + status);
		return status;
	}

	public int markasPaid(String status) {
		int row = getRowNumber(status);
		int result = 0;
		if (row > 0) {
			By pending = By.xpath(paymentStatusFirst + row + paymentStatusSecond);
			System.out.println("Pending status locator ---------------- " + pending);
			if (getTextFromElement(pending).equals(status)) {
				By moreMenu = By.xpath(moreOptionFirst + row + moreOptionSecond);
				System.out.println("More Option Locator ---------------- " + moreMenu);
				clickOnButton(moreMenu);
				String markPaidStr = markAsPaidFirst + row + markAsPaidSecond + "[" + 6 + "]";
				By markPaid = By.xpath(markPaidStr);
				System.out.println("Mark as Paid Locator ----------- " + markPaid);
				DynamicWait.waitForClick(markPaid);
				if (getTextFromElement(markPaid).contains("Mark as Paid")) {
					clickOnButton(markPaid);
					DynamicWait.waitForClick(yesDoIt);
					clickOnButton(yesDoIt);
					result = row;
				} else {
					String markPaidStr1 = markAsPaidFirst + row + markAsPaidSecond + "[" + 7 + "]";
					By markPaid1 = By.xpath(markPaidStr1);
					System.out.println("Mark as Paid Locator ----------- " + markPaid1);
					clickOnButton(markPaid1);
					DynamicWait.waitForClick(yesDoIt);
					clickOnButton(yesDoIt);
					result = row;
				}
			}
		}
		return result;
	}

	public int sendPaymentRequestAgain() {
		int row = 0;
		int cardCount = isElementPresent(totalCard);
		for (int i = 3; i <= cardCount; i++) {
			By scrollValue = By.xpath(paymentStatusFirst + (i - 1) + "]");
			System.out.println("Scroll Value Locators -------------------- " + scrollValue);
			scrollToElement(scrollValue);
			By pending = By.xpath(paymentStatusFirst + i + paymentStatusSecond);
			System.out.println("Pending Status locator -------------- " + pending);
			String status = getTextFromElement(pending);
			if (status.contains("Pending")) {
				By moreMenu = By.xpath(moreOptionFirst + i + moreOptionSecond);
				System.out.println("More Option Locator ------------- " + moreMenu);
				clickOnButton(moreMenu);
				By sendAgain = By.xpath(sendFirstPart + i + sendSecondPart);
				System.out.println("Send Again locator -------------- " + sendAgain);
				DynamicWait.waitForClick(sendAgain);
				clickOnButton(sendAgain);
				DynamicWait.waitForClick(yesDoIt);
				clickOnButton(yesDoIt);
				row = i;
				break;
			}
		}
		return row;
	}

	public int sendRequestAgain(String status) {
		int row = getRowNumber(status);
		int result = 0;
		if (row > 0) {
			By pending = By.xpath(paymentStatusFirst + row + paymentStatusSecond);
			System.out.println("Payment Status locator ---------- " + pending);
			if (getTextFromElement(pending).equals(status)) {
				By moreMenu = By.xpath(moreOptionFirst + row + moreOptionSecond);
				System.out.println("More Option Locator ------------- " + moreMenu);
				clickOnButton(moreMenu);
				String sendAgainStr = sendFirstPart + row + sendSecondPart + "[" + 7 + "]";
				By sendAgain = By.xpath(sendAgainStr);
				System.out.println("Send Again Locator ----------- " + sendAgain);
				DynamicWait.waitForClick(sendAgain);
				if (getTextFromElement(sendAgain).contains("Send")) {
					clickOnButton(sendAgain);
					DynamicWait.waitForClick(yesDoIt);
					clickOnButton(yesDoIt);
					result = row;
				} else {
					String sendAgainStr1 = sendFirstPart + row + sendSecondPart + "[" + 2 + "]";
					By sendAgain1 = By.xpath(sendAgainStr1);
					System.out.println("Send Again Locator ---------- " + sendAgain1);
					clickOnButton(sendAgain1);
					DynamicWait.waitForClick(yesDoIt);
					clickOnButton(yesDoIt);
					result = row;
				}
			}
		}
		return result;
	}

	public String getPaymentlinksuccess_Msg() {
		// DynamicWait.waitForClick(paymentLinkSuccess);
		String msg = null;
		if (isElementPresent(paymentLinkSuccess) > 0) {
			msg = getTextFromElement(paymentLinkSuccess);
			System.out.println("Payment link sent success message ------------- " + msg);
			DynamicWait.waitForClick(close_Btn);
			clickOnButton(close_Btn);
		}

		return msg;
	}

	public int editPaymentRequest() {
		int row = 0;
		int cardCount = isElementPresent(totalCard);
		for (int i = 3; i <= cardCount; i++) {
			By scrollValue = By.xpath(paymentStatusFirst + (i - 1) + "]");
			System.out.println("Pending Status locator ---------- " + scrollValue);
			scrollToElement(scrollValue);
			By pending = By.xpath(paymentStatusFirst + i + paymentStatusSecond);
			System.out.println("Pending Status locator -------------- " + pending);
			String status = getTextFromElement(pending);
			if (status.contains("Pending")) {
				By moreMenu = By.xpath(moreOptionFirst + i + moreOptionSecond);
				System.out.println("More Option Locator ------------------ " + moreMenu);
				clickOnButton(moreMenu);
				By editPayment = By.xpath(editFirstPart + i + editSecondPart);
				System.out.println("Edit Locator -------------- " + editPayment);
				if (getTextFromElement(editPayment).contains("Edit")) {
					DynamicWait.waitForClick(editPayment);
					clickOnButton(editPayment);
					row = i;
					break;
				} else {
					clickOnButton(moreMenu);
				}
			}
		}
		return row;
	}

	public int editPaymentRequests(String status) {
		int row = getRowNumber(status);
		int result = 0;
		if (row > 0) {
			By pending = By.xpath(paymentStatusFirst + row + paymentStatusSecond);
			System.out.println("Pending status locator ----------- " + pending);
			if (getTextFromElement(pending).equals(status)) {
				By moreMenu = By.xpath(moreOptionFirst + row + moreOptionSecond);
				System.out.println("More Option locator -------------- " + moreMenu);
				clickOnButton(moreMenu);
				String editStr = editFirstPart + row + editSecondPart + "[" + 4 + "]";
				By editPayment = By.xpath(editStr);
				System.out.println("Edit Locator ------------------ " + editPayment);
				if (getTextFromElement(editPayment).contains("Edit")) {
					DynamicWait.waitForClick(editPayment);
					clickOnButton(editPayment);
					result = row;
				} else {
					String editStr1 = editFirstPart + row + editSecondPart + "[" + 5 + "]";
					By editPayment1 = By.xpath(editStr1);
					System.out.println("Edit Locator -------------- " + editPayment1);
					clickOnButton(editPayment1);
					result = row;
				}
			}
		}
		return result;
	}

	public int voidPayment() {
		int row = 0;
		int cardCount = isElementPresent(totalCard);
		for (int i = 3; i <= cardCount; i++) {
			By scrollValue = By.xpath(paymentStatusFirst + (i - 1) + "]");
			System.out.println("Scroll locator ------- " + scrollValue);
			scrollToElement(scrollValue);
			By pending = By.xpath(paymentStatusFirst + i + paymentStatusSecond);
			System.out.println("Pending Locator -------- " + pending);
			String status = getTextFromElement(pending);
			if (status.contains("Pending")) {
				By moreMenu = By.xpath(moreOptionFirst + i + moreOptionSecond);
				System.out.println("More Option locator -------------- " + moreMenu);
				clickOnButton(moreMenu);
				By voidPayment = By.xpath(voidFirst + i + voidSecond);
				System.out.println("Void Locator --------------- " + voidPayment);
				if (getTextFromElement(voidPayment).contains("Void")) {
					DynamicWait.waitForClick(voidPayment);
					clickOnButton(voidPayment);
					DynamicWait.waitForClick(yesDoIt);
					clickOnButton(yesDoIt);
					row = i;
					break;
				}
			}
		}
		return row;
	}

	public int getRowNumber(String Pendingstatus) {
		DynamicWait.waitUntil(3);
		int row = 0;
		int cardCount = isElementPresent(totalCard);
		for (int i = 3; i <= cardCount; i++) {
			By scrollValue = By.xpath(paymentStatusFirst + (i - 1) + "]");
			System.out.println("Scroll locator ------------- " + scrollValue);
			scrollToElement(scrollValue);
			By pending = By.xpath(paymentStatusFirst + i + paymentStatusSecond);
			System.out.println("Pending Status locator -------------- " + pending);
			String status = getTextFromElement(pending);
			if (status.equals(Pendingstatus)) {
				row = i;
				break;
			}
		}
		return row;
	}

	public int voidPayment2(String status) {
		int row = getRowNumber(status);
		int result = 0;
		if (row > 0) {
			By pending = By.xpath(paymentStatusFirst + row + paymentStatusSecond);
			System.out.println("Pending Status locator ------------ " + pending);
			if (getTextFromElement(pending).equals(status)) {
				By moreMenu = By.xpath(moreOptionFirst + row + moreOptionSecond);
				System.out.println("More Menu locator ----------------- " + moreMenu);
				clickOnButton(moreMenu);
				String voidStr = voidFirst + row + voidSecond + "[" + 2 + "]";
				By voidPayment = By.xpath(voidStr);
				System.out.println("Void Payment Locator --------------- " + voidPayment);
				if (getTextFromElement(voidPayment).contains("Void")) {
					DynamicWait.waitForClick(voidPayment);
					clickOnButton(voidPayment);
					DynamicWait.waitForClick(yesDoIt);
					clickOnButton(yesDoIt);
					result = row;
				} else {
					String voidStr2 = voidFirst + row + voidSecond + "[" + 3 + "]";
					By voidPayments = By.xpath(voidStr2);
					System.out.println("Void Payment locator ----------- " + voidPayments);
					if (getTextFromElement(voidPayments).equals("Void")) {
						DynamicWait.waitForClick(voidPayments);
						clickOnButton(voidPayments);
						DynamicWait.waitForClick(yesDoIt);
						clickOnButton(yesDoIt);
						result = row;
					}
				}
			}
		}
		return result;
	}

	public int refundPayment(String status, String refundAmount, String description) {
		int row = getRowNumber(status);
		int result = 0;
		if (row > 0) {
			By moreMenu = By.xpath(moreOptionFirst + row + moreOptionSecond);
			System.out.println("More menu locator --------------- " + moreMenu);
			clickOnButton(moreMenu);
			By refundLocator = By.xpath(paymentRefundFirst + row + paymentRefundSecond);
			System.out.println("Payment refund locators ---------------- " + refundLocator);
			if (getTextFromElement(refundLocator).equals("Refund")) {
				DynamicWait.waitForClick(refundLocator);
				clickOnButton(refundLocator);
				DynamicWait.waitForClick(refundAmountInput);
				sendValueToTextBox(refundAmountInput, refundAmount);
				sendValueToTextBox(refundDescription, description);
				clickOnButton(refundNowBtn);
				DynamicWait.waitForClick(yesRefundNow);
				clickOnButton(yesRefundNow);
				result = row;
			}
		}
		return result;
	}

	public void clickOnCreatePaymentLink() {
		DynamicWait.waitUntil(2);
		clickOnButton(sendPaymentLink_moreOption);
		DynamicWait.waitForClick(createPaymentLink);
		clickOnButton(createPaymentLink);
		DynamicWait.waitUntil(2);
	}

	public String create_PaymentLink() {
		DynamicWait.waitForClick(paymentLinkCreateSuccessMsg);
		String msg = getTextFromElement(paymentLinkCreateSuccessMsg);
		System.out.println("Payment Link created ------------- " + msg);
		return msg;
	}

	public void sendEmailPaymentLink() {
		DynamicWait.waitForClick(emailPaymentLink_Btn);
		clickOnButton(emailPaymentLink_Btn);
	}

	public void closeModelAfterCreatePayment() {
		DynamicWait.waitForClick(closeBtn_atModel);
		clickOnButton(closeBtn_atModel);
	}

	public String verifyPendingAfterCreateRequest() {
		String statusLocatorStr = paymentStatusFirst + 3 + paymentStatusSecond;
		By paymentStatus = By.xpath(statusLocatorStr);
		DynamicWait.waitForClick(paymentStatus);
		String status = getTextFromElement(paymentStatus);
		System.out.println("Status of the Payment -------- " + status);
		return status;
	}

	public void chargeNow_FromMenu() {
		clickOnButton(sendPaymentLink_moreOption);
		DynamicWait.waitForClick(chargeNow);
		clickOnButton(chargeNow);
	}

	public void getChargeNowWithoutCreateLink() {
		try {
			DynamicWait.waitForClick(paymentCard_WithoutCreateLink);
			clickOnButton(paymentCard_WithoutCreateLink);
			DynamicWait.waitForClick(paymentFirstCard);
			clickOnButton(paymentFirstCard);
			DynamicWait.waitForClick(chargeNow_WithoutCreateLinks);
			clickOnButton(chargeNow_WithoutCreateLinks);
			DynamicWait.waitUntil(3);
		} catch (Exception ex) {
			clickOnButton(paymentCard_Schedule);
			DynamicWait.waitForClick(paymentFirstCard);
			clickOnButton(paymentFirstCard);
			DynamicWait.waitForClick(chargeNow_Schedule);
			clickOnButton(chargeNow_Schedule);
			DynamicWait.waitUntil(3);
		}

	}

	public void getChargeSchedule() {
		DynamicWait.waitForClick(paymentCard_Schedule);
		clickOnButton(paymentCard_Schedule);
		DynamicWait.waitForClick(paymentFirstCard);
		clickOnButton(paymentFirstCard);
		DynamicWait.waitForClick(chargeNow_Schedule);
		clickOnButton(chargeNow_Schedule);
		DynamicWait.waitUntil(3);
	}

	public void clickOnChargeNow_WithoutCreateLink() {
		clickOnButton(sendPaymentLink_moreOption);
		DynamicWait.waitForClick(chargeNow_withoutCreateLink);
		clickOnButton(chargeNow_withoutCreateLink);
		DynamicWait.waitUntil(2);
	}

	public void clickOnScheduleCharge() {
		DynamicWait.waitForClick(sendPaymentLink_moreOption);
		clickOnButton(sendPaymentLink_moreOption);
		DynamicWait.waitForClick(scheduleCharge_Btn);
		clickOnButton(scheduleCharge_Btn);
		DynamicWait.waitUntil(2);
	}

	public String getRequestId(int row) {
		String requestIdValue = "null";
		boolean shield = isShieldPresent(row);
		if (shield) {
			String requestIdLocator = firstLocator + row + secondLocator + 9 + thirdLocator;
			System.out.println("Request Id Locator --------- " + requestIdLocator);
			requestIdValue = getTextFromElement(By.xpath(requestIdLocator));
			System.out.println("Request Id of the Payment ----------------- " + requestIdValue);
		} else {
			String requestId = requestIdFirst + row + requestIdSecond;
			By requestIdLocator = By.xpath(requestId);
			System.out.println("Request Id locator -------- " + requestIdLocator);
			requestIdValue = getTextFromElement(requestIdLocator);
			System.out.println("Request Id of the Payment ----------------- " + requestIdValue);
		}

		return requestIdValue;
	}

	public void clickOnAuthorizationTab() {
		DynamicWait.waitForClick(authorizationtab);
		clickOnButton(authorizationtab);
	}

	public void clickOnSendAuthorizeLink() {
		DynamicWait.waitForClick(sendAuthorizeLink);
		clickOnButton(sendAuthorizeLink);
		DynamicWait.waitUntil(2);
	}

	public void clickOnMorePaymentOption() {
		DynamicWait.waitForClick(sendPaymentLink_moreOption);
		clickOnButton(sendPaymentLink_moreOption);
	}

	public void clickOnCreateAuthorizationLink() {
		DynamicWait.waitForClick(createAuthorizationLink);
		clickOnButton(createAuthorizationLink);
		DynamicWait.waitUntil(2);
	}

	public void clickOnAuthorizeNow() {
		DynamicWait.waitForClick(authorizeNow);
		clickOnButton(authorizeNow);
		DynamicWait.waitUntil(2);
	}

	public void getChargeWithoutAuthorizeLink() {
		DynamicWait.waitForClick(paymentCard_WithoutCreateLink);
		clickOnButton(paymentCard_WithoutCreateLink);
		DynamicWait.waitForClick(paymentFirstCard);
		clickOnButton(paymentFirstCard);
		DynamicWait.waitForClick(chargeNow_WithoutCreateLinks);
		clickOnButton(chargeNow_WithoutCreateLinks);
		DynamicWait.waitUntil(3);
	}

	public String validatePaymentForm() {
		String status = "null";
		if (isElementPresent(emailRequired) > 0) {
			status = getTextFromElement(emailRequired);
		} else if (isElementPresent(amountRequired) > 0) {
			status = getTextFromElement(amountRequired);
		}
		return status;
	}

	public int clickOnCapture(String status, String amount, String description) {
		int row = getRowNumber(status);
		int result = 0;
		if (row > 0) {
			By moreMenu = By.xpath(moreOptionFirst + row + moreOptionSecond);
			System.out.println("More menu Locator ---------------- " + moreMenu);
			clickOnButton(moreMenu);
			By capture = By.xpath(captureFirst + row + captureSecond);
			System.out.println("Capture Locator --------- " + capture);
			if (getTextFromElement(capture).equals("Capture Amount...")) {
				DynamicWait.waitForClick(capture);
				clickOnButton(capture);
				DynamicWait.waitForClick(captureAmountTextBox);
				sendValueToTextBox(captureAmountTextBox, amount);
				sendValueToTextBox(captureDescription, description);
				clickOnButton(captureNow_Btn);
				DynamicWait.waitForClick(yesCaptureNow);
				clickOnButton(yesCaptureNow);
				result = row;
			}
		}
		return result;
	}

	public int realeasePayment(String status) {
		int row = getRowNumber(status);
		int result = 0;
		if (row > 0) {
			By moreMenu = By.xpath(moreOptionFirst + row + moreOptionSecond);
			System.out.println("More Menu Locator --------- " + moreMenu);
			clickOnButton(moreMenu);
			By release = By.xpath(releaseFirst + row + releaseSecond);
			System.out.println("Release Locator ---------- " + release);
			if (getTextFromElement(release).equals("Release")) {
				DynamicWait.waitForClick(release);
				clickOnButton(release);
				DynamicWait.waitForClick(yesDoIt);
				clickOnButton(yesDoIt);
				result = row;
			}
		}
		return result;
	}

	public void changeCurrency(String country) {
		clickOnButton(currencySelector);
		DynamicWait.waitForClick(searchCurrency);
		sendValueToTextBox(searchCurrency, country);
		DynamicWait.waitUntil(2);
		if (isElementPresent(currencyHeading) > 0) {
			String heading = getTextFromElement(currencyHeading);
			System.out.println("Currency Heading -------------- " + heading);
			if (heading.equals(Constant.ALL_CURRENCIES) || heading.equals(Constant.POPULAR_CURRENCY)) {
				DynamicWait.waitForClick(currencyFirstResult);
				clickOnButton(currencyFirstResult);
			}
		}
	}

	public String getToastMsg() {
		String msg = "null";
		DynamicWait.waitForClick(toaster);
		if (isElementPresent(toaster) > 0) {
			msg = getTextFromElement(toaster);
			System.out.println("Toaster message is --------- " + msg);
		}

		return msg;
	}

	public void clickOnMoreSetting() {
		clickOnButton(moreSetting);
	}

	public void setScheduleDate(String scheduleDate) {
		DynamicWait.waitForClick(scheduleChargeDate);
		setValueJavaScript(scheduleChargeDate, scheduleDate);
		WebDriverFactory.getDriver().findElement(scheduleChargeDate).sendKeys(Keys.ENTER);
	}

	public void setScheduleDateTime(String value) {
		// setValueJavaScript(scheduleDateSet, value);
		// clickOnButton(setButton);
		LocalDate date = LocalDate.now();
		int day = date.getDayOfMonth();
		// day = day + 1;
		LocalDateTime time = LocalDateTime.now();
		int hour = time.getHour() + 1;
		// hour = hour + 1;
		int min = time.getMinute();
		min = min + 5;
		System.out.println("Today's day ------------ " + day);
		System.out.println("Hours now --------------- " + hour);
		System.out.println("Minute now ------------ " + min);
		List<WebElement> days = WebDriverFactory.getDriver().findElements(allDate);
		for (WebElement ele : days) {
			if (ele.getText().equals(Integer.toString(day))) {
				ele.click();
				// DynamicWait.waitUntil(3);
				// clickOnButton(setButton);
				break;
			}
		}
		List<WebElement> hours = WebDriverFactory.getDriver().findElements(allHours);
		for (WebElement ele : hours) {
			if (ele.getText().equals(Integer.toString(hour))) {
				ele.click();
				break;
			}
		}

		List<WebElement> minutes = WebDriverFactory.getDriver().findElements(allMinutes);
		for (WebElement ele : minutes) {
			if (ele.getText().equals(Integer.toString(min))) {
				ele.click();
				break;
			}
		}
		clickSetDateTime();
	}

	public void setExpiryDate(String expiryDate) {
		setValueJavaScript(paymentLinkExpire, expiryDate);
		WebDriverFactory.getDriver().findElement(paymentLinkExpire).sendKeys(Keys.ENTER);
		LocalDate date = LocalDate.now();
		LocalDateTime time = LocalDateTime.now();
		int day = date.getDayOfMonth();
		day = day + 1;
		int hour = time.getHour() + 1;
		int min = time.getMinute();
		System.out.println("Day -------- " + day);
		System.out.println("Hour ------- " + hour);
		System.out.println("Minute ------ " + min);

		List<WebElement> days = WebDriverFactory.getDriver().findElements(allDate);
		for (WebElement ele : days) {
			if (ele.getText().equals(Integer.toString(day))) {
				ele.click();
				break;
			}
		}

		List<WebElement> hours = WebDriverFactory.getDriver().findElements(allHours);
		for (WebElement ele : hours) {
			if (ele.getText().equals(Integer.toString(hour))) {
				// if (ele.getText().equals("09")) {
				ele.click();
				break;
			}
		}

		List<WebElement> minutes = WebDriverFactory.getDriver().findElements(allMinutes);
		for (WebElement ele : minutes) {
			if (ele.getText().equals(Integer.toString(min))) {
				ele.click();
				break;
			}
		}
		clickOnButton(setButton2);
	}

	public String getExpiryDate() {
		String expireDate = getTextJavaSrcipt(paymentLinkExpire);
		String[] str = expireDate.split(",");
		String[] mon = str[1].split(" ");
		for (int i = 0; i < mon.length; i++) {
			System.out.println("Splitted date ----------- " + mon[i]);
		}
		String mont = mon[1];
		String d = mon[2];
		System.out.println("Date from extraction ---------- " + str[1]);
		String[] str1 = str[2].split(" ");
		for (int i = 0; i < str1.length; i++) {
			System.out.println("Splitted Date ------------ " + str1[i]);
		}

		String[] timer = str1[2].split(":");
		int firstTime = Integer.parseInt(timer[0]);
		System.out.println("First Timer is ----------- " + firstTime);
		String finalHour = null;
		String finalDates = null;
		if (firstTime < 10) {
			String hour = String.valueOf(firstTime);
			finalHour = "0" + hour;
			finalHour = finalHour.concat(":" + timer[1] + " " + str1[3]);
			// finalDates = str1[1].concat(" "+finalHour);
		}

		System.out.println("Final Hour --------- " + finalHour);
		System.out.println("Final Date ---------- " + finalDates);

		String date = d;
		System.out.println("Date -------- " + d);
		System.out.println("Mon --------- " + mont);
		date = date.concat(" " + mont + ", ");
		date = date.concat(str1[1] + " ");
		System.out.println("Created date by me ----- " + date);

		String[] dates = expireDate.split(",");
		String dateFirst = dates[1] + ",";
		dateFirst = dateFirst.concat(dates[2]);
		System.out.println("Date First ------- " + dates[1]);
		System.out.println("Date Second -------- " + dates[2]);
		String[] ddd = dates[2].split(" ");

		// date = date.concat(dates[2]);
		if (firstTime < 10) {
			// date = date.concat(ddd[2] + " " + finalHour);
			date = date.concat(finalHour);
		} else {
			date = date.concat(ddd[2] + " " + ddd[3]);
		}

		System.out.println("Corrected Expiry date after manual format ------- " + date);

		// String formatDate = sdf.format(expireDate);
		// System.out.println("Applied Format Date on Existing date ------------- " +
		// date1);

		return date;
	}

	public String getScheduleDateFromForm() {
		String scheduleDate = getTextJavaSrcipt(scheduleChargeDate);
		String[] str = scheduleDate.split(",");
		String[] mon = str[1].split(" ");
		for (int i = 0; i < mon.length; i++) {
			System.out.println("Splitted date ----------- " + mon[i]);
		}
		String mont = mon[1];
		String d = mon[2];
		System.out.println("Date from extraction ---------- " + str[1]);
		String[] str1 = str[2].split(" ");
		for (int i = 0; i < str1.length; i++) {
			System.out.println("Splitted Date ------------ " + str1[i]);
		}

		String[] timer = str1[2].split(":");
		int firstTime = Integer.parseInt(timer[0]);
		System.out.println("First Timer is ----------- " + firstTime);
		String finalHour = null;
		String finalDates = null;
		if (firstTime < 10) {
			String hour = String.valueOf(firstTime);
			finalHour = "0" + hour;
			finalHour = finalHour.concat(":" + timer[1] + " " + str1[3]);
			// finalDates = str1[1].concat(" "+finalHour);
		}

		System.out.println("Final Hour --------- " + finalHour);
		System.out.println("Final Date ---------- " + finalDates);

		String date = d;
		System.out.println("Date -------- " + d);
		System.out.println("Mon --------- " + mont);
		date = date.concat(" " + mont + ", ");
		date = date.concat(str1[1] + " ");
		System.out.println("Created date by me ----- " + date);

		String[] dates = scheduleDate.split(",");
		String dateFirst = dates[1] + ",";
		dateFirst = dateFirst.concat(dates[2]);
		System.out.println("Date First ------- " + dates[1]);
		System.out.println("Date Second -------- " + dates[2]);
		String[] ddd = dates[2].split(" ");

		// date = date.concat(dates[2]);
		if (firstTime < 10) {
			// date = date.concat(ddd[2] + " " + finalHour);
			date = date.concat(finalHour);
		} else {
			date = date.concat(ddd[2] + " " + ddd[3]);
		}

		System.out.println("Corrected Expiry date after manual format ------- " + date);
		return date;
	}

	public void clickSetDateTime() {
		clickOnButton(setButton);
	}

	public void setChargeProtection() {
		clickOnButton(chargeBackProtection);
	}

	public void setTermsCondition(String terms) {
		sendValueToTextBox(transactionTerms, terms);
	}

	public void clickOnAddNewRequestBtn() {
		DynamicWait.waitForClick(addNewRequest_Btn);
		clickOnButton(addNewRequest_Btn);
	}

	public void searchKey(String keyWord) {
		DynamicWait.waitForClick(searchBox);
		sendValueToTextBox(searchBox, keyWord);
	}

	public String getSearchResult(String keyWord) {
		DynamicWait.waitUntil(5);
		String msg = "null";
		int rowCount = isElementPresent(By.xpath(emailIdFirst + 3 + emailIdSecond));
		if (rowCount == 0) {
			msg = getTextFromElement(noResult);
		} else if (keyWord.contains("@")) {
			msg = getEmailId(3);
		} else {
			msg = getRequestId(3);
		}
		System.out.println("Msg ------------ " + msg);
		return msg;
	}

}
