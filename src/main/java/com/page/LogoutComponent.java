package com.page;

import org.openqa.selenium.WebDriver;

import com.pageLocators.LogoutLocators;
import com.utilities.DynamicWait;

public class LogoutComponent extends BasePage implements LogoutLocators{

	public LogoutComponent(WebDriver driver) {
		super(driver);
	}
	
	public void clickOnMenu() {
		DynamicWait.waitForClick(userMenu);
		clickOnButton(userMenu);
	}
	
	public void clickOnLogout() {
		DynamicWait.waitForClick(logout);
		clickOnButton(logout);
	}

}
