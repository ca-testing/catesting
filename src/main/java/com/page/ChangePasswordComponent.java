package com.page;

import org.openqa.selenium.WebDriver;

import com.pageLocators.ChangePasswordLocators;
import com.pageLocators.ForgotPasswordLocators;
import com.pageLocators.LogoutLocators;
import com.utilities.DynamicWait;

public class ChangePasswordComponent extends BasePage implements LogoutLocators, ChangePasswordLocators, ForgotPasswordLocators{

	public ChangePasswordComponent(WebDriver driver) {
		super(driver);
	}
	
	public void clickOnChangePwdMenu() {
		DynamicWait.waitForClick(userMenu);
		clickOnButton(userMenu);
		DynamicWait.waitForClick(changePassword);
		clickOnButton(changePassword);
	}
	
	public void changePassword(String oldPwd, String newPwd, String confirmPwd) {
		DynamicWait.waitForClick(oldPassword);
		sendValueToTextBox(oldPassword, oldPwd);
		DynamicWait.waitForClick(newPassword);
		sendValueToTextBox(newPassword, newPwd);
		DynamicWait.waitForClick(confirmPassword);
		sendValueToTextBox(confirmPassword, confirmPwd);
		DynamicWait.waitForClick(savePasswordBtn);
		clickOnButton(savePasswordBtn);
		DynamicWait.waitUntil(2);
	}
	
	public String validateErrorMsg() {
		String errorMsg = "null";
		if (isElementPresent(oldPwdErrorMsg)>0) {
			if (getTextFromElement(oldPwdErrorMsg).length()>1) {
				errorMsg = getTextFromElement(oldPwdErrorMsg);
			}
		}else if (isElementPresent(newPwdErrorMsg)>0) {
			if (getTextFromElement(newPwdErrorMsg).length()>1) {
				errorMsg = getTextFromElement(newPwdErrorMsg);
			}
		}
		return errorMsg;
	}
	
	public String isPasswordUpdate() {
		DynamicWait.waitForClick(toaster);
		String toastMsg = getTextFromElement(toaster);
		System.out.println("Toaster Message ------ "+ toastMsg);
		return toastMsg;
	}

}
