package com.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import com.driverFactory.WebDriverFactory;
import com.pageLocators.ForgotPasswordLocators;
import com.pageLocators.ProfileUpdateLocators;
import com.pageLocators.TeamsLocators;
import com.utilities.DynamicWait;

public class ProfileUpdateComponent extends BasePage implements ProfileUpdateLocators, ForgotPasswordLocators, TeamsLocators {

	public ProfileUpdateComponent(WebDriver driver) {
		super(driver);
	}

	public void clickOnMenu() {
		DynamicWait.waitForClick(menuDropdown);
		clickOnButton(menuDropdown);
		DynamicWait.waitUntil(2);
	}

	public void clickOnProfileUpdate() {
		if (isElementPresent(profileUpdate) > 0) {
			Actions action = new Actions(WebDriverFactory.getDriver());
			action.moveToElement(WebDriverFactory.getDriver().findElement(teamsText)).build().perform();
			clickOnButton(profileUpdate);
			DynamicWait.waitUntil(2);
		}
	}

	public void uploadUserImage() {
		DynamicWait.waitForClick(uploadProfileBtn);
		// sendValueToTextBox(uploadProfileBtn, System.getProperty("user.dir") +
		// "/uploadImage/UserIcon.png");
		// WebDriverFactory.getDriver().findElement(uploadProfile)
		// .sendKeys("D:/Shahbaz_Work/ChargeAutomation/com.dapl.chargeAutomation/uploadImage/UserIcon.png");
		WebDriverFactory.getDriver().findElement(uploadProfile)
				.sendKeys(System.getProperty("user.dir") + "/uploadImage/UserIcon.png");
		DynamicWait.waitUntil(3);
	}

	public void updateName(String userNames) {
		DynamicWait.waitForClick(userName);
		sendValueToTextBox(userName, userNames);
	}
	
	public String getEmailId() {
		String emailId = getTextFromElement(userEmail);
		System.out.println("User Email Id ------- "+emailId);
		return emailId;
	}

	public void updateConatactNumber(String phnNumber) {
		DynamicWait.waitForClick(userTelephone);
		sendValueToTextBox(userTelephone, phnNumber);
	}

	public void updateAddress(String address) {
		DynamicWait.waitForClick(userAddress);
		sendValueToTextBox(userAddress, address);
	}

	public void uploadCompanyImage() {
		//DynamicWait.waitForClick(companyLogoBtn);
		// sendValueToTextBox(companyLogoBtn, System.getProperty("user.dir") +
		// "/uploadImage/google.png");
		WebDriverFactory.getDriver().findElement(companyLogoBtn)
				.sendKeys(System.getProperty("user.dir") + "/uploadImage/google.jpg");
		DynamicWait.waitUntil(3);
	}

	public void updateCompanyName(String companyNames) {
		DynamicWait.waitForClick(companyName);
		sendValueToTextBox(companyName, companyNames);
	}

	public void updateCompanyEmail(String emails) {
		DynamicWait.waitForClick(companyEmail);
		sendValueToTextBox(companyEmail, emails);
	}

	public void updateCompanyAddress(String address) {
		DynamicWait.waitForClick(companyAddress);
		sendValueToTextBox(companyAddress, address);
	}

	public void clickOnSaveChanges() {
		DynamicWait.waitForClick(save_Changes);
		clickOnButton(save_Changes);
		DynamicWait.waitUntil(2);
	}

	public String validateUpdateForm() {
		String errorMsg = "null";
		if (isElementPresent(userTelePhoneErrorMsg) > 0) {
			if (getTextFromElement(userTelePhoneErrorMsg).length() > 0) {
				errorMsg = getTextFromElement(userTelePhoneErrorMsg);
				System.out.println("User Telephone Error Msg --------- " + errorMsg);
			}
		} else if (isElementPresent(companyNameErrorMsg) > 0) {
			if (getTextFromElement(companyNameErrorMsg).length() > 0) {
				errorMsg = getTextFromElement(companyNameErrorMsg);
				System.out.println("Company Name Error Msg ---------- " + errorMsg);
			}

		} else if (isElementPresent(companyEmailErrorMsg) > 0) {
			if (getTextFromElement(companyEmailErrorMsg).length() > 0) {
				errorMsg = getTextFromElement(companyEmailErrorMsg);
				System.out.println("Company Email error Msg --------- " + errorMsg);
			}
		}
		return errorMsg;
	}

	public void scrollToSaveChange() {
		scrollToElement(save_Changes);
	}
	
	public String getToasterMsg() {
		String msg = null;
		//DynamicWait.waitForClick(toaster);
		if (isElementPresent(toaster)>0) {
			msg = getTextFromElement(toaster);
		}
		return msg;
	}
}
