package com.page;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.driverFactory.WebDriverFactory;
import com.pageLocators.MailSacLocators;
import com.utilities.DynamicWait;

public class VerifyEmailMailsacComponent extends BasePage implements MailSacLocators {

	public VerifyEmailMailsacComponent(WebDriver driver) {
		super(driver);
	}

	public boolean isEmailRecieved(String emailId, String emailHeading) {
		boolean status = false;

		List<WebElement> heading = WebDriverFactory.getDriver().findElements(mailHeading);
		for (int i = 0; i < heading.size(); i++) {
			String headingText = getTextFromElement(mailHeading);
			if (headingText.equalsIgnoreCase(heading.get(i).getText())) {
				status = true;
				break;
			}
		}

		return status;
	}

	public void clickOnVerifyNowLink(String mailHeadingTexts) {
		List<WebElement> heading = WebDriverFactory.getDriver().findElements(mailHeading);
		for (int i = 0; i < heading.size(); i++) {
			String headingText = getTextFromElement(mailHeading);
			if (headingText.equalsIgnoreCase(heading.get(i).getText())) {
				By mailHeadings = By.xpath(allmailHeading + "[text()='" + mailHeadingTexts + "']");
				clickOnButton(mailHeadings);
				break;
			}
		}
	}

	public void loginToMailsac(String emailId, String pwd) {
		DynamicWait.waitForClick(emailIdforLogin);
		sendValueToTextBox(emailIdforLogin, emailId);
		sendValueToTextBox(pwdforLogin, pwd);
		clickOnButton(signInBtn);
	}

	public void clickOnResetNow() {
		DynamicWait.waitForClick(resetNowBtn);
		clickOnButton(resetNowBtn);
	}

	public void openMail(String subject) {
		// String navigateURL = null;
		List<WebElement> heading = WebDriverFactory.getDriver().findElements(mailHeading);
		for (int i = 0; i < heading.size(); i++) {
			if (subject.equalsIgnoreCase(heading.get(i).getText())) {
				// navigateURL = WebDriverFactory.getDriver()
				// .findElement(By.xpath("//tr[@ng-repeat = 'msg in messages']/td[text() = '" +
				// subject + "']"
				// + "/preceding-sibling::td//a[contains(text(), 'Unblock links and images')]"))
				// .getAttribute("href");
				clickOnButton(unblockLinkImage);
				break;
			}
		}
		// WebDriverFactory.getDriver().navigate().to("https://" + navigateURL);
	}

	public void openEmailInBrowser(String subject) {
		String dirtyURL = null;
		List<WebElement> heading = WebDriverFactory.getDriver().findElements(mailHeading);
		for (int i = 0; i < heading.size(); i++) {
			if (heading.get(i).getText().equalsIgnoreCase(subject)) {
				DynamicWait.waitUntil(2);
				WebElement ele = WebDriverFactory.getDriver()
						.findElement(By.xpath("//tr[@ng-repeat = 'msg in messages']/td[text() = '" + subject + "']"
								+ "/preceding-sibling::td//a[contains(text(), 'Unblock links and images')]"));
				dirtyURL = ele.getAttribute("href");
				break;
			}
		}
		System.out.println("DirtyUrl ---- > " + dirtyURL);
		WebDriverFactory.getDriver().navigate().to(dirtyURL);
	}

}
