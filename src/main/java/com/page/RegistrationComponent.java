package com.page;

import org.openqa.selenium.WebDriver;

import com.pageLocators.RegistrationLocators;
import com.utilities.DynamicWait;
import com.utilities.ExtentReport;

public class RegistrationComponent extends BasePage implements RegistrationLocators {

	public RegistrationComponent(WebDriver driver) {
		super(driver);
	}

	public void clickOnRegisterBtn() {
		DynamicWait.waitForClick(registerBtn);
		clickOnButton(registerBtn);
		System.out.println("Clicked on Register button at homepage");
	}

	public void enterFullName(String fullNames) {
		DynamicWait.waitForClick(signUpHeading);
		DynamicWait.waitForClick(fullName);
		sendValueToTextBox(fullName, fullNames);
	}

	public void enterCompanyName(String companyNames) {
		DynamicWait.waitForClick(companyName);
		sendValueToTextBox(companyName, companyNames);
	}

	public void enterPhoneNumber(String phoneNumber) {
		DynamicWait.waitForClick(telephone);
		sendValueToTextBox(telephone, phoneNumber);
	}

	public void enterEmailId(String emails) {
		DynamicWait.waitForClick(email);
		sendValueToTextBox(email, emails);
	}

	public void enterPassword(String pwd) {
		DynamicWait.waitForClick(password);
		sendValueToTextBox(password, pwd);
	}

	public void enterConfirmPWD(String confPWD) {
		DynamicWait.waitForClick(confirmPassword);
		sendValueToTextBox(confirmPassword, confPWD);
	}

	public void enterPMS(String pms) {
		DynamicWait.waitForClick(PMStextBox);
		sendValueToTextBox(PMStextBox, pms);
	}

	public void checkedAgree() {
		scrollToElement(agree);
		// DynamicWait.waitForClick(agree);
		checkedCheckBox(checkBox);
	}

	public void submitForm() {
		DynamicWait.waitForClick(getStartBtn);
		clickOnButton(getStartBtn);
		DynamicWait.waitUntil(5);
	}

	public String verifyEmailHeading() {
		DynamicWait.waitForClick(confirmEmailHeading);
		String confirmHeading = getTextFromElement(confirmEmailHeading);
		System.out.println("Confirm Email Heading ------ " + confirmHeading);
		return confirmHeading;
	}

	public String verifyFullName() {
		String fullName = getTextFromElement(checkFullName);
		System.out.println("Full Name at confirmation page -------- >> " + fullName);
		return fullName;
	}

	public String verifyEmail() {
		String getEmailText = getTextFromElement(confirmEmailId);
		System.out.println("Email text at confirmation page ------------ >> " + getEmailText);
		return getEmailText;
	}

	public boolean isRegisterSuccess() {
		boolean status = false;
		if (isElementPresent(fullNameErrorMsg) > 0) {
			scrollToElement(signUpHeading);
			scrollUp();
			String nameError = getTextFromElement(fullNameErrorMsg);
			System.out.println("Name Error ----- >>> " + nameError);
			ExtentReport.extentTest.warning(nameError);
			status = false;
		} else if (isElementPresent(companyTwo) > 0) {
			scrollToElement(companyTwo);
			scrollUp();
			String companyError = getTextFromElement(companyTwo);
			System.out.println("Company Error ----- >>> " + companyError);
			ExtentReport.extentTest.warning("Company name should be "+companyError);
			status = false;
		} else if (isElementPresent(telePhoneErrorMsg) > 0) {
			scrollToElement(telePhoneErrorMsg);
			String phoneError = getTextFromElement(telePhoneErrorMsg);
			System.out.println("Telephone Error ----- >>> " + phoneError);
			ExtentReport.extentTest.warning(phoneError);
			status = false;
		} else if (isElementPresent(emailErrorMsg) > 0) {
			scrollToElement(emailErrorMsg);
			String emailError = getTextFromElement(emailErrorMsg);
			System.out.println("Email Error ----- >>> " + emailError);
			ExtentReport.extentTest.warning(emailError);
			status = false;
		} else if (isElementPresent(passwordErrorMsg) > 0) {
			scrollToElement(passwordErrorMsg);
			String pwdError = getTextFromElement(passwordErrorMsg);
			System.out.println("Password Error ----- >>> " + pwdError);
			ExtentReport.extentTest.warning(pwdError);
			status = false;
		} else if (isElementPresent(confirmErrorMsg) > 0) {
			scrollToElement(confirmErrorMsg);
			String pwdError = getTextFromElement(confirmErrorMsg);
			System.out.println("Pwd Error ----- >>> " + pwdError);
			ExtentReport.extentTest.warning(pwdError);
			status = false;
		} else if (isElementPresent(termsErrorMsg) > 0) {
			scrollToElement(termsErrorMsg);
			String termsError = getTextFromElement(termsErrorMsg);
			System.out.println("Terms and Conditions Error ----- >>> " + termsError);
			ExtentReport.extentTest.warning(termsError);
			status = false;
		} else {
			DynamicWait.waitForClick(confirmEmailHeading);
			if (isElementPresent(confirmEmailHeading) > 0) {
				status = true;
			}
		}
		return status;
	}

}
