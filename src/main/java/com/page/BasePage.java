package com.page;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.utilities.DynamicWait;

public class BasePage {

	private WebDriver driver;

	public BasePage(WebDriver driver) {
		this.driver = driver;
	}

	public String getPageTitle() {
		return driver.getTitle();
	}

	public void sendValueToTextBox(By locator, String value) {
		System.out.println("Element Got -------------- > " + locator + " Value to send ------------- " + value);
		DynamicWait.waitForClick(locator);
		clearTextBox(locator);
		driver.findElement(locator).sendKeys(value);
		System.out.println("Value sent to the textbox");
	}

	private void clearTextBox(By locator) {
		driver.findElement(locator).click();
		driver.findElement(locator).clear();
	}

	public void clickOnButton(By locator) {
		DynamicWait.waitForClick(locator);
		driver.findElement(locator).click();
		System.out.println("Clicked on Button-------");
	}

	public void checkedCheckBox(By locator) {
		driver.findElement(locator).click();
		System.out.println("Checked the checbox.......!!");
	}

	public int isElementPresent(By locator) {
		// DynamicWait.waitForClick(locator);
		List<WebElement> elements = driver.findElements(locator);
		int size = elements.size();
		System.out.println("Element size is ---- " + size);
		return size;
	}

	public String getTextFromElement(By locator) {
		// DynamicWait.waitForClick(locator);
		String text = driver.findElement(locator).getText();
		System.out.println("Text from the Element ------ " + text);
		return text;
	}

	public void scrollToElement(By locator) {
		WebElement element = driver.findElement(locator);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", element);
	}

	public void scrollToWebElement(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", element);
	}

	public void scrollUp() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,-350)", "");
	}

	public String getTextJavaSrcipt(By locator) {
		WebElement element = driver.findElement(locator);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		String text = (String) js.executeScript("return arguments[0].value", element);
		return text;
	}

	public void setValueJavaScript(By locator, String value) {
		WebElement element = driver.findElement(locator);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].value='" + value + "';", element);
	}

	public void clickOnButtonJS(By locator) {
		WebElement element = driver.findElement(locator);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", element);
	}

	public String getTodayDate() {
		Date curDate = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");

		String DateToStr = format.format(curDate);
		format = new SimpleDateFormat("dd MMM, yyyy");
		DateToStr = format.format(curDate);
		System.out.println(DateToStr);
		return DateToStr;
	}

	public String getAttributeValue(By locator, String attribute) {
		String value = driver.findElement(locator).getAttribute(attribute);
		System.out.println("Attribute value ----------- " + value);
		return value;
	}

	public String getAttributeValueWebElement(WebElement element, String attribute) {
		String value = element.getAttribute(attribute);
		System.out.println("Attribute value ----------- " + value);
		return value;
	}

}
