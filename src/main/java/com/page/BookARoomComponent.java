package com.page;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.awt.AWTException;
import java.awt.datatransfer.StringSelection;
import java.awt.Toolkit;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.driverFactory.WebDriverFactory;
import com.github.javafaker.Faker;
import com.pageLocators.BookARoomLocators;
import com.utilities.Constant;
import com.utilities.DynamicWait;

public class BookARoomComponent extends BasePage implements BookARoomLocators {

	String bookingIdAfterRoomBooking = null;
	ArrayList<String> tabs;

	public BookARoomComponent(WebDriver driver) {
		super(driver);
	}

	public void clickOnCreateTestBooking() {
		int count = isElementPresent(createTestBooking);
		System.out.println("Total count of all menu -------------- " + count);
		do {
			String createTestText = null;
			List<WebElement> menu = WebDriverFactory.getDriver().findElements(createTestBooking);
			for (WebElement ele : menu) {
				scrollToWebElement(ele);
				createTestText = ele.getText();
				if (createTestText.equals("Create Test Booking")) {
					DynamicWait.waitForClickWebElement(ele);
					ele.click();
					break;
				}
			}
			if (createTestText.equals("Create Test Booking")) {
				break;
			}
		} while (true);
	}

	public String getBookingTitle() {
		String title = getTextFromElement(bookingHeading);
		System.out.println("Heading of the Module ------------ " + title);
		return title;
	}

	public void chooseBookingType(String bookingType) {
		DynamicWait.waitUntil(3);
		if (bookingType.equals(Constant.SINGLE_BOOKING)) {
			DynamicWait.waitForClick(singleBooking_radio);
			clickOnButton(singleBooking_radio);
		} else if (bookingType.equals(Constant.GROUP_BOOKING)) {
			DynamicWait.waitForClick(groupBooking_radio);
			clickOnButton(groupBooking_radio);
		}
	}

	public void chooseTestAccount(String testAccount) {
		DynamicWait.waitUntil(3);
		Select select = new Select(WebDriverFactory.getDriver().findElement(testAccount_dropdown));
		select.selectByVisibleText(testAccount);
	}

	public void chooseProperty(String property) {
		Select select = new Select(WebDriverFactory.getDriver().findElement(selectProperty_dropdown));
		DynamicWait.waitUntil(3);
		select.selectByVisibleText(property);
	}

	public void chooseRooms(String roomType) {
		Select select = new Select(WebDriverFactory.getDriver().findElement(selectRoom_dropdown));
		DynamicWait.waitUntil(3);
		select.selectByVisibleText(roomType);
	}

	public void chooseBookingSource(String sourceBooking) {
		Select select = new Select(WebDriverFactory.getDriver().findElement(select_bookingSource_dropdown));
		select.selectByVisibleText(sourceBooking);
	}

	public void chooseCardType(String cardType) {
		Select select = new Select(WebDriverFactory.getDriver().findElement(cardType_dropdown));
		select.selectByVisibleText(cardType);
	}

	public void chooseCheckinDate() {
		clickOnButton(checkin_date);

		LocalDate date = LocalDate.now();
		int day = date.getDayOfMonth();
		if (day < 30) {
			day = day + 1;
		}
		System.out.println("Day ----------- " + day);

		List<WebElement> allDates = WebDriverFactory.getDriver().findElements(eachDate_Value);
		for (WebElement ele : allDates) {
			if (ele.getText().equals(Integer.toString(day))) {
				ele.click();
				break;
			}
		}
	}

	public String getTodaysDate() {
		SimpleDateFormat formatter = new SimpleDateFormat("MMM dd yyyy");
		Date date = new Date();
		String todays = formatter.format(date);
		System.out.println("Today's date in required format is ------------- " + todays);
		return todays;
	}

	public String todaysDateForCheckin() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDateTime now = LocalDateTime.now();
		System.out.println(dtf.format(now));
		String today = dtf.format(now);
		return today;
	}

	public void setcheckinDateForBooking() {
		DynamicWait.waitForClick(checkin_date);
		String today = todaysDateForCheckin();
		sendValueToTextBox(checkin_date, today);
	}

	public void checkoutDateFromListing() {
		Robot robot;
		try {
			robot = new Robot();
			robot.setAutoDelay(300);
			robot.keyPress(KeyEvent.VK_RIGHT);
			robot.keyRelease(KeyEvent.VK_RIGHT);
			robot.keyPress(KeyEvent.VK_RIGHT);
			robot.keyRelease(KeyEvent.VK_RIGHT);

			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	public void setPrice(String price) {
		sendValueToTextBox(totalPrice, price);
	}

	public void enterGuestEmail(String email) {
		sendValueToTextBox(guestEmail, email);
	}

	public void enterGuestComment(String comment) {
		sendValueToTextBox(guestComment, comment);
	}

	public void selectGoodCard(String goodCards) {
		Select select = new Select(WebDriverFactory.getDriver().findElement(goodCard));
		select.selectByVisibleText(goodCards);
	}

	public void clickOnSaveBtn() {
		DynamicWait.waitForClick(save_Btn);
		clickOnButton(save_Btn);
	}

	public String getBookingId() {
		DynamicWait.waitForClick(bookingIdAfterBooking);
		bookingIdAfterRoomBooking = getTextFromElement(bookingIdAfterBooking);
		System.out.println("Response Booking Id ----------- " + bookingIdAfterRoomBooking);
		DynamicWait.waitForClick(close_btn_popup);
		clickOnButton(close_btn_popup);
		return bookingIdAfterRoomBooking;
	}

	public void doLogout() {
		DynamicWait.waitForClick(menuForLogout);
		clickOnButton(menuForLogout);
		DynamicWait.waitForClick(logout_Btn);
		clickOnButton(logout_Btn);
	}

	public void clickOnBookingFromMenuLink() {
		DynamicWait.waitForClick(bookingLink);
		clickOnButton(bookingLink);
	}

	public String isBookingIdMatched() {
		String roomBookingId = "null";
		int i = 0;
		/*
		 * DynamicWait.waitUntil(120);
		 * WebDriverFactory.getDriver().navigate().refresh();
		 * DynamicWait.waitForClick(bookingId); List<WebElement> allId =
		 * WebDriverFactory.getDriver().findElements(bookingId); for (WebElement ele :
		 * allId) { System.out.println("Text from the locator ------------- " +
		 * ele.getText()); if (ele.getText().equals(bookingIdAfterRoomBooking)) {
		 * roomBookingId = ele.getText(); break; } }
		 */
		do {
			DynamicWait.waitForClick(bookingId);
			List<WebElement> allIds = WebDriverFactory.getDriver().findElements(bookingId);
			for (WebElement ele : allIds) {
				System.out.println("Text from the locator ------------ " + ele.getText());
				if (ele.getText().equals(bookingIdAfterRoomBooking)) {
					roomBookingId = ele.getText();
					break;
				}
			}
			if (roomBookingId.equals("null")) {
				DynamicWait.waitUntil(20);
				WebDriverFactory.getDriver().navigate().refresh();
				i = i + 1;
				System.out.println("Counter Value ------------- " + i);
			}
			if (roomBookingId.equals(bookingIdAfterRoomBooking) || i == 15) {
				break;
			}
		} while (true);
		System.out.println("Room Booking Id ------------ " + roomBookingId);
		return roomBookingId;
	}

	public String checkSteps() {
		String stepName = "null";
		int i = 0;
		ArrayList<String> stepNames = new ArrayList<String>();
		DynamicWait.waitForClick(formStepsCard);
		List<WebElement> steps = WebDriverFactory.getDriver().findElements(formStepsCard);
		for (WebElement element : steps) {
			i = i + 1;
			String value = getAttributeValueWebElement(element, "class");
			System.out.println("Attribute Value ----------- " + value);
			if (value.contains("active")) {
				stepName = getTextFromElement(By.xpath(stepName_Card_first + i + stepName_Card_second));
				stepNames.add(stepName);
				break;
			}
		}
		System.out.println("Active Step Name is ------- " + stepName);
		return stepName;
	}

	public int total_Step() {
		DynamicWait.waitUntil(10);
		int cards = isElementPresent(formStepsCard);
		System.out.println("Total Card count ----------- " + cards);
		return cards;
	}

	public String getIdValue() {
		String roomBookingId = null;
		DynamicWait.waitForClick(cards);
		List<WebElement> allId = WebDriverFactory.getDriver().findElements(cards);
		for (WebElement ele : allId) {
			roomBookingId = getAttributeValueWebElement(ele, "id");
			break;
		}
		System.out.println("Id from attribute Value ---------- " + roomBookingId);
		roomBookingId = roomBookingId.replaceAll("[^\\d]", "");
		System.out.println("Only Digit ID --------- " + roomBookingId);
		return roomBookingId;
	}

	public String getIntegerFromString(String str) {
		String res = str.replaceAll("[^\\d]", "");
		System.out.println("Only Digit from String ---------- " + res);
		return res;
	}

	public String getNumberofCreditCardUploaded(String id) {
		String creditCardLocator = creditCardNumber_first + id + creditCardNumber_second;
		System.out.println("Credit card locator ---------- " + creditCardLocator);
		String creditCard = getTextFromElement(By.xpath(creditCardLocator));
		System.out.println("Value of the Credit card --------- " + creditCard);
		String onlyValue = getIntegerFromString(creditCard);
		System.out.println("Only Value after Extraction --------- " + onlyValue);
		return onlyValue;
	}

	public String getNumberOfDocumentUploaded(String id) {
		String documentLocator = creditCardNumber_first + id + documentNumber_second;
		System.out.println("Document Locator -------------- " + documentLocator);
		String document = getTextFromElement(By.xpath(documentLocator));
		System.out.println("Value of the document ------------- " + document);
		String onlyValue = getIntegerFromString(document);
		System.out.println("Only digit value after extraction ---------- " + onlyValue);
		return onlyValue;
	}

	public String getnumberofUpsell(String id) {
		String upsellLocator = creditCardNumber_first + id + upsellNumber_second;
		System.out.println("Upsell Locator ------------- " + upsellLocator);
		String upsellValue = getTextFromElement(By.xpath(upsellLocator));
		System.out.println("Upsell value ----------- " + upsellValue);
		String onlyValue = getIntegerFromString(upsellValue);
		System.out.println("Only digit value after extraction ----------- " + onlyValue);
		return onlyValue;
	}

	public void clickOnMenu(String cardId) {
		String menuXpath = cardMenuFirst + cardId + cardMenuSecond;
		System.out.println("Card Menu Locator ----------- " + menuXpath);
		DynamicWait.waitForClick(By.xpath(menuXpath));
		clickOnButton(By.xpath(menuXpath));
	}

	public void clickOnShareLink(String cardId) {
		String shareLinkXpath = shareLinkFirst + cardId + shareLinkSecond;
		System.out.println("Share Link Locator -------------- " + shareLinkXpath);
		DynamicWait.waitForClick(By.xpath(shareLinkXpath));
		clickOnButton(By.xpath(shareLinkXpath));
	}

	public String getLinkFromPopup() {
		DynamicWait.waitForClick(link);
		String linkValue = getTextJavaSrcipt(link);
		System.out.println("Link from share link popup ----------- " + linkValue);
		return linkValue;
	}

	public void clickOnSend_Pre_Check_in_Email(String cardId) {
		String sendPrecheckinLink = shareLinkFirst + cardId + Send_Pre_Check_in_Email_Second;
		System.out.println("Send Pre-checkin link Locator ------------ " + sendPrecheckinLink);
		DynamicWait.waitForClick(By.xpath(sendPrecheckinLink));
		clickOnButton(By.xpath(sendPrecheckinLink));
	}

	public void clickOnRe_Sync(String cardId) {
		String reSyncLink = shareLinkFirst + cardId + re_Sync_Second;
		System.out.println("Re Sync Locator ------------ " + reSyncLink);
		DynamicWait.waitForClick(By.xpath(reSyncLink));
		clickOnButton(By.xpath(reSyncLink));
	}

	public void clickOnPre_Check_in_Link(String cardId) {
		String preCheckinLink = shareLinkFirst + cardId + pre_checkin_link_Second;
		System.out.println("Pre checkin Link Locator --------------- " + preCheckinLink);
		DynamicWait.waitForClick(By.xpath(preCheckinLink));
		clickOnButton(By.xpath(preCheckinLink));
		tabs = new ArrayList<String>(WebDriverFactory.getDriver().getWindowHandles());
		WebDriverFactory.getDriver().switchTo().window(tabs.get(1));
	}

	public void openBlankTab(String link) {
		try {
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_T);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_T);

			// Switch focus to new tab
			tabs = new ArrayList<String>(WebDriverFactory.getDriver().getWindowHandles());
			WebDriverFactory.getDriver().switchTo().window(tabs.get(1));

			// Launch URL in the new tab
			WebDriverFactory.getDriver().get(link);
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	public void clickOngetStarted() {
		DynamicWait.waitForClick(get_Started_Btn);
		DynamicWait.waitUntil(3);
		clickOnButton(get_Started_Btn);
		DynamicWait.waitUntil(10);
	}

	public void enterGuestEmailId(String emailId) {
		DynamicWait.waitForClick(guestEmail_At_BasicInfo);
		sendValueToTextBox(guestEmail_At_BasicInfo, emailId);
	}

	public void enterNumberOfAdult(String numberOfAdult) {
		sendValueToTextBox(guest_Adult_Count, numberOfAdult);
	}

	public void enterNumberOfChildren(String numberOfChild) {
		sendValueToTextBox(guest_Children_Count, numberOfChild);
	}

	public void clickOnSaveContinueBtn() {
		DynamicWait.waitForClick(save_Continue_Btn);
		clickOnButton(save_Continue_Btn);
	}

	public String getSummaryHeading() {
		DynamicWait.waitUntil(7);
		String head = "null";
		if (isElementPresent(your_Summary) > 0) {
			head = getTextFromElement(your_Summary);
			System.out.println("Summary Heading -------- " + head);
		}

		return head;
	}

	public String getPaymentSummaryHeading() {
		DynamicWait.waitUntil(5);
		String head = "null";
		if (isElementPresent(payment_Summary) > 0) {
			head = getTextFromElement(payment_Summary);
			System.out.println("Payment Summary Heading -------------- " + head);
		}
		return head;
	}

	public void changeToLiveMode(String mode) {
		if (mode.equals("Live Mode")) {
			DynamicWait.waitForClick(mode_toggle_btn);
			String btn_attr = getAttributeValue(view_mode_btn_attr, "class");
			System.out.println("Mode Button Attribute --------- " + btn_attr);
			if (btn_attr.equals("active")) {
				DynamicWait.waitForClick(live_Mode_Btn);
				clickOnButton(live_Mode_Btn);
				DynamicWait.waitForClick(confirm_Btn);
				clickOnButton(confirm_Btn);
			}
		} else if (mode.equals("View Mode")) {
			DynamicWait.waitForClick(mode_toggle_btn);
			String btn_attr = getAttributeValue(live_mode_btn_attr, "class");
			System.out.println("Mode Button Attribute --------------- " + btn_attr);
			if (btn_attr.equals("active")) {
				DynamicWait.waitForClick(view_Mode_Btn);
				clickOnButton(view_Mode_Btn);
				DynamicWait.waitForClick(confirm_Btn);
				clickOnButton(confirm_Btn);
			}
		}
	}

	public String getArrivingHeading() {
		DynamicWait.waitUntil(5);
		int count = isElementPresent(arrival_info_heading);
		String head = "null";
		if (count > 0) {
			head = getTextFromElement(arrival_info_heading);
			System.out.println("Heading of the form -------------- " + head);
		}
		return head;
	}

	public int isCredit_CardTextBoxPresent() {
		int count = 0;
		DynamicWait.waitUntil(5);
		int frameCount = isElementPresent(credit_Card_Frame);
		if (frameCount > 0) {
			DynamicWait.waitForSwitchFrame(credit_Card_Frame);
			count = isElementPresent(credit_Card_Number);
			System.out.println("Credit Card Textbox Count ----------- " + count);
		}
		return count;
	}

	public void setCredit_Card(String cardNumber, String expiryDate, String cvv, String postal) {
		DynamicWait.waitForClick(credit_Card_Number);
		sendValueToTextBox(credit_Card_Number, cardNumber);
		DynamicWait.waitForClick(credit_Card_ExpiryDate);
		sendValueToTextBox(credit_Card_ExpiryDate, expiryDate);
		sendValueToTextBox(credit_Card_CVV, cvv);
		sendValueToTextBox(credit_Card_Postal, postal);
	}

	public void updateCreditCard() {
		DynamicWait.waitForClick(guestCreditCardUpdate_Button);
		clickOnButton(guestCreditCardUpdate_Button);
		DynamicWait.waitForClick(yesUpdate_button);
		clickOnButton(yesUpdate_button);
		DynamicWait.waitUntil(5);
	}

	public String getAddOnServiceHeading() {
		DynamicWait.waitUntil(15);
		String head = "null";
		if (isElementPresent(addOnService_heading) > 0) {
			head = getTextFromElement(addOnService_heading);
			System.out.println("Add on Service Heading ------------- " + head);
		}
		return head;
	}

	public void selectArrivingBy(String arrivingBy) {
		DynamicWait.waitForClick(arrivingBy_dropdown);
		Select select = new Select(WebDriverFactory.getDriver().findElement(arrivingBy_dropdown));
		select.selectByVisibleText(arrivingBy);
	}

	public void setFlightNumber(String flightNumbers) {
		DynamicWait.waitForClick(flightNumber);
		sendValueToTextBox(flightNumber, flightNumbers);
	}

	public void setOtherDetails(String otherDetails) {
		DynamicWait.waitForClick(arriving_Other);
		sendValueToTextBox(arriving_Other, otherDetails);
	}

	public void selectExpectedArrivalTime(String arrivalTime) {
		DynamicWait.waitForClick(estimated_ArrivalTime_dropdown);
		Select select = new Select(WebDriverFactory.getDriver().findElement(estimated_ArrivalTime_dropdown));
		select.selectByVisibleText(arrivalTime);
	}

	public String getCreditCardHeading() {
		DynamicWait.waitUntil(5);
		String head = "null";
		if (isElementPresent(uploadCardHeading) > 0) {
			head = getTextFromElement(uploadCardHeading);
			System.out.println("Upload Credit Card Heading ------------ " + head);
		}
		return head;
	}

	public void uploadCreditCard() {
		DynamicWait.waitForClick(creditCard_upload);
		/*
		 * WebDriverFactory.getDriver().findElement(creditCard_upload)
		 * .sendKeys(System.getProperty("user.dir") + "/uploadImage/UserIcon.png");
		 **/

		String imgPath = System.getProperty("user.dir") + "\\uploadImage\\UserIcon.png";
		System.out.println("Image Path --------- " + imgPath);

		clickOnButton(creditCard_upload);

		try {
			Robot robot = new Robot();

			// StringSelection str = new
			// StringSelection("C:\\Users\\Kamal\\Desktop\\UserIcon.png");
			StringSelection str = new StringSelection(imgPath);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str, null);

			robot.delay(5000);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);

			// release Contol+V for pasting
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_V);

			robot.delay(4000);
			// for pressing and releasing Enter
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (AWTException e) {
			e.printStackTrace();
		}

		/*
		 * JavascriptExecutor jse = (JavascriptExecutor) WebDriverFactory.getDriver();
		 * jse.executeScript("return arguments[0].value = arguments[1];",
		 * WebDriverFactory.getDriver().findElement(creditCard_upload), imgPath);
		 */

		DynamicWait.waitUntil(10);
		System.out.println("Image uploaded......");
	}

	public String getSelfieHeading() {
		DynamicWait.waitUntil(5);
		String head = "null";
		if (isElementPresent(selfie_heading) > 0) {
			head = getTextFromElement(selfie_heading);
			System.out.println("Take Selfie Heading -------- " + head);
		}
		return head;
	}

	public void checkedBreakFast() {
		scrollToElement(lunch_for_Guest);
		clickOnButtonJS(breakFast_Checkbox);
	}

	public void checkedCarParking() {
		scrollToElement(breakFast_for_Guest);
		clickOnButtonJS(car_Parking_Checkbox);
	}

	public void checkedBikeTour() {
		scrollToElement(guest_car_Parking_input);
		clickOnButtonJS(bike_tour_Checkbox);
	}

	public void checkedContinental() {
		scrollToElement(guest_bike_Parking_input);
		clickOnButtonJS(continental_Checkbox);
	}

	public void enterNumberOfGuestForLunch(String numberOfGuest) {
		DynamicWait.waitForClick(lunch_for_Guest);
		sendValueToTextBox(lunch_for_Guest, numberOfGuest);
	}

	public void enterNumberOfGuestForBreakFast(String numberOfGuest) {
		// clickOnButton(breakFast_Checkbox);
		scrollToElement(lunch_for_Guest);
		sendValueToTextBox(breakFast_for_Guest, numberOfGuest);
	}

	public void enterNumberOfCarParking(String numberOfGuest) {
		// clickOnButton(car_Parking_Checkbox);
		scrollToElement(breakFast_for_Guest);
		sendValueToTextBox(guest_car_Parking_input, numberOfGuest);
	}

	public void enterNumberOfBikeTour(String numberOfGuest) {
		// clickOnButton(bike_tour_Checkbox);
		scrollToElement(guest_car_Parking_input);
		sendValueToTextBox(guest_bike_Parking_input, numberOfGuest);
	}

	public void checkEarlyCheckIn() {
		clickOnButtonJS(early_checkin_Checkbox);
	}

	public void enterNumberOfGuestForContinental(String numberOfGuest) {
		// clickOnButton(continental_Checkbox);
		scrollToElement(early_checkin_Checkbox);
		sendValueToTextBox(continental_breakFast_input, numberOfGuest);
	}

	public void clickOnPay_BalanceContinue() {
		try {
			scrollToElement(pay_balance_Btn);
			DynamicWait.waitForClick(pay_balance_Btn);
			clickOnButton(pay_balance_Btn);
		} catch (Exception ex) {
			scrollToElement(pay_balance_Btn_second);
			DynamicWait.waitForClick(pay_balance_Btn_second);
			clickOnButton(pay_balance_Btn_second);
		}
	}

	public void clickOnTermsCondition() {
		DynamicWait.waitUntil(5);
		scrollToElement(terms_Condition_CheckBox);
		clickOnButtonJS(terms_Condition_CheckBox);
	}

	public void drawYourSign() {
		changeToLiveMode("Live Mode");
		scrollToElement(draw_sign);
		DynamicWait.waitUntil(3);
		Actions actions = new Actions(WebDriverFactory.getDriver());
		actions.clickAndHold(WebDriverFactory.getDriver().findElement(draw_sign))
				.moveToElement(WebDriverFactory.getDriver().findElement(draw_sign), 50, 80).build().perform();
		DynamicWait.waitUntil(2);
	}

	public boolean isPrecheckinComplete(String id) {
		boolean status = false;
		WebDriverFactory.getDriver().switchTo().window(tabs.get(0));
		WebDriverFactory.getDriver().navigate().refresh();
		String preCheckinLocator = pre_checkin_first_status + id + pre_checkin_second_status;
		DynamicWait.waitUntil(10);
		DynamicWait.waitForClick(By.xpath(preCheckinLocator));
		String precheckin = getTextFromElement(By.xpath(preCheckinLocator));
		if (precheckin.equals("Pre-checkin completed")) {
			status = true;
		}
		return status;
	}

	public String getEmailSuccessMsg() {
		DynamicWait.waitForClick(toaster);
		String msg = getTextFromElement(toaster);
		System.out.println("Toaster Msg ------------- " + msg);
		return msg;
	}

	public String getFirstNameOfGuest() {
		String firstName = getTextJavaSrcipt(guestFirstName);
		System.out.println("Guest First Name -------------- " + firstName);
		return firstName;
	}

	public String getLastNameOfGuest() {
		String lastName = getTextJavaSrcipt(guestLastName);
		System.out.println("Guest Last Name -------------- " + lastName);
		return lastName;
	}

	public String getGuestNameAtBookingListPage(String id) {
		String fullNameLocator = guestName_first + id + guestName_second;
		System.out.println("Full Name Locator ----------- " + fullNameLocator);
		String fullName = getTextFromElement(By.xpath(fullNameLocator));
		System.out.println("Guest Full Name -------- " + fullName);
		return fullName;
	}

	public void openCard(String id) {
		String openCardLocator = openCard_first + id + openCard_second;
		System.out.println("Open Card Locator ------------ " + openCardLocator);
		clickOnButton(By.xpath(openCardLocator));
	}

	public String guestEmailAtBookingListPage(String id) {
		String guestEmailLocator = emailAtBookingList_first + id + emailAtBookingList_second;
		System.out.println("Guest Email Locator ----------------- " + guestEmailLocator);
		DynamicWait.waitForClick(By.xpath(guestEmailLocator));
		String email = getTextFromElement(By.xpath(guestEmailLocator));
		System.out.println("Email of the Guest ----------- " + email);
		return email;
	}

	public String numberOfAdultGuestAtBookingList(String id) {
		String adultLocator = emailAtBookingList_first + id + adultsAtBookingList_second;
		System.out.println("Adult Locator --------------- " + adultLocator);
		String adult = getTextFromElement(By.xpath(adultLocator));
		System.out.println("Number of adults --------------- " + adult);
		return adult;
	}

	public String numberOfChildGuestAtBookingList(String id) {
		String childLocator = emailAtBookingList_first + id + childAtBookingList_second;
		System.out.println("Child Locator ------------- " + childLocator);
		String child = getTextFromElement(By.xpath(childLocator));
		System.out.println("Number of child ------------ " + child);
		return child;
	}

	public String paymentTypeAtBookingList(String id) {
		String paymentLocator = emailAtBookingList_first + id + paymentTypeAtBookingList_second;
		System.out.println("Payment Type at booking List -------------- " + paymentLocator);
		String paymentType = getTextFromElement(By.xpath(paymentLocator));
		System.out.println("Payment Type -------------- " + paymentType);
		return paymentType;
	}

	public void clickOnGuestPortal(String id) {
		String guestPortalLocator = guest_Portal_Link_First + id + guest_Portal_Link_second;
		System.out.println("Guest Portal Locator ------------ " + guestPortalLocator);
		DynamicWait.waitForClick(By.xpath(guestPortalLocator));
		clickOnButton(By.xpath(guestPortalLocator));
		tabs = new ArrayList<String>(WebDriverFactory.getDriver().getWindowHandles());
		WebDriverFactory.getDriver().switchTo().window(tabs.get(2));
	}

	public void clickOnViewDetails(String id) {
		String guestPortalLocator = guest_Portal_Link_First + id + view_detail_Link_second;
		System.out.println("Guest Portal Locator ------------ " + guestPortalLocator);
		DynamicWait.waitForClick(By.xpath(guestPortalLocator));
		clickOnButton(By.xpath(guestPortalLocator));
		tabs = new ArrayList<String>(WebDriverFactory.getDriver().getWindowHandles());
		WebDriverFactory.getDriver().switchTo().window(tabs.get(2));
	}

	public void clickOnBookingInformation() {
		DynamicWait.waitForClick(booking_information_guest_Portal);
		// scrollToElement(booking_information_guest_Portal);
		clickOnButton(booking_information_guest_Portal);
	}

	public void clickOnEditButton() {
		DynamicWait.waitForClick(edit_button);
		clickOnButton(edit_button);
	}

	public void editEmail(String email) {
		DynamicWait.waitForClick(edit_Email_booking_information);
		sendValueToTextBox(edit_Email_booking_information, email);
	}

	public void changeArrivingBy(String arrivingBy, String flightNumber) {
		DynamicWait.waitForClick(edit_ArrivingBy_booking_information);
		Select select = new Select(WebDriverFactory.getDriver().findElement(edit_ArrivingBy_booking_information));
		select.selectByVisibleText(arrivingBy);
		sendValueToTextBox(edit_fligh_number_booking_information, flightNumber);
	}

	public void changeArrivingBy(String arrivingBy) {
		DynamicWait.waitForClick(edit_ArrivingBy_booking_information);
		Select select = new Select(WebDriverFactory.getDriver().findElement(edit_ArrivingBy_booking_information));
		select.selectByVisibleText(arrivingBy);
	}

	public void changeEstimatedTime(String time) {
		DynamicWait.waitForClick(edit_estimated_time_booking_information);
		Select select = new Select(WebDriverFactory.getDriver().findElement(edit_estimated_time_booking_information));
		select.selectByVisibleText(time);
	}

	public void clickOnUpdateAfterEdit() {
		DynamicWait.waitForClick(update_button_booking_information);
		clickOnButton(update_button_booking_information);
	}

	public void clickOnEditPaymentDetails() {
		scrollToElement(booking_information_guest_Portal);
		clickOnButton(payment_details_Tab);
		DynamicWait.waitForClick(credit_card_Edit);
		clickOnButton(credit_card_Edit);
		DynamicWait.waitUntil(2);
		scrollToElement(payment_details_Tab);
	}

	public void clickOnSaveBtn_AfterEditCreditCard() {
		DynamicWait.waitForClick(save_button_payment_details);
		clickOnButton(save_button_payment_details);
	}

	public String verifyDocumentUploaded() {
		// DynamicWait.waitUntil(8);
		DynamicWait.waitForClick(document_Upload_Tab);
		scrollToElement(payment_details_Tab);
		clickOnButton(document_Upload_Tab);
		DynamicWait.waitForClick(credit_card_Submitted);
		String text = getTextFromElement(credit_card_Submitted);
		System.out.println("Credit card submitted Text ------------ " + text);
		return text;
	}

	public int verifySignGiven() {
		DynamicWait.waitUntil(8);
		DynamicWait.waitForClick(digital_sign_Tab);
		scrollToElement(document_Upload_Tab);
		clickOnButton(digital_sign_Tab);
		DynamicWait.waitForClick(guest_sign_outlet);
		int sign = isElementPresent(guest_sign_outlet);
		System.out.println("Sign Count --------- " + sign);
		return sign;
	}

	public boolean verifyaddOnService() {
		boolean status = false;
		DynamicWait.waitUntil(8);
		DynamicWait.waitForClick(upsell_Purchased_Tab);
		scrollToElement(digital_sign_Tab);
		clickOnButton(upsell_Purchased_Tab);
		DynamicWait.waitForClick(purchased_Add_on_Services_Heading);
		String heading = getTextFromElement(purchased_Add_on_Services_Heading);
		System.out.println("Purchased Add on heading -------------- " + heading);
		Assert.assertTrue(heading.contains("Purchased Add-on Services"));
		if (heading.contains("Purchased Add-on Services")) {
			String upselCount = getTextFromElement(service_count);
			System.out.println("Upsell Count  -------------- " + upselCount);
			Assert.assertTrue(upselCount.contains("6"));
			if (upselCount.contains("6")) {
				List<WebElement> allHeading = WebDriverFactory.getDriver().findElements(all_service_heading);
				for (WebElement element : allHeading) {
					String title = element.getText();
					if (title.equals("Lunch")) {
						status = true;
					} else if (title.equals("Breakfast")) {
						status = true;
					} else if (title.equals("Car Parking CC")) {
						status = true;
					} else if (title.equals("Early Check-in")) {
						status = true;
					} else if (title.equals("Continental Breakfast")) {
						status = true;
					} else if (title.equals("Bike Tour")) {
						status = true;
					}
				}
			}
		}
		return status;
	}

	public String getcheckinDate_in_Card(String id) {
		String checkindatefirst = pre_checkin_first_status + id + checkin_date_second + "/div[1]";
		String checkindatesecond = pre_checkin_first_status + id + checkin_date_second + "/div[2]";
		DynamicWait.waitForClick(By.xpath(checkindatefirst));
		String text1 = getTextFromElement(By.xpath(checkindatefirst));
		String text2 = getTextFromElement(By.xpath(checkindatesecond));

		// text = text.replace("\n", "").replace("\r", "");
		String text = text1.concat(text2);
		System.out.println("Checkin Date on the card ------------ " + text);
		return text;
	}

	public String get_numberOfDays_Stays(String id) {
		String stays = pre_checkin_first_status + id + total_stays_in_days_second;
		DynamicWait.waitForClick(By.xpath(stays));
		String days = getTextFromElement(By.xpath(stays));
		System.out.println("Guest Stays for ------------- " + days);
		return days;
	}

	public String getcheckoutDate_in_Card(String id) {
		String checkoutdate = pre_checkin_first_status + id + checkout_date_second;
		DynamicWait.waitForClick(By.xpath(checkoutdate));
		String date = getTextFromElement(By.xpath(checkoutdate));
		date = date.replace("\n", "").replace("\r", "");
		System.out.println("Checkout Date ----------- " + date);
		return date;
	}

	public String getBookingId_from_bookingTab() {
		// DynamicWait.waitForClick(bookingId_bookingTab);
		DynamicWait.waitUntil(12);
		String bookingId = getTextJavaSrcipt(bookingId_bookingTab);
		System.out.println("Booking Id from Booking Tab --------------- " + bookingId);
		return bookingId;
	}

	public String getBookingDate_from_bookingTab() {
		// DynamicWait.waitForClick(booking_date_bookingTab);
		String bookingDate = getTextJavaSrcipt(booking_date_bookingTab);
		System.out.println("Booking Date from Booking Tab --------------- " + bookingDate);
		return bookingDate;
	}

	public String getcheckinDate_from_BookingTab() {
		// DynamicWait.waitForClick(checkin_date_bookingTab);
		String date = getTextJavaSrcipt(checkin_date_bookingTab);
		System.out.println("Checkin date from Booking Tab ------------- " + date);
		return date;
	}

	public String getcheckoutDate_from_BookingTab() {
		// DynamicWait.waitForClick(checkout_date_bookingTab);
		String date = getTextJavaSrcipt(checkout_date_bookingTab);
		System.out.println("Checkout Date from Booking Tab ----------- " + date);
		return date;
	}

	public String getBookingSource_from_BookingTab() {
		// DynamicWait.waitForClick(bookingSource_bookingTab);
		String source = getTextJavaSrcipt(bookingSource_bookingTab);
		System.out.println("Booking Source from Booking Tab -------------- " + source);
		return source;
	}

	public void changeEstimatedCheckinTime(String time) {
		DynamicWait.waitForClick(checkinTime_bookingTab);
		scrollToElement(checkinTime_bookingTab);
		Select select = new Select(WebDriverFactory.getDriver().findElement(checkinTime_bookingTab));
		select.selectByVisibleText(time);
	}

	public void changeNumberOfGuest(String guest) {
		DynamicWait.waitForClick(numberOf_guest_bookingTab);
		Select select = new Select(WebDriverFactory.getDriver().findElement(numberOf_guest_bookingTab));
		select.selectByVisibleText(guest);
	}

	public void changeNumberOfChild(String child) {
		DynamicWait.waitForClick(numberOf_child_bookingTab);
		Select select = new Select(WebDriverFactory.getDriver().findElement(numberOf_guest_bookingTab));
		select.selectByVisibleText(child);
	}

	public String getGuestComment() {
		String text = null;
		if (isElementPresent(guest_comment_bookingTab) > 0) {
			// DynamicWait.waitForClick(guest_comment_bookingTab);
			text = getTextJavaSrcipt(guest_comment_bookingTab);
			System.out.println("Guest Comment from booking Tab --------------- " + text);

		}
		return text;
	}

	public void changeInternalNotes(String notes) {
		if (isElementPresent(internal_Notes_bookingTab) > 0) {
			sendValueToTextBox(internal_Notes_bookingTab, notes);
		}
	}

	public String getNotes() {
		String note = null;
		if (isElementPresent(notes_bookingTab) > 0) {
			note = getTextJavaSrcipt(notes_bookingTab);
			System.out.println("Notes from Booking Tab ------------- " + note);
		}
		return note;
	}

	public String getFirstName_from_BookingTab() {
		scrollToElement(firstName_bookingTab);
		String first = getTextJavaSrcipt(firstName_bookingTab);
		System.out.println("First name from Booking Tab -------------- " + first);
		return first;
	}

	public String getLastName_from_BookingTab() {
		String last = getTextJavaSrcipt(lastName_bookingTab);
		System.out.println("Last name from Booking Tab ------------ " + last);
		return last;
	}

	public void changeEmail(String email) {
		sendValueToTextBox(email_bookingTab, email);
	}

	public void changePhoneNumber(String phone) {
		sendValueToTextBox(phoneNumber_bookingTab, phone);
	}

	public void clickSaveChanges() {
		DynamicWait.waitForClick(saveChanges_button_bookingTab);
		clickOnButton(saveChanges_button_bookingTab);
		DynamicWait.waitUntil(10);
	}

	/*
	 * Payments Tab Methods Start
	 */

	public void clickOnPaymentTab() {
		scrollToElement(gotoTop);
		clickOnButton(paymentsTab);
		DynamicWait.waitUntil(8);
	}

	public int paymentStatusCard() {
		int count = isElementPresent(payment_Status_Cards);
		System.out.println("Total Payment status cards -------------- " + count);
		return count;
	}

	public int totalNumberOfCreditCard() {
		int count = isElementPresent(allCredit_Card);
		System.out.println("Total Credit Card present ------- " + count);
		return count;
	}

	public void additionalCharge(String amount, String description) {
		DynamicWait.waitForClick(chargeAdditional_button);
		clickOnButton(chargeAdditional_button);
		DynamicWait.waitForClick(amount_for_additional_charge);
		sendValueToTextBox(amount_for_additional_charge, amount);
		DynamicWait.waitForClick(description_for_additional_charge);
		sendValueToTextBox(description_for_additional_charge, description);
		clickOnButton(chargeProtection_toggle);
		clickOnButton(chargePaymentMethod);
		clickOnButton(firstOption);
		clickOnButton(charge_button_paymentTab);
		DynamicWait.waitForClick(yesDoIt_Button);
		clickOnButton(yesDoIt_Button);
		DynamicWait.waitUntil(5);
	}

	public void additionalChargeAtCard(String amount, String description) {
		DynamicWait.waitForClick(amount_for_additional_charge);
		sendValueToTextBox(amount_for_additional_charge, amount);
		DynamicWait.waitForClick(description_for_additional_charge);
		sendValueToTextBox(description_for_additional_charge, description);
		clickOnButton(chargeProtection_toggle);
		clickOnButton(chargePaymentMethod);
		clickOnButton(firstOption);
		clickOnButton(charge_button_paymentTab);
		DynamicWait.waitForClick(yesDoIt_Button);
		clickOnButton(yesDoIt_Button);
		DynamicWait.waitUntil(5);
	}

	public void clickOnAddPaymentMethod() {
		scrollToElement(addPayment_Method);
		DynamicWait.waitForClick(addPayment_Method);
		clickOnButton(addPayment_Method);
	}

	public void updateAfterAddPaymentMethod() {
		clickOnButton(update_button_after_add_Payment);
		DynamicWait.waitForClick(yesUpdate_button);
		clickOnButton(yesUpdate_button);
		DynamicWait.waitUntil(10);
	}

	public void clickOnRefundButton() {
		DynamicWait.waitForClick(refund_button);
		clickOnButton(refund_button);
	}

	public void doRefund(String amount, String description) {
		DynamicWait.waitForClick(refundButton_refundPopup);
		DynamicWait.waitForClick(refundAmount_textbox);
		sendValueToTextBox(refundAmount_textbox, amount);
		DynamicWait.waitForClick(refund_Description);
		sendValueToTextBox(refund_Description, description);
		clickOnButton(refundButton_refundPopup);
		DynamicWait.waitForClick(yesRefund_Now);
		clickOnButton(yesRefund_Now);
	}

	public String refund_DescriptionVerify() {
		DynamicWait.waitForClick(description_Text_Verify);
		String text = getTextFromElement(description_Text_Verify);
		System.out.println("Description Text Verify ------------ " + text);
		return text;
	}

	public String amountRefundVerify() {
		DynamicWait.waitForClick(amount_Verify);
		String text = getTextFromElement(amount_Verify);
		System.out.println("Amount text verify ----------- " + text);
		return text;
	}

	public void clickOnCheckin_Tab() {
		scrollToElement(gotoTop);
		clickOnButton(onlineCheckin_Tab);
		DynamicWait.waitUntil(8);
	}

	public void changeEstimatedArrivalTime(String time) {
		DynamicWait.waitForClick(estimatedTime_checkinTab);
		Select select = new Select(WebDriverFactory.getDriver().findElement(estimatedTime_checkinTab));
		select.selectByVisibleText(time);
	}

	public void changeArrivalBy(String arrival, String flight) {
		DynamicWait.waitForClick(arrivalBy_checkinTab);
		Select select = new Select(WebDriverFactory.getDriver().findElement(arrivalBy_checkinTab));
		select.selectByVisibleText(arrival);
		if (arrival.equals("Flight")) {
			DynamicWait.waitUntil(2);
			sendValueToTextBox(flight_Other_checkinTab, flight);
		}
	}

	public void clickOnSaveChangeAfterEdit() {
		scrollToElement(saveChanges_button_bookingTab);
		clickOnButton(saveChanges_button_bookingTab);
		DynamicWait.waitUntil(8);
	}

	/*
	 * Upsell
	 */

	public void clickOnUpsell() {
		scrollToElement(gotoTop);
		clickOnButton(upsell_Tab);
		DynamicWait.waitUntil(8);
	}

	public String verifyTitleUpsell() {
		DynamicWait.waitForClick(upsell_heading);
		String heading = getTextFromElement(upsell_heading);
		System.out.println("Upsell heading ------------- " + heading);
		return heading;
	}

	/*
	 * Documents
	 */

	public void clickOnDocument() {
		DynamicWait.waitForClick(document_Tab);
		clickOnButton(document_Tab);
		DynamicWait.waitUntil(8);
	}

	public int total_document() {
		DynamicWait.waitForClick(total_col_document);
		int count = isElementPresent(total_col_document);
		System.out.println("Total column ----------- " + count);
		return count;
	}

	public String getHeadingDocument() {
		DynamicWait.waitForClick(document_Heading);
		String text = getTextFromElement(document_Heading);
		System.out.println("Heading of the document ----------- " + text);
		return text;
	}

	/*
	 * Questionnaire
	 */

	public void fill_Questionnare(String textbox, String phoneNumber, String email, String number, String textarea) {
		DynamicWait.waitForClick(textbox_answer_questionnare);
		sendValueToTextBox(textbox_answer_questionnare, textbox);
		DynamicWait.waitForClick(phone_number_questionare);
		sendValueToTextBox(phone_number_questionare, phoneNumber);
		DynamicWait.waitForClick(email_questionnare);
		sendValueToTextBox(email_questionnare, email);
		DynamicWait.waitForClick(radio_questionnare);
		scrollToElement(email_questionnare);
		clickOnButtonJS(radio_questionnare);
		DynamicWait.waitForClick(number_questionnare);
		sendValueToTextBox(number_questionnare, number);
		DynamicWait.waitForClick(textArea_questionnare);
		sendValueToTextBox(textArea_questionnare, textarea);
		scrollToElement(textArea_questionnare);
		DynamicWait.waitForClick(choice_questionnare);
		clickOnButtonJS(choice_questionnare);
		clickOnButtonJS(checkbox_questionnare);
		clickOnButton(fileUpload_questionnare);
		String imgPath = System.getProperty("user.dir") + "\\uploadImage\\UserIcon.png";
		System.out.println("Image Path --------- " + imgPath);
		try {
			Robot robot = new Robot();

			// StringSelection str = new
			// StringSelection("C:\\Users\\Kamal\\Desktop\\UserIcon.png");
			StringSelection str = new StringSelection(imgPath);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str, null);

			robot.delay(5000);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);

			// release Contol+V for pasting
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_V);

			robot.delay(4000);
			// for pressing and releasing Enter
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (AWTException e) {
			e.printStackTrace();
		}
		DynamicWait.waitUntil(10);
	}

	/*
	 * Questionnaire Tab
	 */

	public String getTextBoxValue_questionTab() {
		scrollToElement(gotoTop);
		DynamicWait.waitForClick(questionnare_Tab);
		clickOnButton(questionnare_Tab);
		DynamicWait.waitForClick(textbox_questionTab);
		String text = getTextJavaSrcipt(textbox_questionTab);
		System.out.println("Textbox value ------------- " + text);
		return text;
	}

	public String getTextPhone_questionTab() {
		String text = getTextJavaSrcipt(phoneNumber_questionTab);
		System.out.println("Phone number value ------------ " + text);
		return text;
	}

	public String getEmail_questionTab() {
		String text = getTextJavaSrcipt(email_questionTab);
		System.out.println("Email Id Value --------------- " + text);
		return text;
	}

	public String getNumber_questionTab() {
		String text = getTextJavaSrcipt(number_questionTab);
		System.out.println("Number Text value ---------- " + text);
		return text;
	}

	public String textarea_questionTab() {
		String text = getTextJavaSrcipt(textarea_questionTab);
		System.out.println("Text area value ---------- " + text);
		return text;
	}

	public void clickOnVoid(String id) {
		// boolean status = false;
		DynamicWait.waitForClick(paid_Reservation_Charge);
		String value = getAttributeValue(paid_Reservation_Charge, "class");
		if (value.contains("warning")) {
			String menu = paid_Reservation_Charge_menu_First + id + paid_Reservation_Charge_menu_Second;
			System.out.println("Menu Locator ------------ " + menu);
			DynamicWait.waitForClick(By.xpath(menu));
			clickOnButton(By.xpath(menu));
			String voidLocator = paid_Reservation_Charge_ChargeNow_first + id + paid_Reservation_Charge_Void;
			System.out.println("Void Locator --------------- " + voidLocator);
			DynamicWait.waitForClick(By.xpath(voidLocator));
			clickOnButton(By.xpath(voidLocator));
			DynamicWait.waitForClick(yesDoIt_Button);
			clickOnButton(yesDoIt_Button);

			String menuLocatorSecondCard = paid_Reservation_Charge_menu_First + id
					+ paid_Reservation_Charge_menu_Second_secondCard;
			System.out.println("Second Card Menu Locator --------- " + menuLocatorSecondCard);
			DynamicWait.waitForClick(By.xpath(menuLocatorSecondCard));
			clickOnButton(By.xpath(menuLocatorSecondCard));
			DynamicWait.waitForClick(By.xpath(voidLocator));
			clickOnButton(By.xpath(voidLocator));
			if (isElementPresent(yesDoIt_Button) > 0) {
				clickOnButton(yesDoIt_Button);
			} else if (isElementPresent(yes_Button) > 0) {
				clickOnButton(yes_Button);
			}

			String menuLocatorthirdCard = paid_Reservation_Charge_menu_First + id
					+ paid_Reservation_Charge_menu_Second_thirdCard;
			System.out.println("Third Card Menu Locator --------- " + menuLocatorthirdCard);
			if (isElementPresent(By.xpath(menuLocatorthirdCard)) > 0) {
				DynamicWait.waitForClick(By.xpath(menuLocatorthirdCard));
				clickOnButton(By.xpath(menuLocatorthirdCard));
				DynamicWait.waitForClick(By.xpath(voidLocator));
				clickOnButton(By.xpath(voidLocator));
				DynamicWait.waitUntil(1);
				if (isElementPresent(yesDoIt_Button) > 0) {
					clickOnButton(yesDoIt_Button);
				} else if (isElementPresent(yes_Button) > 0) {
					clickOnButton(yes_Button);
				}
			}
		}
	}

	public void chargeNowFromCard(String id) {
		DynamicWait.waitForClick(paid_Reservation_Charge);
		String value = getAttributeValue(paid_Reservation_Charge, "class");
		if (value.contains("warning")) {
			String authorizeNowLocator = paid_Reservation_Charge_ChargeNow_first + id
					+ paid_Reservation_Charge_AuthorizeNow;

			String menu = paid_Reservation_Charge_menu_First + id + paid_Reservation_Charge_menu_Second;
			System.out.println("Menu Locator ------------ " + menu);
			DynamicWait.waitForClick(By.xpath(menu));
			clickOnButton(By.xpath(menu));
			String chargeNowLocator = paid_Reservation_Charge_ChargeNow_first + id
					+ paid_Reservation_Charge_ChargeNow_second;
			System.out.println("Charge Now Locator --------------- " + chargeNowLocator);
			DynamicWait.waitForClick(By.xpath(chargeNowLocator));
			clickOnButton(By.xpath(chargeNowLocator));
			DynamicWait.waitUntil(1);
			if (isElementPresent(yesDoIt_Button) > 0) {
				clickOnButton(yesDoIt_Button);
			} else if (isElementPresent(yes_Button) > 0) {
				clickOnButton(yes_Button);
			}
			DynamicWait.waitUntil(7);

			String menuLocatorSecondCard = paid_Reservation_Charge_menu_First + id
					+ paid_Reservation_Charge_menu_Second_secondCard;
			System.out.println("Second Card Menu Locator --------- " + menuLocatorSecondCard);
			DynamicWait.waitForClick(By.xpath(menuLocatorSecondCard));
			clickOnButton(By.xpath(menuLocatorSecondCard));
			if (isElementPresent(By.xpath(chargeNowLocator)) > 0) {
				DynamicWait.waitForClick(By.xpath(chargeNowLocator));
				clickOnButton(By.xpath(chargeNowLocator));
			} else if (isElementPresent(By.xpath(authorizeNowLocator)) > 0) {
				DynamicWait.waitForClick(By.xpath(authorizeNowLocator));
				clickOnButton(By.xpath(authorizeNowLocator));
			}

			DynamicWait.waitUntil(1);
			if (isElementPresent(yesDoIt_Button) > 0) {
				clickOnButton(yesDoIt_Button);
			} else if (isElementPresent(yes_Button) > 0) {
				clickOnButton(yes_Button);
			}
			DynamicWait.waitUntil(7);

			String menuLocatorthirdCard = paid_Reservation_Charge_menu_First + id
					+ paid_Reservation_Charge_menu_Second_thirdCard;
			System.out.println("Third Card Menu Locator --------- " + menuLocatorthirdCard);
			if (isElementPresent(By.xpath(menuLocatorthirdCard)) > 0) {
				DynamicWait.waitForClick(By.xpath(menuLocatorthirdCard));
				clickOnButton(By.xpath(menuLocatorthirdCard));

				System.out.println("Authorize Now Locator ------------ " + authorizeNowLocator);
				DynamicWait.waitForClick(By.xpath(authorizeNowLocator));
				clickOnButton(By.xpath(authorizeNowLocator));
				DynamicWait.waitUntil(1);
				if (isElementPresent(yesDoIt_Button) > 0) {
					clickOnButton(yesDoIt_Button);
				} else if (isElementPresent(yes_Button) > 0) {
					clickOnButton(yes_Button);
				}
			}

		}
	}

	public void changeAmount(String amount) {
		DynamicWait.waitForClick(paid_Reservation_Charge_New_Amount);
		sendValueToTextBox(paid_Reservation_Charge_New_Amount, amount);
		DynamicWait.waitForClick(paid_Reservation_Charge_Reduce_Button);
		clickOnButton(paid_Reservation_Charge_Reduce_Button);
		DynamicWait.waitForClick(yesDoIt_Button);
		clickOnButton(yesDoIt_Button);
		DynamicWait.waitUntil(5);
	}

	public void changeAmountMarkasPaid(String id) {
		DynamicWait.waitForClick(paid_Reservation_Charge);
		String value = getAttributeValue(paid_Reservation_Charge, "class");
		if (value.contains("warning")) {

			String menu = paid_Reservation_Charge_menu_First + id + paid_Reservation_Charge_menu_Second;
			System.out.println("Menu Locator ------------ " + menu);
			DynamicWait.waitForClick(By.xpath(menu));
			clickOnButton(By.xpath(menu));
			String changeAmountLocator = paid_Reservation_Charge_ChargeNow_first + id
					+ paid_Reservation_Charge_ChangeAmount_Second;
			System.out.println("Change Amount Locator --------------- " + changeAmountLocator);
			DynamicWait.waitForClick(By.xpath(changeAmountLocator));
			clickOnButton(By.xpath(changeAmountLocator));
			DynamicWait.waitUntil(1);
			changeAmount("1000");
			DynamicWait.waitForClick(By.xpath(menu));
			clickOnButton(By.xpath(menu));
			String markAsPaidLocator = paid_Reservation_Charge_ChargeNow_first + id
					+ paid_Reservation_Charge_MarkAsPaid_Second;
			System.out.println("Mark As Paid Locator --------------- " + markAsPaidLocator);
			DynamicWait.waitForClick(By.xpath(markAsPaidLocator));
			clickOnButton(By.xpath(markAsPaidLocator));
			if (isElementPresent(yesDoIt_Button) > 0) {
				clickOnButton(yesDoIt_Button);
			} else if (isElementPresent(yes_Button) > 0) {
				clickOnButton(yes_Button);
			}
			DynamicWait.waitUntil(7);

			/*
			 * String menuLocatorSecondCard = paid_Reservation_Charge_menu_First + id +
			 * paid_Reservation_Charge_menu_Second_secondCard;
			 * System.out.println("Second Card Menu Locator --------- " +
			 * menuLocatorSecondCard);
			 * DynamicWait.waitForClick(By.xpath(menuLocatorSecondCard));
			 * clickOnButton(By.xpath(menuLocatorSecondCard));
			 * DynamicWait.waitForClick(By.xpath(changeAmountLocator));
			 * clickOnButton(By.xpath(changeAmountLocator)); DynamicWait.waitUntil(1);
			 * changeAmount("1000");
			 * DynamicWait.waitForClick(By.xpath(menuLocatorSecondCard));
			 * clickOnButton(By.xpath(menuLocatorSecondCard));
			 * DynamicWait.waitForClick(By.xpath(markAsPaidLocator));
			 * clickOnButton(By.xpath(markAsPaidLocator)); if
			 * (isElementPresent(yesDoIt_Button) > 0) { clickOnButton(yesDoIt_Button); }
			 * else if (isElementPresent(yes_Button) > 0) { clickOnButton(yes_Button); }
			 * DynamicWait.waitUntil(7);
			 */

		}
	}

	public void AuthorizeAndRelease(String id) {
		String menuLocatorSecondCard = paid_Reservation_Charge_menu_First + id
				+ paid_Reservation_Charge_menu_Second_secondCard;
		System.out.println("Second Card Menu Locator --------- " + menuLocatorSecondCard);

		String menuLocatorthirdCard = paid_Reservation_Charge_menu_First + id
				+ paid_Reservation_Charge_menu_Second_thirdCard;
		System.out.println("Second Card Menu Locator --------- " + menuLocatorthirdCard);
		if (isElementPresent(By.xpath(menuLocatorthirdCard)) > 0) {
			DynamicWait.waitForClick(By.xpath(menuLocatorthirdCard));
			clickOnButton(By.xpath(menuLocatorthirdCard));
		} else if (isElementPresent(By.xpath(menuLocatorSecondCard)) > 0) {
			DynamicWait.waitForClick(By.xpath(menuLocatorSecondCard));
			clickOnButton(By.xpath(menuLocatorSecondCard));
		}
		String authorizeNowLocator = paid_Reservation_Charge_ChargeNow_first + id
				+ paid_Reservation_Charge_AuthorizeNow;
		System.out.println("Authorize Now Locator ------------ " + authorizeNowLocator);
		DynamicWait.waitForClick(By.xpath(authorizeNowLocator));
		clickOnButton(By.xpath(authorizeNowLocator));
		DynamicWait.waitUntil(1);
		if (isElementPresent(yesDoIt_Button) > 0) {
			clickOnButton(yesDoIt_Button);
		} else if (isElementPresent(yes_Button) > 0) {
			clickOnButton(yes_Button);
		}
		DynamicWait.waitUntil(5);
		if (isElementPresent(By.xpath(menuLocatorthirdCard)) > 0) {
			DynamicWait.waitForClick(By.xpath(menuLocatorthirdCard));
			clickOnButton(By.xpath(menuLocatorthirdCard));
		} else if (isElementPresent(By.xpath(menuLocatorSecondCard)) > 0) {
			DynamicWait.waitForClick(By.xpath(menuLocatorSecondCard));
			clickOnButton(By.xpath(menuLocatorSecondCard));
		}
		String releaseLocator = paid_Reservation_Charge_ChargeNow_first + id + paid_Reservation_Charge_Release;
		System.out.println("Release Locator ------------ " + releaseLocator);
		DynamicWait.waitForClick(By.xpath(releaseLocator));
		clickOnButton(By.xpath(releaseLocator));
		DynamicWait.waitUntil(1);
		if (isElementPresent(yesDoIt_Button) > 0) {
			clickOnButton(yesDoIt_Button);
		} else if (isElementPresent(yes_Button) > 0) {
			clickOnButton(yes_Button);
		}	
		DynamicWait.waitUntil(3);
	}

	public void AuthorizeAndCapture(String id) {
		String menuLocatorSecondCard = paid_Reservation_Charge_menu_First + id
				+ paid_Reservation_Charge_menu_Second_secondCard;
		System.out.println("Second Card Menu Locator --------- " + menuLocatorSecondCard);
		
		String menuLocatorthirdCard = paid_Reservation_Charge_menu_First + id
				+ paid_Reservation_Charge_menu_Second_thirdCard;
		System.out.println("Second Card Menu Locator --------- " + menuLocatorthirdCard);
		if (isElementPresent(By.xpath(menuLocatorthirdCard)) > 0) {
			DynamicWait.waitForClick(By.xpath(menuLocatorthirdCard));
			clickOnButton(By.xpath(menuLocatorthirdCard));
		} else if (isElementPresent(By.xpath(menuLocatorSecondCard)) > 0) {
			DynamicWait.waitForClick(By.xpath(menuLocatorSecondCard));
			clickOnButton(By.xpath(menuLocatorSecondCard));
		}
			String authorizeNowLocator = paid_Reservation_Charge_ChargeNow_first + id
					+ paid_Reservation_Charge_AuthorizeNow;
			System.out.println("Authorize Now Locator ------------ " + authorizeNowLocator);
			DynamicWait.waitForClick(By.xpath(authorizeNowLocator));
			clickOnButton(By.xpath(authorizeNowLocator));
			DynamicWait.waitUntil(1);
			if (isElementPresent(yesDoIt_Button) > 0) {
				clickOnButton(yesDoIt_Button);
			} else if (isElementPresent(yes_Button) > 0) {
				clickOnButton(yes_Button);
			}
			DynamicWait.waitUntil(5);
			if (isElementPresent(By.xpath(menuLocatorthirdCard)) > 0) {
				DynamicWait.waitForClick(By.xpath(menuLocatorthirdCard));
				clickOnButton(By.xpath(menuLocatorthirdCard));
			} else if (isElementPresent(By.xpath(menuLocatorSecondCard)) > 0) {
				DynamicWait.waitForClick(By.xpath(menuLocatorSecondCard));
				clickOnButton(By.xpath(menuLocatorSecondCard));
			}
			String captureLocator = paid_Reservation_Charge_ChargeNow_first + id + paid_Reservation_Charge_Capture;
			System.out.println("Capture Locator ------------ " + captureLocator);
			DynamicWait.waitForClick(By.xpath(captureLocator));
			clickOnButton(By.xpath(captureLocator));
			DynamicWait.waitForClick(paid_Reservation_Capture_Description);
			sendValueToTextBox(paid_Reservation_Capture_Description, "Test Capture Description");
			DynamicWait.waitForClick(paid_Reservation_Charge_CaptureNow_Button);
			clickOnButton(paid_Reservation_Charge_CaptureNow_Button);
			DynamicWait.waitUntil(1);
			if (isElementPresent(yesDoIt_Button) > 0) {
				clickOnButton(yesDoIt_Button);
			} else if (isElementPresent(yes_Button) > 0) {
				clickOnButton(yes_Button);
			}
			DynamicWait.waitUntil(3);
	}

	public void addCreditCardFromBookingList(String id) {
		String addCreditCardLocator = creditCardNumber_first + id + creditCardNumber_second;
		DynamicWait.waitForClick(By.xpath(addCreditCardLocator));
		clickOnButton(By.xpath(addCreditCardLocator));
	}

	public void AuthorizeNowWithout3D(String id) {

		DynamicWait.waitForClick(paid_Reservation_Charge);
		String value = getAttributeValue(paid_Reservation_Charge, "class");
		if (value.contains("warning")) {

			String menu = paid_Reservation_Charge_menu_First + id + paid_Reservation_Charge_menu_Second;
			System.out.println("Menu Locator ------------ " + menu);
			DynamicWait.waitForClick(By.xpath(menu));
			clickOnButton(By.xpath(menu));
			String changeAmountLocator = paid_Reservation_Charge_ChargeNow_first + id
					+ paid_Reservation_Charge_ChangeAmount_Second;
			System.out.println("Change Amount Locator --------------- " + changeAmountLocator);
			DynamicWait.waitForClick(By.xpath(changeAmountLocator));
			clickOnButton(By.xpath(changeAmountLocator));
			DynamicWait.waitUntil(1);
			changeAmount("1000");
			DynamicWait.waitForClick(By.xpath(menu));
			clickOnButton(By.xpath(menu));
			String markAsPaidLocator = paid_Reservation_Charge_ChargeNow_first + id
					+ paid_Reservation_Charge_MarkAsPaid_Second;
			System.out.println("Mark As Paid Locator --------------- " + markAsPaidLocator);
			DynamicWait.waitForClick(By.xpath(markAsPaidLocator));
			clickOnButton(By.xpath(markAsPaidLocator));
			if (isElementPresent(yesDoIt_Button) > 0) {
				clickOnButton(yesDoIt_Button);
			} else if (isElementPresent(yes_Button) > 0) {
				clickOnButton(yes_Button);
			}
			DynamicWait.waitUntil(7);

			String menuLocatorthirdCard = paid_Reservation_Charge_menu_First + id
					+ paid_Reservation_Charge_menu_Second_secondCard;
			System.out.println("Second Card Menu Locator --------- " + menuLocatorthirdCard);
			if (isElementPresent(By.xpath(menuLocatorthirdCard)) > 0) {
				DynamicWait.waitForClick(By.xpath(menuLocatorthirdCard));
				clickOnButton(By.xpath(menuLocatorthirdCard));
				String authorizeNowLocator = paid_Reservation_Charge_ChargeNow_first + id
						+ paid_Reservation_Charge_AuthorizeNow_Without3D;
				System.out.println("Authorize Now Locator ------------ " + authorizeNowLocator);
				DynamicWait.waitForClick(By.xpath(authorizeNowLocator));
				clickOnButton(By.xpath(authorizeNowLocator));
				DynamicWait.waitUntil(1);
				if (isElementPresent(yesDoIt_Button) > 0) {
					clickOnButton(yesDoIt_Button);
				} else if (isElementPresent(yes_Button) > 0) {
					clickOnButton(yes_Button);
				}
				DynamicWait.waitUntil(7);
			}
		}
	}

	public String authorize_Status(String id) {
		DynamicWait.waitUntil(3);
		String authorizeStatusLocator = guestName_first + id + authorizeStatus_second;
		System.out.println("Authorize Status ------------ " + authorizeStatusLocator);
		DynamicWait.waitForClick(By.xpath(authorizeStatusLocator));
		String authorize = getTextFromElement(By.xpath(authorizeStatusLocator));
		System.out.println("Authorize Text from card ----------- " + authorize);
		return authorize;
	}

	public String paid_Status(String id) {
		String paidStatus_Locator = guestName_first + id + paidStatus_second;
		System.out.println("Paid Status ------------ " + paidStatus_Locator);
		DynamicWait.waitForClick(By.xpath(paidStatus_Locator));
		String paid = getTextFromElement(By.xpath(paidStatus_Locator));
		System.out.println("Paid Status Text from card ------- " + paid);
		return paid;
	}

	public void clickOnAdditionalPaymentButton(String id) {
		String additionalBtn = additinalCharge_at_card_first + id + additionalCharge_at_card_second;
		System.out.println("Additional Charge Button locator ------ " + additionalBtn);
		DynamicWait.waitForClick(By.xpath(additionalBtn));
		clickOnButton(By.xpath(additionalBtn));
	}

	public int totalPaymentCardCount() {
		DynamicWait.waitForClick(total_paymentCards);
		int count = isElementPresent(total_paymentCards);
		System.out.println("Payments Card --------- " + count);
		return count;
	}

	public void setAssignedRental(String rental) {
		DynamicWait.waitForClick(assignedRental);
		Select select = new Select(WebDriverFactory.getDriver().findElement(assignedRental));
		select.selectByVisibleText(rental);
	}

	public void setBookingSource(String source) {
		DynamicWait.waitForClick(booking_source);
		Select select = new Select(WebDriverFactory.getDriver().findElement(booking_source));
		select.selectByVisibleText(source);
	}

	public void setDates() {
		String checkinDate = todaysDateForCheckin();
		System.out.println("Checkin date ------------- " + checkinDate);
		DynamicWait.waitForClick(dates_in_form);
		clickOnButton(dates_in_form);
		String checkindateLocator = checkinDate_first + checkinDate + checkinDate_second;
		System.out.println("Checkin date locator ------------- " + checkindateLocator);
		clickOnButton(By.xpath(checkindateLocator));
		DynamicWait.waitUntil(2);
		checkoutDateFromListing();
	}

	public void setPriceInForm(String price) {
		DynamicWait.waitForClick(price_in_form);
		sendValueToTextBox(price_in_form, price);
	}

	public void setNoteInForm(String notes) {
		DynamicWait.waitForClick(bookingNotes);
		sendValueToTextBox(bookingNotes, notes);
	}

	public void setFirstName() {
		DynamicWait.waitForClick(firstName_bookingForm);
		String name = new Faker().name().firstName();
		System.out.println("First Name ---------------- " + name);
		sendValueToTextBox(firstName_bookingForm, name);
	}

	public void setLastName() {
		DynamicWait.waitForClick(lastName_bookingForm);
		String name = new Faker().name().lastName();
		System.out.println("Last Name ----------- " + name);
		sendValueToTextBox(lastName_bookingForm, name);
	}

	public void setPhoneNumber() {
		DynamicWait.waitForClick(telephone_bookingForm);
		sendValueToTextBox(telephone_bookingForm, "9870001236");
	}

	public void setEmail() {
		DynamicWait.waitForClick(email_bookingForm);
		sendValueToTextBox(email_bookingForm, "pgtest@yopmail.com");
	}

	public void setAdults() {
		DynamicWait.waitForClick(adults_bookingForm);
		scrollToElement(adults_bookingForm);
		sendValueToTextBox(adults_bookingForm, "10");
	}

	public void setChild() {
		DynamicWait.waitForClick(child_bookingForm);
		sendValueToTextBox(child_bookingForm, "5");
	}

	public void clickOnSaveForms() {
		DynamicWait.waitForClick(saveChanges_bookingForm);
		clickOnButton(saveChanges_bookingForm);
		DynamicWait.waitUntil(2);
		WebDriverFactory.getDriver().navigate().refresh();
		DynamicWait.waitUntil(10);
	}

	public void openBookingForm() {
		DynamicWait.waitForClick(addBookingButton);
		clickOnButton(addBookingButton);
	}

	public void clickOnEdit_Booking(String cardId) {
		String editBookingLocator = shareLinkFirst + cardId + edit_Booking_second;
		System.out.println("Edit Booking Locator ------------ " + editBookingLocator);
		DynamicWait.waitForClick(By.xpath(editBookingLocator));
		clickOnButton(By.xpath(editBookingLocator));
	}

	public String getBookingStatus(String id) {
		String bookingStatus = pre_checkin_first_status + id + booking_status_second;
		System.out.println("Booking Status Locator ---------- " + bookingStatus);
		DynamicWait.waitForClick(By.xpath(bookingStatus));
		String status = getTextFromElement(By.xpath(bookingStatus));
		System.out.println("Booking Status --------- " + status);
		return status;
	}

	public void changeBookingStatus(String status) {
		DynamicWait.waitForClick(reservation);
		Select select = new Select(WebDriverFactory.getDriver().findElement(reservation));
		select.selectByVisibleText(status);
	}

	public void charge_and_AuthorizeNowWithout3D(String id) {

		DynamicWait.waitForClick(paid_Reservation_Charge);
		String value = getAttributeValue(paid_Reservation_Charge, "class");
		if (value.contains("warning")) {

			String menu = paid_Reservation_Charge_menu_First + id + paid_Reservation_Charge_menu_Second;
			System.out.println("Menu Locator ------------ " + menu);
			DynamicWait.waitForClick(By.xpath(menu));
			clickOnButton(By.xpath(menu));
			String chargeWithout3DLocator = paid_Reservation_Charge_ChargeNow_first + id
					+ paid_Reservation_Charge_Without3D;
			System.out.println("Charge now without 3D Locator --------------- " + chargeWithout3DLocator);
			DynamicWait.waitForClick(By.xpath(chargeWithout3DLocator));
			clickOnButton(By.xpath(chargeWithout3DLocator));
			DynamicWait.waitUntil(1);
			if (isElementPresent(yesDoIt_Button) > 0) {
				clickOnButton(yesDoIt_Button);
			} else if (isElementPresent(yes_Button) > 0) {
				clickOnButton(yes_Button);
			}
			DynamicWait.waitUntil(7);

			String menuLocatorthirdCard = paid_Reservation_Charge_menu_First + id
					+ paid_Reservation_Charge_menu_Second_secondCard;
			System.out.println("Second Card Menu Locator --------- " + menuLocatorthirdCard);
			if (isElementPresent(By.xpath(menuLocatorthirdCard)) > 0) {
				DynamicWait.waitForClick(By.xpath(menuLocatorthirdCard));
				clickOnButton(By.xpath(menuLocatorthirdCard));
				String authorizeNowLocator = paid_Reservation_Charge_ChargeNow_first + id
						+ paid_Reservation_Charge_AuthorizeNow_Without3D;
				System.out.println("Authorize Now Locator ------------ " + authorizeNowLocator);
				DynamicWait.waitForClick(By.xpath(authorizeNowLocator));
				clickOnButton(By.xpath(authorizeNowLocator));
				DynamicWait.waitUntil(1);
				if (isElementPresent(yesDoIt_Button) > 0) {
					clickOnButton(yesDoIt_Button);
				} else if (isElementPresent(yes_Button) > 0) {
					clickOnButton(yes_Button);
				}
				DynamicWait.waitUntil(7);
			}
		}
	}

}
