package com.page;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.driverFactory.WebDriverFactory;
import com.github.javafaker.Faker;
import com.pageLocators.ProfileUpdateLocators;
import com.pageLocators.TeamsLocators;
//import com.testScripts.TeamsTestScript;
import com.utilities.Constant;
import com.utilities.DynamicWait;

public class TeamsComponent extends BasePage implements TeamsLocators, ProfileUpdateLocators {

	public static HashMap<String, ArrayList<String>> teamsEmailList = new HashMap<String, ArrayList<String>>();
	ArrayList<String> cardsID = new ArrayList<String>();
	ArrayList<String> allTeamsEmails = new ArrayList<String>();

	public TeamsComponent(WebDriver driver) {
		super(driver);
	}

	public void clickOnTeams() {
		DynamicWait.waitForClick(menuDropdown);
		clickOnButton(menuDropdown);
		DynamicWait.waitForClick(team);
		clickOnButton(team);
	}

	public void clickOnAddTeams() {
		DynamicWait.waitForClick(addTeamMember);
		clickOnButton(addTeamMember);
	}

	public void sendTeamProfileIcon() {
		DynamicWait.waitForClick(teamFirstName);
		// sendValueToTextBox(teamUserImg, System.getProperty("user.dir") +
		// "/uploadImage/UserIcon.png");
		WebDriverFactory.getDriver().findElement(teamUserImg)
				.sendKeys(System.getProperty("user.dir") + "/uploadImage/UserIcon.png");
	}

	public void enterTeamsFirstName(String firstName) {
		DynamicWait.waitForClick(teamFirstName);
		sendValueToTextBox(teamFirstName, firstName);
	}

	public void editTeamsFirstName(String firstName) {
		DynamicWait.waitForClick(editFirstName);
		sendValueToTextBox(editFirstName, firstName);
	}

	public void enterTeamLastName(String lastname) {
		DynamicWait.waitForClick(teamLastName);
		sendValueToTextBox(teamLastName, lastname);
	}

	public void enterTeamEmail(String emailId) {
		DynamicWait.waitForClick(teamEmail);
		sendValueToTextBox(teamEmail, emailId);
	}

	public void enterTeamPhoneNumber(String phnNum) {
		// DynamicWait.waitForClick(teamPhone);
		sendValueToTextBox(teamPhone, phnNum);
	}

	public void selectTeamRole(String role) {
		// DynamicWait.waitForClick(roleManager);
		if (role.equals(Constant.MANAGER_ROLE)) {
			clickOnButton(roleManager);
		} else {
			clickOnButton(roleTeams);
		}
	}

	public void getAccountSetup() {
		DynamicWait.waitForClick(accountSetup);
		clickOnButton(accountSetup);
	}

	public void getBookings() {
		DynamicWait.waitForClick(bookings);
		clickOnButton(bookings);
	}

	public void getGuestExperience() {
		DynamicWait.waitForClick(guestExperience);
		clickOnButton(guestExperience);
	}

	public void getPreferences() {
		DynamicWait.waitForClick(preferences);
		clickOnButton(preferences);
	}

	public void getProperties() {
		DynamicWait.waitForClick(properties);
		clickOnButton(properties);
	}

	public String validateForm() {
		String errorMsg = "null";
		if (isElementPresent(teamfirstnameerror) > 0) {
			if (getTextFromElement(teamfirstnameerror).length() > 1) {
				errorMsg = getTextFromElement(teamfirstnameerror);
			}
		} else if (isElementPresent(teamlastnameerror) > 0) {
			if (getTextFromElement(teamlastnameerror).length() > 1) {
				errorMsg = getTextFromElement(teamlastnameerror);
			}
		} else if (isElementPresent(teamemailerror) > 0) {
			if (getTextFromElement(teamemailerror).length() > 1) {
				errorMsg = getTextFromElement(teamemailerror);
			}
		}
		return errorMsg;
	}

	public void clickOnSendInvitation() {
		DynamicWait.waitForClick(sendInvitation);
		clickOnButton(sendInvitation);
		DynamicWait.waitUntil(3);
	}

	public void getCardId() {
		List<WebElement> cards = WebDriverFactory.getDriver().findElements(cardCount);
		for (WebElement element : cards) {
			String idValue = element.getAttribute("id");
			idValue = idValue.replaceAll("[^0-9]", "");
			// String cardsLocator = "user-de-card-da-id" + idValue;
			cardsID.add(idValue);
		}
		System.out.println("Cards ID ----------------- " + cardsID);
	}

	public void getEmailsTextFromCard() {
		String idText = "user-de-card-da-id";
		getCardId();
		for (int i = 0; i < cardsID.size(); i++) {
			String id = idText + cardsID.get(i);
			String finalId = teamsCardEmailsFirst + '"' + id + '"' + teamsCardEmailsSecond;
			By emailColumn = By.xpath(finalId);
			System.out.println("Email locators ------------- " + emailColumn);
			scrollToElement(emailColumn);
			String emailsText = getTextFromElement(emailColumn);
			allTeamsEmails.add(emailsText);
		}
		System.out.println("All Teams Emails from card ------------- " + allTeamsEmails);
	}

	public void resendInvitation() {
		String menuId = "moreMenu-";
		for (int i = 0; i < cardsID.size(); i++) {
			for (int j = 0; j < teamsEmailList.size(); j++) {
				if (allTeamsEmails.get(i).equals(teamsEmailList.get(j))) {
					String id = menuId + cardsID.get(i);
					String finalMenuId = moreMenufirst + '"' + id + '"' + moreMenuSecond;
					By moreMenu = By.xpath(finalMenuId);
					System.out.println("More Menu Locator ------------- " + moreMenu);
					scrollToElement(moreMenu);
					clickOnButton(moreMenu);
					// DynamicWait.waitForClick(resend);
					// clickOnButton(resend);
					DynamicWait.waitForClick(yesResend);
					clickOnButton(yesResend);
				}
			}
		}
	}

	public void resendsInvitation(String emailId) {
		String idText = "user-de-card-da-id";
		String menuId = "moreMenu-";
		List<WebElement> cards = WebDriverFactory.getDriver().findElements(cardCount);
		for (WebElement element : cards) {
			String idValue = element.getAttribute("id");
			idValue = idValue.replaceAll("[^0-9]", "");

			String id = idText + idValue;
			String finalId = teamsCardEmailsFirst + '"' + id + '"' + teamsCardEmailsSecond;
			By emailColumn = By.xpath(finalId);
			System.out.println("Email locators ------------- " + emailColumn);
			scrollToElement(emailColumn);
			String emailsText = getTextFromElement(emailColumn);

			if (teamsEmailList.containsKey(emailId)) {
				int eachValueCount = teamsEmailList.values().size();
				for (int i = 0; i < eachValueCount; i++) {
					if (emailsText.equals(teamsEmailList.get(emailId).get(i))) {
						String menuID = menuId + idValue;
						String finalMenuId = moreMenufirst + '"' + menuID + '"' + moreMenuSecond;
						By moreMenu = By.xpath(finalMenuId);
						System.out.println("More Menu Locator ------------- " + moreMenu);
						clickOnButton(moreMenu);
						// DynamicWait.waitForClick(resend);
						// clickOnButton(resend);
						DynamicWait.waitForClick(yesResend);
						clickOnButton(yesResend);
					}
				}
			}
		}
	}

	public void teamResendInvitation(String emailId) {
		int cardCounts = isElementPresent(cardCount);
		ArrayList<String> allKeys = new ArrayList<String>();
		ArrayList<String> values = new ArrayList<String>();
		int keySize;

		for (String key : teamsEmailList.keySet()) {
			System.out.println(key);
			allKeys.add(key);
		}
		keySize = allKeys.size();
		values = teamsEmailList.get(emailId);
		for (int j = 0; j < keySize; j++) {
			if (emailId.equals(allKeys.get(j))) {
				for (int i = 2; i <= cardCounts + 1; i++) {
					By emailLocator = By.xpath(emailLocatorFirstPart + i + emailLocatorSecondPart);
					String emailText = getTextFromElement(emailLocator);

					if (i > 2) {
						By scrollLocator = By.xpath(emailLocatorFirstPart + (i - 1) + emailLocatorSecondPart);
						System.out.println("Scroll Locator ------------ " + scrollLocator);
						scrollToElement(scrollLocator);
					}

					for (int k = 0; k < values.size(); k++) {
						if (emailText.equals(values.get(k))) {
							By menuLocator = By.xpath(moreOptionFirstPart + i + moreOptionSecondPart);
							System.out.println("More Menu Locator ------------- " + menuLocator);
							clickOnButton(menuLocator);
							// DynamicWait.waitForClick(resend);
							DynamicWait.waitUntil(1);
							String resendStr = resend + (i - 2);
							By resendId = By.id(resendStr);
							System.out.println("Resend Locator -------- " + resendId);
							clickOnButton(resendId);
							DynamicWait.waitForClick(yesResend);
							clickOnButton(yesResend);
							DynamicWait.waitUntil(7);
							// i = 1;
						}
					}
				}
			}
		}
	}

	public void teamActivateDeactivate(String emailId) {
		int cardCounts = isElementPresent(cardCount);
		ArrayList<String> allKeys = new ArrayList<String>();
		ArrayList<String> values = new ArrayList<String>();

		for (String key : teamsEmailList.keySet()) {
			System.out.println(key);
			allKeys.add(key);
		}
		values = teamsEmailList.get(emailId);
		for (int j = 0; j < allKeys.size(); j++) {
			if (emailId.equals(allKeys.get(j))) {
				for (int i = 2; i <= cardCounts + 1; i++) {
					By emailLocator = By.xpath(emailLocatorFirstPart + i + emailLocatorSecondPart);
					String emailText = getTextFromElement(emailLocator);

					if (i > 2) {
						By scrollLocator = By.xpath(emailLocatorFirstPart + (i - 1) + emailLocatorSecondPart);
						System.out.println("Scroll Locator ------------ " + scrollLocator);
						scrollToElement(scrollLocator);
					}

					for (int k = 0; k < values.size(); k++) {
						if (emailText.equals(values.get(k))) {
							By menuLocator = By.xpath(moreOptionFirstPart + i + moreOptionSecondPart);
							System.out.println("More Menu Locator -------------- " + menuLocator);
							clickOnButton(menuLocator);
							DynamicWait.waitUntil(1);
							String deactivatestr = deactivate + (i - 2);
							By deactivateId = By.id(deactivatestr);
							System.out.println("Deactivate Locator ------------ " + deactivateId);
							clickOnButton(deactivateId);
							DynamicWait.waitForClick(yesDoIt);
							clickOnButton(yesDoIt);
							DynamicWait.waitUntil(2);
							if (k > 0) {
								DynamicWait.waitUntil(1);
							}
							String toastMsg = new ProfileUpdateComponent(WebDriverFactory.getDriver()).getToasterMsg();
							System.out.println("Toast Message after deactivate ----------- " + toastMsg);
							DynamicWait.waitUntil(5);
							if (toastMsg.equals(Constant.DEACTIVATE_SUCCESS)) {
								DynamicWait.waitUntil(1);
								clickOnButton(menuLocator);
								System.out.println("Menu button clicked........");
								DynamicWait.waitUntil(1);
								String activateStr = activate + (i - 2);
								By activateId = By.id(activateStr);
								clickOnButton(activateId);
								System.out.println("Activated button clicked...." + activateId);
								DynamicWait.waitForClick(yesDoIt);
								clickOnButton(yesDoIt);
								DynamicWait.waitUntil(10);
								System.out.println("Activated successfully");
								// i = 1;
							}
						}
					}
				}
			}
		}
	}

	public void editTeams(String emailId) {
		int cardCounts = isElementPresent(cardCount);
		ArrayList<String> allKeys = new ArrayList<String>();
		ArrayList<String> values = new ArrayList<String>();

		for (String keys : teamsEmailList.keySet()) {
			System.out.println(keys);
			allKeys.add(keys);
		}

		values = teamsEmailList.get(emailId);
		for (int j = 0; j < allKeys.size(); j++) {
			if (emailId.equals(allKeys.get(j))) {
				for (int i = 2; i <= cardCounts; i++) {
					By emailLocator = By.xpath(emailLocatorFirstPart + i + emailLocatorSecondPart);
					String emailText = getTextFromElement(emailLocator);

					if (i > 2) {
						By scrollLocator = By.xpath(emailLocatorFirstPart + (i - 1) + emailLocatorSecondPart);
						System.out.println("Scroll Locator ------------ " + scrollLocator);
						scrollToElement(scrollLocator);
					}

					for (int k = 0; k < values.size(); k++) {
						if (emailText.equals(values.get(k))) {
							By menuLocator = By.xpath(moreOptionFirstPart + i + moreOptionSecondPart);
							System.out.println("Menu Locator ------------- " + menuLocator);
							clickOnButton(menuLocator);
							DynamicWait.waitUntil(1);
							String editStr = editBtn + (i - 2);
							By editLocator = By.id(editStr);
							System.out.println("Edit locator -------- " + editLocator);
							clickOnButton(editLocator);
							DynamicWait.waitForClick(editFirstName);
							editTeamsFirstName(new Faker().name().firstName());
							String saveChangeStr = saveChanges + (i - 2);
							By saveChangeLocator = By.id(saveChangeStr);
							System.out.println("Save Changes Locators ----------- " + saveChangeLocator);
							clickOnButton(saveChangeLocator);
							if (k > 0) {
								DynamicWait.waitUntil(7);
							}
							String toastMsg = new ProfileUpdateComponent(WebDriverFactory.getDriver()).getToasterMsg();
							System.out.println("Toaster Message is --------------- " + toastMsg);
							// i = 1;
						}
					}
				}
			}
		}
	}

	public void deleteTeams(String emailId) {
		int cardCounts = isElementPresent(cardCount);
		ArrayList<String> allKeys = new ArrayList<String>();
		ArrayList<String> values = new ArrayList<String>();

		for (String keys : teamsEmailList.keySet()) {
			System.out.println(keys);
			allKeys.add(keys);
		}

		values = teamsEmailList.get(emailId);
		for (int j = 0; j < allKeys.size(); j++) {
			if (emailId.equals(allKeys.get(j))) {
				for (int i = 2; i <= cardCounts; i++) {
					By emailLocator = By.xpath(emailLocatorFirstPart + i + emailLocatorSecondPart);
					String emailText = getTextFromElement(emailLocator);
					if (i > 2) {
						By scrollLocator = By.xpath(emailLocatorFirstPart + (i - 1) + emailLocatorSecondPart);
						System.out.println("Scroll Locator ------------ " + scrollLocator);
						scrollToElement(scrollLocator);
					}
					for (int k = 0; k < values.size(); k++) {
						if (emailText.equals(values.get(k))) {
							By menuLocator = By.xpath(moreOptionFirstPart + i + moreOptionSecondPart);
							System.out.println("Menu Locator ------------- " + menuLocator);
							clickOnButton(menuLocator);
							DynamicWait.waitUntil(1);
							String editStr = editBtn + (i - 2);
							By editLocator = By.id(editStr);
							System.out.println("Edit Locator ----------- " + editLocator);
							clickOnButton(editLocator);
							DynamicWait.waitForClick(editFirstName);
							String deleteBtnStr = deleteBtn + (i - 2);
							By deleteLocator = By.id(deleteBtnStr);
							System.out.println("Delete Locator -------------- " + deleteLocator);
							clickOnButton(deleteLocator);
							DynamicWait.waitForClick(yesDeleteIt);
							clickOnButton(yesDeleteIt);
							String toastMsg = new ProfileUpdateComponent(WebDriverFactory.getDriver()).getToasterMsg();
							System.out.println("Toaster Message ------------- " + toastMsg);
							i = 1;
							cardCounts = isElementPresent(cardCount);
							DynamicWait.waitUntil(7);
						}
					}

				}
			}
		}
	}

}
