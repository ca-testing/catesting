package com.page;

import org.openqa.selenium.WebDriver;

import com.pageLocators.LoginLocators;
import com.utilities.DynamicWait;

public class LoginComponent extends BasePage implements LoginLocators {

	public LoginComponent(WebDriver driver) {
		super(driver);
	}

	public void clickOnLoginAtHomePage() {
		DynamicWait.waitForClick(loginBtn);
		clickOnButton(loginBtn);
	}

	public void waitForLoginButton() {
		DynamicWait.waitForClick(loginBtn);
	}

	public void enterEmail(String emailId) {
		DynamicWait.waitForClick(emailText);
		sendValueToTextBox(emailText, emailId);
	}

	public void enterEmails(String emailId) {
		DynamicWait.waitForClick(emailTexts);
		sendValueToTextBox(emailTexts, emailId);
	}

	public void enterPassword(String password) {
		DynamicWait.waitForClick(passwordText);
		sendValueToTextBox(passwordText, password);
	}

	public void enterPasswords(String password) {
		DynamicWait.waitForClick(passwordTexts);
		sendValueToTextBox(passwordTexts, password);
	}

	/*
	 * public PaymentRequestComponent clickOnLogin() {
	 * DynamicWait.waitForClick(signInBtn); clickOnButton(signInBtn); return new
	 * PaymentRequestComponent(WebDriverFactory.getDriver()); }
	 */

	public void clickOnLogin() {
		DynamicWait.waitForClick(signInBtn);
		clickOnButton(signInBtn);
		DynamicWait.waitUntil(2);
		// return new PaymentRequestComponent(WebDriverFactory.getDriver());
	}
	
	public void clickOnDashBoard() {
		int btn_Count = isElementPresent(dashboard);
		if (btn_Count > 0) {
			clickOnButton(dashboard);
		}
	}

	public boolean validateLogin() {
		boolean status = false;
		int count = isElementPresent(errorMsg);
		System.out.println("Error message count ----- > " + count);
		if (count > 0) {
			status = false;
		} else {
			status = true;
		}
		return status;
	}

}
