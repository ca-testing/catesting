package com.driverFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.utilities.Constant;

import io.github.bonigarcia.wdm.WebDriverManager;

public class WebDriverFactory {

	public static ThreadLocal<WebDriver> tldriver = new ThreadLocal<WebDriver>();

	/*
	 * This method is used to initialize the thread local driver on the basis of
	 * given browser
	 * 
	 * @param browser
	 * 
	 * @return
	 */

	public WebDriver init_Driver(String browser) {
		System.out.println("Driver to initialize === " + browser);
		
		if (browser.equalsIgnoreCase(Constant.CHROME_BROWSER)) {
			WebDriverManager.chromedriver().setup();
			tldriver.set(new ChromeDriver());
		} else if (browser.equalsIgnoreCase(Constant.FIREFOX_BROWSER)) {
			WebDriverManager.firefoxdriver().setup();
			tldriver.set(new FirefoxDriver());
		}
		
		getDriver().manage().deleteAllCookies();
		getDriver().manage().window().maximize();
		
		return getDriver();
	}
	
	/*
	 * This is used to get the driver using thread local
	 * 
	 * @return
	 */
	
	public static synchronized WebDriver getDriver() {
		return tldriver.get();
	}

}
