package com.pageLocators;

import org.openqa.selenium.By;

public interface ProfileUpdateLocators {

	By menuDropdown = By.id("dropdownMenuButton");
	By profileUpdate = By.name("Profile Update");
	By changePWD = By.name("Change Password");
	By team = By.xpath("//a[contains(text(),' Team ')]");
	By logout = By.xpath("//a[contains(text(),' Logout')]");
	By uploadProfileBtn = By.id("user_img_btn");
	By uploadProfile = By.id("user_img");
	By userName = By.id("u_name");
	By userNameError = By.xpath("//input[@id='u_name']/following::strong[1]");
	By userTelephone = By.name("telephone");
	By userTelePhoneErrorMsg = By.xpath("//*[contains(@name,'telephone')]/following::strong[1]");
	By userAddress = By.id("u_address");
	By userEmail = By.id("u_email");

	/*
	 * Company details Locators
	 */

	//By companyLogoBtn = By.id("company_img");
	//By companyLogoBtn = By.name("company_img");
	By companyLogoBtn = By.xpath("//input[@name='company_img']");
	By companyName = By.id("c_name");
	By companyNameErrorMsg = By.xpath("//*[contains(@id,'c_name')]/following::strong[1]");
	By companyEmailErrorMsg = By.xpath("//*[contains(@id,'c_email')]/following::strong");
	By companyEmail = By.id("c_email");
	
	By companyAddress = By.id("c_address");

	By cancelBtn = By.id("update_client_porfile_modal_close_btn");
	By save_Changes = By.xpath("//button[contains(text(),' Save Changes')]");
}
