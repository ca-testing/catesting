package com.pageLocators;

import org.openqa.selenium.By;

public interface LoginLocators {

	By loginBtn = By.xpath("//*[contains(text(),'Log In')]");
	By emailText = By.name("email");
	By emailTexts = By.name("emailllk");
	By passwordText = By.name("password");
	By passwordTexts = By.name("passwordddd");
	By signInBtn = By.id("loginbtn");
	By dashboard = By.xpath("//body/descendant::a[contains(text(),'Dashboard')]");

	// By errorMsg = By.xpath("//form/descendant::span[4]");
	By errorMsg = By.xpath("//form/div[1]/div/div/span/span");

}
