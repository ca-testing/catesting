package com.pageLocators;

import org.openqa.selenium.By;

public interface MailSacLocators {
	
	By mailHeading = By.xpath("//tr[@ng-repeat = 'msg in messages']/td[4]");
	By verifyNow = By.xpath("//a[text()='Verify Now']");
	String allmailHeading = "//tr[@ng-repeat = 'msg in messages']/td";
	By emailIdforLogin = By.name("username");
	By pwdforLogin = By.name("password");
	By signInBtn = By.xpath("//button[text()='Sign In']");
	By resetNowBtn = By.xpath("//a[text()='Reset Now']");
	By unblockLinkImage = By.xpath("//a[contains(text(),'Unblock links and images')]");
}
