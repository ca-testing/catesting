package com.pageLocators;

import org.openqa.selenium.By;

public interface PaymentsRequestsLocators {
	
	//By paymentRequestsHeading = By.xpath("//*[@id=\"app\"]/div[2]/div[1]/div/div[2]/div[1]/h1/span");
	//By paymentRequestsHeading = By.xpath("//*[@id=\"app\"]/div[2]/div[1]/div/div[1]/div[1]/h1");
	By paymentRequestsHeading = By.xpath("//*[@id='app']/descendant::h1");
	
	//By paymentRequestMenu = By.xpath("/html/body/div[1]/div/header/div[1]/nav/div[2]/div/div[1]/a");
	By paymentRequestMenu = By.xpath("//nav/descendant::a[5]");
	By paymentRequestMenus = By.xpath("//*[@id='navbarDropdown']");
	By createRequestMenu = By.xpath("//a[contains(text(),'Create Request')]");
	By manageRequestMenu = By.xpath("//a[contains(text(),'Manage Request')]");
	By email = By.id("transactionContact");
	By amount = By.id("transactionAmount");
	By description = By.id("transactionDescription");
	By sendPaymentLink_Btn = By.xpath("//button[contains(text(),'Send Payment Link')]");
	By sendPaymentLink_moreOption = By.xpath("//div[@class='btn-group dropup']/child::button[2]");
	By createPaymentLink = By.xpath("//*[@class='dropdown-menu dropdown-menu-right show']/a[contains(text(),'Create Payment Link')]");
	By chargeNow = By.xpath("//*[@class='dropdown-menu dropdown-menu-right show']/a[contains(text(),'Charge Now')]");
	By scheduleCharge = By.xpath("//*[@class='dropdown-menu dropdown-menu-right show']/a[contains(text(),'Schedule Charge')]");
	By copyLink = By.xpath("//*[@id='paymentLinkSentModal']/descendant::div[7]/child::button");
	By copyLink_CreateLink = By.xpath("//*[@id='paymentLinkCreatedModal']/descendant::div[7]/child::button");
	By copyLink_Schedule = By.xpath("//*[@id='paymentScheduleConfirmationModal']/descendant::div[8]/child::button");
	By copyLinksTextBox = By.xpath("//*[@id='paymentLinkSentModal_linkCopyInput']");
	By copyLinkTextbox = By.id("paymentLinkSentModal_linkCopyInput");
	By copyLinkTextbox_Schedule = By.id("paymentScheduleConfirmationModal_linkCopyInput");
	By copyLinkTextbox_CreateLink = By.id("paymentLinkCreatedModal_linkCopyInput");
	By close_Btn = By.xpath("//*[@id='paymentLinkSentModal']/descendant::div[10]/child::a");
	By pay_button = By.xpath("//*[@id='app']/descendant::button[2]");
	By checkCard = By.xpath("//*[contains(@id,'custom_check_card_')]");
	By payAllBtn = By.xpath("//div[@id='app']/descendant::button[3]");
	By paymentSuccess = By.xpath("//h2[text()='Payment success']");
	By oK_Btn = By.xpath("//h2[text()='Payment success']/following::button[2]");
	//By successMsg = By.xpath("//*[@id='app']/descendant::h3");
	By successMsg = By.xpath("//*[@id='app']/descendant::h4[2]/preceding-sibling::h3");
	By paymentLinkSuccess = By.xpath("//*[@id='paymentLinkSentModal']/descendant::h4");
	By totalCard = By.xpath("//*[@id='app']/div[1]/div[1]/div/div[2]/div[2]/div/div");
	String paymentStatusFirst = "//*[@id='app']/div[1]/div[1]/div/div[2]/div[2]/div/div[";
	String paymentStatusSecond = "]/div[1]/div/div[2]/div/div/span";
	String paymentAmountFirst = "//*[@id='app']/div[1]/div[1]/div/div[2]/div[2]/div/div[";
	String paymentAmountSecond = "]/descendant::div[2]/descendant::div[2]/span";
	String shieldStatusFirst = "//*[@id='app']/div[1]/div[1]/div/div[2]/div[2]/div/div[";
	String shieldStatusSecond = "]/descendant::div[2]/descendant::div[2]/span/descendant::i";
	String moreOptionFirst = "//*[@id='app']/div[1]/div[1]/div/div[2]/div[2]/div/div[";
	String moreOptionSecond = "]/div[1]/div/div[7]/div/a";
	String chargeNowFirst = "//*[@id='app']/div[1]/div[1]/div/div[2]/div[2]/div/div[";
	String chargeNowSecond = "]/div[1]/div/div[7]/descendant::a";
	String markAsPaidFirst = "//*[@id='app']/div[1]/div[1]/div/div[2]/div[2]/div/div[";
	String markAsPaidSecond = "]/div[1]/div/div[7]/descendant::a";
	By cancelBtn_AtPopup = By.id("manual-closeChargeNowModal");
	By paymentCardId = By.id("chargePaymentMethod");
	By paymentCard_WithoutCreateLink = By.xpath("//div[@id='paymentChargeModal']/descendant::select[@id='chargePaymentMethod']");
	By paymentFirstCard = By.xpath("//*[@id='chargePaymentMethod']/option[2]");
	By paymentCard_Schedule = By.xpath("//div[@id='paymentScheduleSelectCardModal']/descendant::select[@id='chargePaymentMethod']");
	By chargeNow_BtnForPayment = By.xpath("//*[@id='manual-paymentChargeModal']/descendant::a[2]");
	By chargeNow_WithoutCreateLinks = By.xpath("//div[@id='paymentChargeModal']/descendant::select[@id='chargePaymentMethod']/following::a[2]");
	By chargeNow_Schedule = By.xpath("//div[@id='paymentScheduleSelectCardModal']/descendant::select[@id='chargePaymentMethod']/following::a[2]");
	By yesDoIt = By.xpath("//*[contains(text(),'Yes, do it!')]");
	String sendFirstPart = "//*[@id='app']/div[1]/div[1]/div/div[2]/div[2]/div/div[";
	String sendSecondPart = "]/div[1]/div/div[7]/descendant::a";
	String editFirstPart = "//*[@id='app']/div[1]/div[1]/div/div[2]/div[2]/div/div[";
	String editSecondPart = "]/div[1]/div/div[7]/descendant::a";
	String voidFirst = "//*[@id='app']/div[1]/div[1]/div/div[2]/div[2]/div/div[";
	String voidSecond = "]/div[1]/div/div[7]/descendant::a";
	String paymentRefundFirst = "//*[@id='app']/div[1]/div[1]/div/div[2]/div[2]/div/div[";
	String paymentRefundSecond = "]/div[1]/div/div[7]/descendant::a[3]";
	By refundAmountInput = By.xpath("//div[@id='refund_amount']/descendant::input[@type='number']");
	By refundDescription = By.id("description_RA_refund_amount");
	By refundNowBtn = By.xpath("//div[@id='refund_amount']/descendant::button[2]");
	By yesRefundNow = By.xpath("//button[text()='Yes, Refund Now!']");
	By paymentLinkCreateSuccessMsg = By.xpath("//h4[contains(text(),'Payment link successfully created!')]");
	By emailPaymentLink_Btn = By.xpath("//div[@id='paymentLinkCreatedModal']/descendant::a[contains(text(),'Email Payment Link')]");
	By emailPaymentLink_Btn_Schedule = By.xpath("//div[@id='paymentScheduleConfirmationModal']/descendant::a[contains(text(),'Email Payment Link')]");
	By emailPaymentLink_Btn_CreateLink = By.xpath("//div[@id='paymentLinkCreatedModal']/descendant::a[contains(text(),'Email Payment Link')]");
	By closeBtn_atModel = By.id("close_sentLinkModal");
	By chargeNow_withoutCreateLink = By.xpath("//button[@id='closeAddEditPaymentRequestButton']/following::a[2]");
	By scheduleCharge_Btn = By.xpath("//button[@id='closeAddEditPaymentRequestButton']/following::a[3]");
	String requestIdFirst = "//*[@id='app']/div[1]/div[1]/div/div[2]/div[2]/div/div[";
	String requestIdSecond = "]/descendant::div[7]/span";
	String emailIdFirst = "//*[@id='app']/div[1]/div[1]/div/div[2]/div[2]/div/div[";
	String emailIdSecond = "]/descendant::div[8]/span";
	String dueDateFirst = "//*[@id='app']/div[1]/div[1]/div/div[2]/div[2]/div/div[";
	String dueDateSecond = "]/descendant::div[9]/span";
	String createdDateFirst = "//*[@id='app']/div[1]/div[1]/div/div[2]/div[2]/div/div[";
	String createdDateSecond = "]/descendant::div[10]/span";
	By emailRequired = By.xpath("//*[@id='chargeTab']/descendant::strong[1]");
	By amountRequired = By.xpath("//*[@id='chargeTab']/descendant::strong[2]");
	By currencyHeading = By.xpath("//*[@id='currency-selector']/descendant::b[1]");
	By currencySelector = By.id("currency-selector");
	By searchCurrency = By.xpath("//*[@id='currency-selector']/descendant::input[1]");
	By currencyFirstResult = By.xpath("//*[@id='currency-selector']/descendant::div[5]");
	
	String firstLocator = "//*[@id='app']/div[1]/div[1]/div/div[2]/div[2]/div/div[";
	String secondLocator = "]/descendant::div[";
	String thirdLocator = "]/span";
	
	String moreValueFirst = "//*[@id='app']/div[1]/div[1]/div/div[2]/div[2]/div/div[";
	String moreValueSecond = "]/descendant::a";
	By schedule_Date = By.xpath("//*[contains(@aria-expanded,'true')]/following-sibling::div/descendant::dd[2]");
	By expired_Date = By.xpath("//*[contains(@aria-expanded,'true')]/following-sibling::div/descendant::dd[3]");
	By chargeBack = By.xpath("//*[contains(@aria-expanded,'true')]/following-sibling::div/descendant::dd[4]");
	By paymentMethod = By.xpath("//*[contains(@aria-expanded,'true')]/following-sibling::div/descendant::dd[5]");
	By description_InMore = By.xpath("//*[contains(@aria-expanded,'true')]/following-sibling::div/descendant::dd[6]");
	By termsCondition_InMore = By.xpath("//*[contains(@aria-expanded,'true')]/following-sibling::div/descendant::dd[7]");
	By addNewRequest_Btn = By.xpath("//span[contains(text(),'Add New Request')]");
	
	/*
	 *   Authorization 
	 */
	
	By authorizationtab = By.id("authorization-tab");
	By sendAuthorizeLink = By.xpath("//div[@class='btn-group dropup']/child::button[1]");
	By createAuthorizationLink = By.xpath("//button[@id='closeAddEditPaymentRequestButton']/following::a[1]");
	By authorizeNow = By.xpath("//button[@id='closeAddEditPaymentRequestButton']/following::a[2]");
	By captureAmountTextBox = By.xpath("//div[@id='capture_security_deposit_amount']/descendant::input[@type='number']");
	By captureDescription = By.id("capture_description");
	By captureNow_Btn = By.xpath("//div[@id='capture_security_deposit_amount']/descendant::button[2]");
	String captureFirst = "//*[@id='app']/div[1]/div[1]/div/div[2]/div[2]/div/div[";
	String captureSecond = "]/div[1]/div/div[7]/descendant::a[3]";
	String releaseFirst = "//*[@id='app']/div[1]/div[1]/div/div[2]/div[2]/div/div[";
	String releaseSecond = "]/div[1]/div/div[7]/descendant::a[4]";
	By yesCaptureNow = By.xpath("//button[text()='Yes, Capture Now!']");
	
	String lock_first = "//*[@id='app']/div[1]/div[1]/div/div[2]/div[2]/div/div[";
	String lock_second = "]/descendant::i";
	
	/*
	 *   More Settings
	 */
	
	By moreSetting = By.xpath("//*[@id='paymentLinkAddEditModal']/descendant::a[3]");
	By scheduleChargeDate = By.id("date_1-input");
	By paymentLinkExpire = By.id("date_2-input");
	By chargeBackProtection = By.xpath("//label[@for='create-payment-request-cb-toggle']");
	By transactionTerms = By.id("transactionTerms");
	By scheduleDateSet = By.xpath("//*[@id='date_1-wrapper']/descendant::span[2]/span");
	By allDate = By.xpath("//*[contains(@class,'datepicker-days')]/child::button");
	By allHours = By.xpath("//*[contains(@class,'time-picker-column-hours')]/descendant::button");
	By allMinutes = By.xpath("//*[contains(@class,'time-picker-column-minutes')]/descendant::button");
	By setButton = By.xpath("//*[contains(@class,'datepicker-buttons-container')]/descendant::button[1]");
	By setButton2 = By.xpath("//*[@id='date_2-wrapper']/div[2]/div/div[3]/button");
	
	/*
	 *  Search
	 */
	
	By searchBox = By.id("filter-search");
	By noResult = By.xpath("//div[@class='page-body']/descendant::div[13]/descendant::div[3]");
	
}
