package com.pageLocators;

import org.openqa.selenium.By;

public interface BookARoomLocators {

	By menuForLogout = By.xpath("//div[@id='m_header_topbar']/descendant::a[3]");
	By logout_Btn = By.xpath("//div[@id='m_header_topbar']/descendant::a[6]");

	/*
	 * Create Booking Locators
	 */
	By createTestBooking = By.xpath("//div[@id='m_ver_menu']/descendant::a/span");
	By bookingHeading = By.xpath("//h3[contains(text(),'Bookings')]");
	By singleBooking_radio = By.id("single-booking");
	By groupBooking_radio = By.id("group-booking");
	By testAccount_dropdown = By.id("select-account");
	By selectProperty_dropdown = By.id("select-property");
	By selectRoom_dropdown = By.id("select-room");
	By select_bookingSource_dropdown = By.id("select-booking-source");
	By cardType_dropdown = By.id("card-type");
	By checkin_date = By.id("check-in-date");
	By checkOut_date = By.id("check-out-date");
	By eachDate_Value = By.xpath("//tbody/tr/td");
	By totalPrice = By.id("total-price");
	By guestEmail = By.id("guest-email");
	By guestComment = By.id("guest-comments");
	By goodCard = By.id("good-card");
	By save_Btn = By.id("booking-submit");
	By bookingIdAfterBooking = By.id("response-booking-id");
	By close_btn_popup = By.xpath("//button[contains(text(),'Close')]");
	By go_to_next_Month = By.xpath("/html/body/div[4]/div[1]/table/thead/tr[2]/th[3]");
	By guestFirstName = By.id("guest-first-name");
	By guestLastName = By.id("guest-last-name");
	By guestMobile = By.id("guest-mobile");
	By guestAddress = By.id("guest-address");
	By guestPostalAddress = By.id("guest-postal-code");
	By guestCountry = By.id("guest-country");
	By guestCity = By.id("guest-city");
	By guestCard = By.id("guest-card");
	By cardExpiry = By.id("card-expiry");
	By cardCvv = By.id("card-cvv");
	
	/*
	 * Verifying Booking Details
	 */

	By cards = By.xpath("//div[contains(@id,'single_booking_box-outer-')]");
	By bookingId = By.xpath("//div[contains(@id,'single_booking_box-outer-')]/descendant::span[1]");
	By bookingLink = By.xpath("//nav/descendant::a[5]");
	String pre_checkin_first_status = "//div[@id='single_booking_box-outer-";
	String pre_checkin_second_status = "']/descendant::span[4]";
	String booking_status_second = "']/descendant::span[2]";
	String checkin_date_second = "']/descendant::span[5]/following::div[3]/div[1]";
	String total_stays_in_days_second = "']/descendant::span[5]/following::div[3]/div[2]";
	String checkout_date_second = "']/descendant::span[5]/following::div[3]/div[3]";
	String guestName_first = "//div[@id='single_booking_box-outer-";
	String guestName_second = "']/descendant::span[3]";
	String cardMenuFirst = "//div[@id='single_booking_box-outer-";
	String cardMenuSecond = "']/descendant::a[@id='moreMenu']";
	String shareLinkFirst = "//div[@id='single_booking_box-outer-";
	String shareLinkSecond = "']/descendant::a[contains(text(),'Share Pre check-in')]";
	String Send_Pre_Check_in_Email_Second = "']/descendant::a[contains(text(),'Send Pre Check-in Email')]";
	String re_Sync_Second = "']/descendant::a[contains(text(),'Re-sync')]";
	String pre_checkin_link_Second = "']/descendant::a[contains(text(),'Pre Check-in Link')]";
	By link = By.id("shareBookingModal_linkCopyInput");
	String edit_Booking_second = "']/descendant::a[contains(text(),'Edit Booking')]";
	By close_Btn_At_Popup = By.xpath("//button[contains(text(),'idden for shareBookingModal modal')]/preceding::a[contains(text(),'Close')]");
	String openCard_first = "//i[@id='";
	String openCard_second = "']";
	String emailAtBookingList_first = "//div[@id='id_";
	String emailAtBookingList_second = "']/descendant::div[7]/following::span[2]";
	String adultsAtBookingList_second = "']/descendant::div[7]/following::span[4]";
	String childAtBookingList_second = "']/descendant::div[7]/following::span[6]";
	String paymentTypeAtBookingList_second = "']/descendant::div[7]/following::span[9]";
	String creditCardNumber_first = "//a[@id='add_credit_card_button_";
	String creditCardNumber_second = "']";
	String documentNumber_second = "']/following::a[1]";
	String upsellNumber_second = "']/following::a[2]";
	String authorizeStatus_second = "']/descendant::span[6]";
	String paidStatus_second = "']/descendant::span[9]";
	
	/*
	 *  Forms Fill up details
	 */

	By formStepsCard = By.xpath("//div[@id='chat-box-div']/following::h3[1]/following::a[contains(@class,'gp-step')]");
	String stepName_Card_first = "//div[@id='chat-box-div']/following::h3[1]/following::a[";
	String stepName_Card_second = "][contains(@class,'gp-step')]/descendant::span[2]";
	By formFilupStart_heading = By.xpath("//div[@id='app']/descendant::p");
	By get_Started_Btn = By.xpath("//a[contains(text(),'Get Started')]");
	By live_Mode_Btn = By.xpath("//div[@class='button-cover']/button[2]");
	By view_Mode_Btn = By.xpath("//div[@class='button-cover']/button[1]");
	By confirm_Btn = By.xpath("//button[contains(text(),'CONFIRM')]");
	By guestEmail_At_BasicInfo = By.id("guestEmail");
	By guest_Adult_Count = By.id("guestAdults");
	By guest_Children_Count = By.id("guestChildren");
	By save_Continue_Btn = By.xpath("//a[contains(text(),'Save & Continue')]");
	By arrival_info_heading = By.xpath("//form/descendant::h4[contains(text(),'ARRIVAL INFORMATION')]");
	By arrivingBy_dropdown = By.id("guestArrivalMethod");
	By flightNumber = By.id("flightNumber");
	By arriving_Other = By.id("other");
	//By estimated_ArrivalTime_dropdown = By.xpath("//*[@id='guestArrivalMethod']/following::select");
	By estimated_ArrivalTime_dropdown = By.id("standard_check_in_time");
	By uploadCardHeading = By.xpath("//div[@id='app']/descendant::h4[contains(text(),'Upload Document(s)')]");
	By creditCard_upload = By.xpath("//div[@class='UploadImages']/child::label[@for='credit_card_file'][1]");
	//By creditCard_upload = By.xpath("//div[@class='UploadImages']/descendant::img");credit_card_file
	//By creditCard_upload = By.id("credit_card_file");
	By selfie_heading = By.xpath("//form[1]/descendant::p[1]");
	By addOnService_heading = By.xpath("//div[@id='app']/descendant::h3[2]");
	By lunch_for_Guest = By.xpath("//form[1]/descendant::input");
	By breakFast_for_Guest = By.xpath("//form[2]/descendant::input[2]");
	By breakFast_Checkbox = By.xpath("//form[2]/descendant::input[2]/preceding::input[1]");
	By guest_car_Parking_input = By.xpath("//form[2]/descendant::input[2]/following::p[3]/following::input[1]");
	By car_Parking_Checkbox = By.xpath("//form[2]/descendant::input[2]/following::input[1]");
	By bike_tour_Checkbox = By.xpath("//form[2]/descendant::input[2]/following::input[3]");
	By guest_bike_Parking_input = By.xpath("//form[2]/descendant::input[2]/following::input[4]");
	By early_checkin_Checkbox = By.xpath("//form[2]/descendant::input[2]/following::input[5]");
	By continental_Checkbox = By.xpath("//form[2]/descendant::input[2]/following::input[6]");
	By continental_breakFast_input = By.xpath("//form[2]/descendant::input[2]/following::input[7]");
	By pay_balance_Btn = By.xpath("//div[@id='chat_panel_right']/following::a[11]");
	By pay_balance_Btn_second = By.xpath("//div[@id='chat_panel_right']/following::a[12]");
	By terms_Condition_CheckBox = By.id("customCheck1");
	By save_Continue = By.xpath("//a[contains(text(),'Save & Continue')]");
	By your_Summary = By.xpath("//div[@id='app']/descendant::h1");
	By payment_Summary = By.xpath("//form/descendant::h4");
	By view_mode_btn_attr = By.xpath("//span[contains(text(),'View Mode')]/parent::button");
	By live_mode_btn_attr = By.xpath("//span[contains(text(),'Live Mode')]/parent::button");
	By mode_toggle_btn = By.xpath("//span[contains(text(),'Live Mode')]/parent::button/parent::div");
	By draw_sign = By.xpath("//div[@id='signaturePad-wrapper']/descendant::canvas");
	By checkin_At_Summary = By.xpath("//div[@class='row'][2]/descendant::dd[3]");
	By checkOut_At_Summary = By.xpath("//div[@class='row'][2]/descendant::dd[4]");
	
	/*
	 *  Credit Card Locators
	 */
	
	By credit_Card_Frame = By.xpath("//iframe[contains(@name,'__privateStripeFrame')]");
	By credit_Card_Number = By.xpath("//input[contains(@name,'cardnumber')]");
	By credit_Card_ExpiryDate = By.xpath("//input[contains(@name,'exp-date')]");
	By credit_Card_CVV = By.xpath("//input[contains(@name,'cvc')]");
	By credit_Card_Postal = By.xpath("//input[contains(@name,'postal')]");
	By toaster = By.xpath("//*[contains(@id,'toast-container')]");
	
	
	/*
	 *     Guest Portal
	 */
	
	String guest_Portal_Link_First = "//div[@id='single_booking_box-outer-";
	String guest_Portal_Link_second = "']/descendant::a[contains(text(),'Guest Portal')]";
	String view_detail_Link_second = "']/descendant::a[contains(text(),'View Detail')]";
	By booking_information_guest_Portal = By.xpath("//a[contains(text(),'Booking Information')]");
	By booking_status_booking_information = By.xpath("//dt[contains(text(),'Booking Status')]/following::dd[1]/child::span[1]");
	By booking_number_booking_information = By.xpath("//dt[contains(text(),'Booking No.')]/following::dd[1]");
	By checkin_date_booking_information = By.xpath("//dt[contains(text(),'Check-In Date')]/following::dd[1]");
	By checkout_date_booking_information = By.xpath("//dt[contains(text(),'Check-Out Date')]/following::dd[1]");
	By email_booking_information = By.xpath("//dt[contains(text(),'Email')]/following::dd[1]");
	By arriving_by_booking_information = By.xpath("//dt[contains(text(),'Arriving By')]/following::dd[1]");
	By edit_Email_booking_information = By.xpath("//input[@placeholder='Email address']");
	By edit_ArrivingBy_booking_information = By.xpath("//label[contains(text(),'Arriving By')]/following::select[contains(@class,'custom-select-arrow')][1]");
	By edit_fligh_number_booking_information = By.id("flightNumber");
	//By edit_estimated_time_booking_information = By.xpath("//label[contains(text(),'Estimated Arrival Time')]/preceding::select[@class='custom-select-arrow'][1]");
	By edit_estimated_time_booking_information = By.id("standard_check_in_time");
	By update_button_booking_information = By.xpath("//button[contains(text(),'Update')]");
	By edit_button = By.xpath("//dt[contains(text(),'Estimated Arrival Time')]/following::dd[1]/following::span[1][contains(text(),'Edit')]");
	By save_button_payment_details = By.xpath("//button[contains(text(),'Save')]");
	By booking_information_payment_details = By.xpath("//a[contains(text(),'Payment Details')]");
	By payment_details_Tab = By.xpath("//a[@id='payment_details_tab']");
	By credit_card_Edit = By.xpath("//div[@class='edit-card']/descendant::span[contains(text(),'Edit')]");
	By document_Upload_Tab = By.xpath("//a[@id='document_upload_tab']");
	By credit_card_Submitted = By.xpath("//span[@class='guest-portal-doc-upload-status text-white bg-primary']");
	By digital_sign_Tab = By.xpath("//a[@id='digital_signature_tab']");
	By guest_sign_outlet = By.xpath("//div[@class='guest-documents-outter-wrapper']");
	By upsell_Purchased_Tab = By.xpath("//a[@id='upsell_purchased_tab']");
	By purchased_Add_on_Services_Heading = By.xpath("//div[@class='upsell_purchased_tab collapse show']/descendant::h4");
	By service_count = By.xpath("//div[@class='upsell_purchased_tab collapse show']/descendant::h4/span[1]");
	By all_service_heading = By.xpath("//div[@class='upsell-header-infos']/descendant::h3");
	
	/*
	 *   Booking Details Tab
	 */
	
	By bookingId_bookingTab = By.id("bookingID");
	By booking_date_bookingTab = By.id("bookingDate");
	By bookingSource_bookingTab = By.id("source");
	By checkin_date_bookingTab = By.id("checkinDate");
	By checkout_date_bookingTab = By.id("checkoutDate");
	By checkinTime_bookingTab = By.id("checkinTime");
	By numberOf_guest_bookingTab = By.id("guestsAdults");
	By numberOf_child_bookingTab = By.id("guestsChildren");
	By guest_comment_bookingTab = By.id("comments");
	By internal_Notes_bookingTab = By.id("internal_notes");
	By notes_bookingTab = By.id("notes");
	By firstName_bookingTab = By.id("firstName");
	By lastName_bookingTab = By.id("lastName");
	By email_bookingTab = By.id("email");
	By phoneNumber_bookingTab = By.xpath("//label[@for='phone']/following::input[@name='telephone']");
	By saveChanges_button_bookingTab = By.xpath("//button[@class='btn btn-sm btn-success']");
	
	
	/*
	 *   Payments Tab
	 */
	
	By paymentsTab = By.id("tab_general-payment-detail");
	By amount_for_additional_charge = By.id("additional_amount");
	By description_for_additional_charge = By.id("description_AC_additional_charge_modal");
	By chargeProtection_toggle = By.xpath("//textarea[@id='description_AC_additional_charge_modal']/following::span[3][@class='toggle-switch']");
	By chargePaymentMethod = By.id("chargePaymentMethod");
	By firstOption = By.xpath("//select[@id='chargePaymentMethod']/option[2]");
	By charge_button_paymentTab = By.xpath("//button[contains(@class,'btn btn-sm btn-success')][contains(text(),'Charge')]");
	By yesDoIt_Button = By.xpath("//button[contains(text(),'Yes, do it!')]");
	By payment_Status_Cards = By.xpath("//div[@class='status-grid-item']");
	By chargeAdditional_button = By.xpath("//div[@class='status-grid-item']/following::button[2]");
	By refund_button = By.xpath("//div[@class='status-grid-item']/following::button[3]");
	By addPayment_Method = By.xpath("//div[@class='status-grid-item']/following::button[4]");
	By update_button_after_add_Payment = By.xpath("//button[@id='guest_credit_card_modal_close']/following::button[1]");
	By yesUpdate_button = By.xpath("//button[contains(text(),'Yes, Update Now!')]");
	By allCredit_Card = By.xpath("//div[@class='rounded border card-wrap']");
	By gotoTop = By.xpath("//div[@class='page-header']/descendant::h1");
	By refundAmount_textbox = By.xpath("//div[@id='refund_amount']/descendant::input[@placeholder='Amount']");
	By refund_Description = By.id("description_RA_refund_amount");
	By refundButton_refundPopup = By.xpath("//button[@id='force_close']/following::button[contains(@class,'btn btn-sm btn-success')][1]");
	By yesRefund_Now = By.xpath("//button[contains(text(),'Yes, Refund Now!')]");
	By description_Text_Verify = By.xpath("//div[@class='booking-payment-summary']/descendant::strong[3]");
	By amount_Verify = By.xpath("//div[@class='booking-payment-summary']/descendant::strong[3]/following::td[1]");
	
	/*
	 *   Online Check-in
	 */
	
	By onlineCheckin_Tab = By.id("tab_general-guest-experience");
	By estimatedTime_checkinTab = By.id("estArrivalTime");
	By arrivalBy_checkinTab = By.id("arrivalBy");
	By flight_Other_checkinTab = By.id("flightNumber");
	
	
	/*
	 *  Upsell Tab
	 */
	
	By upsell_Tab = By.id("tab_general-upsell-detail");
	By upsell_heading = By.xpath("//div[@id='myTabContent']/descendant::h4[1]");
	
	
	/*
	 *  Document Tab
	 */
	
	By document_Tab = By.id("tab_general-documents-detail");
	By document_Heading = By.xpath("//div[@class='document-check']/descendant::h4[1]");
	By total_col_document = By.xpath("//div[@class='document-check']/descendant::div[@class='col-md-6']");
	
	/*
	 *   Questionnaire
	 */
	
	
	By textbox_answer_questionnare = By.xpath("//h4[text()='New Question']/following::input[1]");
	By date_questionnare = By.xpath("//h4[text()='New Question']/following::input[2]");
	By phone_number_questionare = By.xpath("//h4[text()='New Question']/following::input[2]");
	By email_questionnare = By.xpath("//h4[text()='New Question']/following::input[3]");
	By radio_questionnare = By.xpath("//h4[text()='New Question']/following::input[4]");
	By number_questionnare = By.xpath("//h4[text()='New Question']/following::input[6]");
	By choice_questionnare = By.xpath("//h4[text()='New Question']/following::input[7]");
	By textArea_questionnare = By.xpath("//h4[text()='New Question']/following::textarea");
	By checkbox_questionnare = By.xpath("//h4[text()='New Question']/following::input[11]");
	By fileUpload_questionnare = By.xpath("//div[@class='custom-file']/label[@class='btn btn-primary btn-sm']");
	
	
	By questionnare_Tab = By.id("tab_general-questionnaire-detail");
	By textbox_questionTab = By.xpath("//div[@id='myTabContent']/descendant::form[1]/descendant::input");
	By phoneNumber_questionTab = By.xpath("//div[@id='myTabContent']/descendant::form[2]/descendant::input");
	By email_questionTab = By.xpath("//div[@id='myTabContent']/descendant::form[3]/descendant::input");
	By number_questionTab = By.xpath("//div[@id='myTabContent']/descendant::form[5]/descendant::input");
	By textarea_questionTab = By.xpath("//div[@id='myTabContent']/descendant::textarea");
	
	
	/*
	 *   Card Different charge at booking listing
	 */
	
	By total_paymentCards = By.xpath("//div[@class='booking-card-status-grid']/child::div");
	By paid_Reservation_Charge = By.xpath("//div[@class='booking-card-status-grid']/child::div[1]/child::div");
	By paid_Reservation_Charge_menu = By.xpath("//div[@class='booking-card-status-grid']//div[@class='grid-item-content']/descendant::div[@class='grid-item-action']");
	By paid_Reservation_Charge_Refund = By.xpath("//a[contains(text(),'Refund')]");
	By card_title_Booking_List = By.xpath("//div[@class='booking-card-status-grid']//div[@class='grid-item-content']/descendant::span[1]");
	String paid_Reservation_Charge_menu_First = "//*[@id='id_";
	String paid_Reservation_Charge_menu_Second = "']/div/div[3]/div[2]/div[1]/div/div[3]";
	String paid_Reservation_Charge_menu_Second_secondCard = "']/div/div[3]/div[2]/div[2]/div/div[3]";
	String paid_Reservation_Charge_menu_Second_thirdCard = "']/div/div[3]/div[2]/div[3]/div/div[3]";
	String paid_Reservation_Charge_ChargeNow_first = "//div[@id='drop-down-actions";
	String paid_Reservation_Charge_ChargeNow_second = "']/child::a[contains(text(),'Charge Now')]";
	By yes_Button = By.xpath("//button[contains(text(),'Yes')]");
	String paid_Reservation_Charge_ChangeAmount_Second = "']/child::a[contains(text(),'Change amount')]";
	String paid_Reservation_Charge_Without3D = "']/child::a[contains(text(),'Charge Now (without 3D)')]";
	String paid_Reservation_Charge_MarkAsPaid_Second = "']/child::a[contains(text(),'Mark as Paid')]";
	String paid_Reservation_Charge_Void = "']/child::a[contains(text(),'Void')]";
	By paid_Reservation_Charge_New_Amount = By.xpath("//input[@placeholder='New Amount']");
	By paid_Reservation_Charge_Reduce_Button = By.xpath("//button[contains(@class,'btn btn-sm btn-success px-3')][contains(text(),'Reduce')]");
	String paid_Reservation_Charge_AuthorizeNow = "']/child::a[contains(text(),'Authorize Now')]";
	By paid_Reservation_Charge_AdditionalCharge = By.xpath("//a[contains(text(),'Additional Charge')]");
	String paid_Reservation_Charge_Release = "']/child::a[contains(text(),'Release')]";
	String paid_Reservation_Charge_Capture = "']/child::a[contains(text(),'Capture')]";
	By paid_Reservation_Capture_Description = By.id("capture_description");
	By paid_Reservation_Charge_CaptureNow_Button = By.xpath("//button[contains(@class,'btn btn-sm btn-success px-3')][contains(text(),'Capture')]");
	By yesCaptureNow_Button = By.xpath("//button[contains(text(),'Yes, Capture Now!')]");
	String paid_Reservation_Charge_AuthorizeNow_Without3D = "']/child::a[contains(text(),'Authorize Now (without 3D)')]";
	By guestCreditCardUpdate_Button = By.xpath("//button[@id='guest_credit_card_modal_close']/following::button[contains(text(),'Update')][1]");
	String additinalCharge_at_card_first = "//div[@id='id_";
	String additionalCharge_at_card_second = "']/descendant::a[contains(text(),'Additional Charge')]";
	
	
	/*
	 *  Add New Booking from Listing
	 */
	
	By addBookingButton = By.id("add_booking_button");
	By assignedRental = By.id("assigned_rental");
	By booking_source = By.id("booking_source");
	By reservation = By.id("reservation_status");
	By dates_in_form = By.id("datepicker-trigger-booking-add");
	By price_in_form = By.id("balancePrice");
	By bookingNotes = By.id("bookingNotes");
	String checkinDate_first = "//div[@id='bookings-tabContent']/descendant::td[@data-date='";
	String checkinDate_second = "']";
	By firstName_bookingForm = By.id("first_name");
	By lastName_bookingForm = By.id("last_name");
	//By telephone_bookingForm = By.name("telephone");
	By telephone_bookingForm = By.xpath("//div[@id='accordion-booking-booker-section']/descendant::input[@name='telephone']");
	By email_bookingForm = By.id("email");
	By adults_bookingForm = By.id("num_adults");
	By child_bookingForm = By.id("num_child");
	By saveChanges_bookingForm = By.xpath("//button[@id='add_booking_close_modal']/following::button[contains(text(),'Save Changes')][1]");
	
	
	/*
	 *  Edit Booking Form
	 */
	
	By first_Booking_Row = By.xpath("//div[@id='reduce_amount_modal']/following::div[contains(@id,'single_booking_box-outer-')][1]");
	
	
}