package com.pageLocators;

import org.openqa.selenium.By;

public interface ForgotPasswordLocators {
	
	By forgotPWDLink = By.xpath("//a[contains(text(),'Forgot Password?')]");
	By forgotEmail = By.xpath("//input[contains(@type,'email')]");
	By changePWDBtn = By.id("loginbtn");
	By toaster = By.xpath("//*[contains(@id,'toast-container')]");
	By errorAlert = By.xpath("//form/descendant::span[4]");
	// By errorAlert = By.xpath("//*[@id="app"]/main/div/div[3]/div/div/div/form/div/div/div/span/span");
	By backToLogin = By.xpath("//a[text()='Log In']");
	By languageDropdown = By.xpath("//main/descendant::button");
}
