package com.pageLocators;

import org.openqa.selenium.By;

public interface TeamsLocators {

	By teamsText = By.xpath("//a[contains(text(),'Team')]");
	By addTeamMember = By.xpath("//button[contains(text(),'+ Add Team Member')]");
	By teamUserImg = By.id("user_pic");
	By teamFirstName = By.id("add_fname");
	By teamLastName = By.id("add_lname");
	By teamEmail = By.id("add_email");
	// By teamPhone = By.name("telephone");
	By teamPhone = By.xpath("//form/div/div/div[1]/div[2]/div[2]/div[3]/div/div/div/input");
	By roleManager = By.xpath("//*[@for='add_checkbox-4']");
	By roleTeams = By.xpath("//*[@for='add_checkbox-5']");
	By accountSetup = By.xpath("//*[@for='add_checkbox-60']");
	By bookings = By.xpath("//*[@for='add_checkbox-56']");
	By guestExperience = By.xpath("//*[@for='add_checkbox-58']");
	By preferences = By.xpath("//*[@for='add_checkbox-59']");
	By properties = By.xpath("//*[@for='add_checkbox-57']");
	By sendInvitation = By.id("_signup_submit");

	By teamfirstnameerror = By.xpath("//input[@id='add_fname']/following::strong[1]");
	By teamlastnameerror = By.xpath("//input[@id='add_lname']/following::strong[1]");
	By teamemailerror = By.xpath("//input[@id='add_email']/following::strong[1]");

	/*
	 * Teams Edit, Activate, Deactivate
	 */

	By cardCount = By.xpath("//div[contains(@id,'user-de-card-da-id')]");
	// By teamCardEmail = By.xpath("//span[@class='name']/following::a[1]");
	String teamsCardEmailsFirst = "//*[@id=";
	String teamsCardEmailsSecond = "]/div[1]/div/div/div[3]";
	String moreMenufirst = "//*[@id=";
	String moreMenuSecond = "]";
	// By resend = By.name("Resend Invite");
	String resend = "ResendInviteBtn-";
	By yesResend = By.xpath("//button[text()='Yes, Resend']");

	String emailLocatorFirstPart = "/html/body/div[1]/div/div[1]/div[3]/div/div/div/div[2]/div[";
	String emailLocatorSecondPart = "]/div[1]/div/div/div[3]";

	String moreOptionFirstPart = "/html/body/div[1]/div/div[1]/div[3]/div/div/div/div[2]/div[";
	String moreOptionSecondPart = "]/div[1]/div/div/div[5]/div/div/a";

	String deactivate = "DeactivateBtn-";
	String activate = "ActivateBtn-";

	By yesDoIt = By.xpath("//button[contains(text(),'Yes, do it!')]");

	String editBtn = "DataEditBtn-";
	String saveChanges = "UpdateUserBtn-";
	By editFirstName = By.id("_fname");

	String deleteBtn = "DeleteUserBtn-";
	By yesDeleteIt = By.xpath("//button[contains(text(),'Yes, Delete it!')]");

}
