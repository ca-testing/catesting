package com.pageLocators;

import org.openqa.selenium.By;

public interface LogoutLocators {
	
	By userMenu = By.id("dropdownMenuButton");
	By logout = By.xpath("//a[contains(text(),'Logout')]");

}
