package com.pageLocators;

import org.openqa.selenium.By;

public interface ChangePasswordLocators {
	
	By changePassword = By.name("Change Password");
	By oldPassword = By.id("old_password");
	By oldPwdErrorMsg = By.xpath("//input[@id='old_password']/following::strong");
	By newPassword = By.id("new_password");
	By newPwdErrorMsg = By.xpath("//input[@id='new_password']/following::strong");
	By confirmPassword = By.id("new_password_confirmation");
	//By confirmPwdErrorMsg = By.xpath("");
	By savePasswordBtn = By.name("Save Password");

}
