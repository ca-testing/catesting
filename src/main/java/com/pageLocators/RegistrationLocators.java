package com.pageLocators;

import org.openqa.selenium.By;

public interface RegistrationLocators {

	By registerBtn = By.xpath("//a[text()='Register']");
	By signUpHeading = By.xpath("//h2[text()='Sign Up']");

	By fullName = By.xpath("//input[@placeholder='Full Name']");
	// By fullName = By.xpath("//form[@id='register-form']/descendant::input[1]");

	By companyName = By.xpath("//form[@id='register-form']/descendant::input[2]");
	// By companyName = By.xpath("//input[@placeholder='Company Name']");

	By telephone = By.name("telephone");
	By email = By.xpath("//form[@id='register-form']/descendant::input[4]");
	By password = By.id("password");
	By confirmPassword = By.id("password-confirm");
	By PMStextBox = By.xpath("//form[@id='register-form']/descendant::input[7]");
	By agree = By.name("agree");
	By checkBox = By.xpath("//*[@id=\"register-form\"]/label/span");
	By getStartBtn = By.id("signupbtn");
	
	/*/
	 *   Error Messages
	 */
	
	//By fullNameErrorMsg = By.xpath("//form[@id='register-form']/descendant::input[1]/following::span[4]");
	By fullNameErrorMsg = By.xpath("//*[@id=\"register-form\"]/div[1]/div[1]/div/span/span");
	//By companyNameErrorMsg = By.xpath("//form[@id='register-form']/descendant::input[2]/following::span[4]");
	By companyNameErrorMsg = By.xpath("//*[@id=\"register-form\"]/div[1]/div[2]/div/span/span");
	//By telePhoneErrorMsg = By.xpath("//form[@id='register-form']/descendant::input[3]/following::span[4]");
	//By telePhoneErrorMsg = By.xpath("//*[@id=\"register-form\"]/div[1]/div[3]/div/span/span");
	By telePhoneErrorMsg = By.xpath("//*[@id=\"result\"]/div[2]/span/span");
	//By emailErrorMsg = By.xpath("//form[@id='register-form']/descendant::input[4]/following::span[4]");
	//By emailErrorMsg = By.xpath("//*[@id=\"register-form\"]/div[1]/div[4]/div/span/span");
	By emailErrorMsg = By.xpath("//*[@id=\"register-form\"]/div[3]/div/div/span/span");
	//By passwordErrorMsg = By.xpath("//form[@id='register-form']/descendant::input[5]/following::span[4]");
	//By passwordErrorMsg = By.xpath("//*[@id=\"register-form\"]/div[1]/div[5]/div/span/span");
	By passwordErrorMsg = By.xpath("//*[@id=\"register-form\"]/div[4]/div[1]/div/span/span");
	//By confirmErrorMsg = By.xpath("//form[@id='register-form']/descendant::input[6]/following::span[4]");
	//By confirmErrorMsg = By.xpath("//*[@id=\"register-form\"]/div[1]/div[6]/div/span/span");
	By confirmErrorMsg = By.xpath("//*[@id=\"register-form\"]/div[4]/div[2]/div/span/span");
	//By termsErrorMsg = By.xpath("//form[@id='register-form']/descendant::input[7]/following::span[4]");
	//By termsErrorMsg = By.xpath("//*[@id=\"register-form\"]/div[1]/div[7]/div/span/span");
	By termsErrorMsg = By.xpath("//*[@id=\"register-form\"]/div[6]/span/span");
	By companyTwo = By.xpath("//*[@id=\"register-form\"]/div[1]/div[2]/div/span/span");
	
	/*
	 *  Success Register
	 */
	
	
	By confirmEmailHeading = By.xpath("//h3[text()='Confirm Your Email']");
	// Example //h2[text()='Hello Abc']
	By checkFullName = By.xpath("//h3[text()='Confirm Your Email']/following::h2");
	By confirmEmailId = By.xpath("//h3[text()='Confirm Your Email']/following::p[1]");
	By continueBtn = By.id("ContinueBtn");
	By resendBtn = By.id("ResendEmailBtn");
}
